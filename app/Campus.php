<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Throwable;

class Campus extends Model
{
    protected $guarded = [];

    public function campus()
    {
        return $this->belongsTo('App\Campus');
    }

    public static function getAllCitiesArray()
    {
        try {
            // Validate the value...
            $campus = Campus::all();
            if (count($campus) > 0){
                foreach ($campus as $item) {
                    $data[$item->city] = $item->city;
                }

                return $data;
            }

        } catch (Throwable $e) {
            return false;
        }
    }
}


