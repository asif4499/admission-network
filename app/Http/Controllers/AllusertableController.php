<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use App\Supervisor;
use App\Student;
use App\Program;
use App\Models\Recruiter;
use App\Models\SchoolInfo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class AllusertableController extends Controller
{
    public function index($role_id)
    {
        
        $datas = User::all();
      
        //dd($datas);
        return view('layouts.backend.users.user-list', compact('datas','role_id'));
    }


    public function userList(Request $request){
        if($request->input('role_id')==1){
            $data = User::all();
        }
            
        elseif($request->input('role_id')==10){
            $data = User::all()
                    ->where('status',2);
        }
        else{
            $data = User::all()
                ->where('role_id',$request->input('role_id'));
        //dd($data);
        }

        return DataTables::of($data)
        ->editColumn('name', function($data){
            return $data->name;
        })
        ->editColumn('email', function($data){
            return $data->email;
        })
        ->editColumn('role_id', function($data){
            return $data->role_id;
        })
        // ->editColumn('status', function($data){
        //     return $data->phone;
        // })
        // ->editColumn('created_by', function($data){
        //     return $data->district->title;
        // })
        // ->editColumn('updated_by', function($data){
        //     return $data->status;
        // })
        ->editColumn('registered_at', function($data){
            $date= strtotime($data->created_at);
            return date('d-M-Y | h:i A', $date);
        })
        ->editColumn('activated_at', function($data){
            $date= strtotime($data->updated_at);
            return date('d-M-Y | h:i A', $date);
        })
        ->addColumn('action', function($data){
            return $data;
        })
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);

        
        return view('layouts.backend.users.user-list');
    }

    public function supervisorIndex()
    {
        $datas = Supervisor::all();
        //dd($datas);
        return view('layouts.backend.supervisor.admin', compact('datas'));
    }


    public function supervisorList(){
       
        $data = Supervisor::all();
        
        //dd($data);

        return DataTables::of($data)
        // ->editColumn('name', function($data){
        //     return $data->name;
        // })
        // ->editColumn('email', function($data){
        //     return $data->email;
        // })
        // ->editColumn('role_id', function($data){
        //     return $data->role_id;
        // })
        // ->editColumn('status', function($data){
        //     return $data->phone;
        // })
        // ->editColumn('created_by', function($data){
        //     return $data->district->title;
        // })
        // ->editColumn('updated_by', function($data){
        //     return $data->status;
        // })
        ->editColumn('registered_at', function($data){
            $date= strtotime($data->created_at);
            return date('d-M-Y | h:i A', $date);
        })
        ->editColumn('activated_at', function($data){
            $date= strtotime($data->updated_at);
            return date('d-M-Y | h:i A', $date);
        })
        ->addColumn('action', function($data){
            return $data;
        })
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);

        
        return view('layouts.backend.supervisor.admin');
    }

    public function studentIndex()
    {
        
       
            if(auth()->user()->role_id==4){
                $data = Student::all()
                        ->where('supervisor_id',auth()->user()->supervisor->id);
            }
            else if(auth()->user()->role_id==3){
                $data = Student::all()
                        ->where('recruiter_id',auth()->user()->recruiter->id);
            }
            else if(auth()->user()->role_id==5){
                $data=DB::table('program_student')
                        ->join('students','program_student.student_id','=','students.id')
                        ->join('programs','program_student.program_id','=','programs.id')
                        ->where('school_info_id',auth()->user()->school->id)
                        ->groupby('student_id')
                        ->get();
                        
              $data=json_decode($data,true);
              
              $data= Student::hydrate($data);
              return view('layouts.backend.users.student-list1', compact('data'));
             
            }
            else{
                $data = Student::all();
            }
        
          
            
        return view('layouts.backend.users.student-list', compact('data'));
    }


    public function studentList(){
       

        if(auth()->user()->role_id==4){
        $data = Student::all()
                ->where('supervisor_id',auth()->user()->supervisor->id);
                
        }
        else if(auth()->user()->role_id==3){
        $data = Student::all()
                ->where('recruiter_id',auth()->user()->recruiter->id);
        }
        else if(auth()->user()->role_id==5){
            $data=DB::table('program_student')
                ->join('students','program_student.student_id','=','students.id')
                ->join('programs','program_student.program_id','=','programs.id')
                ->where('school_info_id',auth()->user()->school->id)
                ->groupby('student_id')
                ->get();
                        
            $data=json_decode($data,true);
            
            $data= Student::hydrate($data);
                        

        }
        
        else{
        $data = Student::all();
        }
        //dd( $data);
        //dd($data);

        return DataTables::of($data)
            ->editColumn('registered_at', function($data){
                $date= strtotime($data->created_at);
                return date('d-M-Y | h:i A', $date);
            })
            ->editColumn('activated_at', function($data){
                $date= strtotime($data->updated_at);
                return date('d-M-Y | h:i A', $date);
            })
            ->addColumn('action', function($data){
                return $data;
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);           

        return view('layouts.backend.users.student-list');
    }


    

    public function recruiterIndex()
    {
        $datas = Recruiter::all();
        //dd($datas);
        return view('layouts.backend.users.recruiter-list', compact('datas'));
    }


    public function recruiterList(){
       
        $data = Recruiter::all();
        //dd($data);

        return DataTables::of($data)
            ->editColumn('registered_at', function($data){
                $date= strtotime($data->created_at);
                return date('d-M-Y | h:i A', $date);
            })
            ->editColumn('activated_at', function($data){
                $date= strtotime($data->updated_at);
                return date('d-M-Y | h:i A', $date);
            })
            ->addColumn('action', function($data){
                return $data;
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);           

        return view('layouts.backend.users.student-list');
    }

    public function schoolIndex()
    {
            $datas = SchoolInfo::all();
  
        return view('layouts.backend.users.school-list', compact('datas'));
    }


    public function schoolList(){
       
       
        $data = SchoolInfo::all();
      
        //dd($data);

        return DataTables::of($data)
            ->editColumn('registered_at', function($data){
                $date= strtotime($data->created_at);
                return date('d-M-Y | h:i A', $date);
            })
            ->editColumn('activated_at', function($data){
                $date= strtotime($data->updated_at);
                return date('d-M-Y | h:i A', $date);
            })

            ->addColumn('action', function($data){
                return $data;
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);           

        return view('layouts.backend.users.school.list');
    }
    public function programIndex($id)
    {
            $programs = Program::all();
            if(auth()->user()->role_id==2)
            {
                $student = Student::find(auth()->user()->student->id);
            }
        $student = Student::find($id);
        return view('layouts.backend.users.program-list', compact('programs','student'));
    }

    public function programRecruiter($id)
    {
            $programs = Program::all();
            $student = Student::findOrFail($id);
  
        return view('layouts.backend.users.program-list', compact('programs','student'));
    }


}
