<?php

namespace App\Http\Controllers;

use App\User;
use App\comment;
use App\Student;
use App\Supervisor;
use App\Models\Recruiter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notifications\ProgramComment;
use Illuminate\Support\Facades\Notification;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comment = new comment();
        $comment->comment_details = $request->comment_details;
        $comment->commented_by = auth()->user()->id;
        $comment->commented_at = date('Y-m-d H:i:s');
        $comment->comment_group_id= $request->student_id;

        $comment->program_id = $request->program_id;
        $comment->save();
        

        $commented_by = auth()->user()->name;

        if(auth()->user()->role_id==2 ){
            $supervisor_id= auth()->user()->student->supervisor_id;
            $user_id= Supervisor::findOrFail($supervisor_id)->user_id;
            $user=User::findOrFail($user_id);

            
            $program_name= $comment->program->course_name;
            $student_id= auth()->user()->student->id;
            $program_id= $comment->program->id;
        }

        if(auth()->user()->role_id==3 ){
            $supervisor_id=Student::findOrFail($request->student_id)->supervisor_id;
            $user_id= Supervisor::findOrFail($supervisor_id)->user_id;
            $user=User::findOrFail($user_id);

            
            $program_name= $comment->program->course_name;
            $student_id= $request->student_id;
            $program_id= $comment->program->id;
        }

        if(auth()->user()->role_id==4){
            $recruiter_id=Student::findOrFail($request->student_id)->recruiter_id;
            
            if($recruiter_id==null){
                $user_id=Student::findOrFail($request->student_id)->user_id;
                $user=User::findOrFail($user_id);
            }else{
                $user_id=Recruiter::findOrFail($recruiter_id)->user_id;
                $user=User::findOrFail($user_id);
            }

            
            $program_name= $comment->program->course_name;
            $student_id= $request->student_id;
            $program_id= $comment->program->id;
        }
        
        Notification::send($user, new ProgramComment($commented_by,$student_id,$program_id,$program_name));
      
        return back()->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(comment $comment)
    {
        //
    }

    
}
