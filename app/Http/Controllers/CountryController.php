<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\CountryImage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = User::where('role_id', '4')->where('status', '1')->orderBy('id', 'desc')->with('district')->get();
        return view('layouts.backend.country.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $existTitle = Country::where('title', 'LIKE', '%'.$request->input('title'))->pluck('title')->first();
        if($existTitle){
            return back()->with('error', 'Country name alredy exist');
        }

        $input = new Country();
        $input = $request->except(['_token', 'country_img']);
        $input['created_by'] = Auth::user()->id;
        $input['updated_at'] = null;
        Country::create($input);
        $countryId = Country::orderBy('id', 'desc')->pluck('id')->first();
        $uploadedFiles = $request->file('country_img');
        // dd($countryId);
        if ($uploadedFiles != null){
            foreach($uploadedFiles as $file){
                $originalName = $file->getClientOriginalName();
                $filename = time().'.'.$originalName;
                $file->move('country-img/', $filename);

                $imgInput['country_id'] = $countryId;
                $imgInput['img_name'] = $filename;
                $imgInput['created_by'] = Auth::user()->id;
                $imgInput['updated_at'] = null;;
                CountryImage::create($imgInput);
            }
        }
        return back()->with('success', 'Successfully Added');
    }


    public function getAllCountry(){

        $data = Country::orderBy('title', 'asc')->get();

        return DataTables::of($data)

        ->editColumn('title', function($data){
            return $data->title;
        })
        ->editColumn('code', function($data){
            return $data->code;
        })
        ->editColumn('created_at', function($data){
            $date= strtotime($data->created_at);
            return date('d-M-Y | H:i', $date);
        })
        ->editColumn('updated_at', function($data){
            $date= strtotime($data->updated_at);
            return date('d-M-Y | H:i', $date);
        })
        ->addColumn('action', function($data){
            return $data;
        })
        ->addIndexColumn()
        ->make(true);

    }

    public function getCountryInfo(Request $request){

        $id=$request->input('id');
        $data = Country::where('id', $id)->first();
        $img = CountryImage::where('country_id', $id)->get()->pluck('img_name');
        return json_encode([$data, $img]);
    }

    public function show(Country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        if(auth()->user()->role_id == 1){
            return view('layouts.backend.country.edit', compact('country'));
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->input('title') != Country::where('id', $id)->pluck('title')->first()){
            $existTitle = Country::where('title', 'LIKE', '%'.$request->input('title'))->pluck('title')->first();
            if($existTitle){
                return back()->with('error', 'Country name alredy exist');
            }
        }

        $input = new Country();
        $input = $request->except(['_token', '_method', 'country_img']);
        $input['updated_by'] = auth()->user()->id;
        Country::where('id', $id)->update($input);

        $uploadedFiles = $request->file('country_img');
        if ($uploadedFiles != null){
            CountryImage::where('country_id', $id)->delete();
            foreach($uploadedFiles as $file){
                $originalName = $file->getClientOriginalName();
                $filename = time().'.'.$originalName;
                $file->move('country-img/', $filename);

                $imgInput['country_id'] = $id;
                $imgInput['img_name'] = $filename;
                $imgInput['created_by'] = Auth::user()->id;
                $imgInput['updated_at'] = null;;
                CountryImage::create($imgInput);
            }
        }

        return redirect()->route('country.index')->with('success', 'Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        //
    }
}
