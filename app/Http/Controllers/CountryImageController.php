<?php

namespace App\Http\Controllers;

use App\Models\CountryImage;
use Illuminate\Http\Request;

class CountryImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CountryImage  $countryImage
     * @return \Illuminate\Http\Response
     */
    public function show(CountryImage $countryImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CountryImage  $countryImage
     * @return \Illuminate\Http\Response
     */
    public function edit(CountryImage $countryImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CountryImage  $countryImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CountryImage $countryImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CountryImage  $countryImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(CountryImage $countryImage)
    {
        //
    }
}
