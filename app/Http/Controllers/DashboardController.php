<?php

namespace App\Http\Controllers;
use DB;
use App\Models\SchoolInfo;
use App\Models\Delivery;
use App\Models\role;
use App\User;
use App\Program;
use App\Student;
use App\Supervisor;
use App\Models\Recruiter;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    // After Login Components
    public function index()
    {
        if(auth()->user()->role_id==1 && auth()->user()->status==1){

            $user=User::all();
            $student=Student::orderBy('created_at', 'desc')->get();
            $recruiter=Recruiter::all();
            $supervisor=Supervisor::all();
            $school=Schoolinfo::orderBy('created_at', 'desc')->get();
            $program=Program::all();

            return view('layouts.backend.dashboard.dashboard',compact('user','student','recruiter','supervisor','school','program'));
        }

        if(auth()->user()->role_id==2 && auth()->user()->status==1){
            $student=Student::findOrFail(auth()->user()->student->id);

            return view('layouts.backend.dashboard.dashboard', compact('student'));
        }

        if(auth()->user()->role_id==3 && auth()->user()->status==1){
            $recruiter=Recruiter::findOrFail(auth()->user()->recruiter->id);

            return view('layouts.backend.dashboard.dashboard', compact('recruiter'));
        }

        if(auth()->user()->role_id==5 && auth()->user()->status==1){
            $school_student=DB::table('program_student')
            ->join('students','program_student.student_id','=','students.id')
            ->join('programs','program_student.program_id','=','programs.id')
            ->where('school_info_id',auth()->user()->school->id)

            ->get();

            $school_student=json_decode($school_student,true);

            $school_student= Student::hydrate($school_student);

            //$school=SchoolInfo::findOrFail(auth()->user()->school->id);

            $school_programs = Program::all()->where('school_info_id',auth()->user()->school->id);
            return view('layouts.backend.dashboard.dashboard', compact('school_student','school_programs'));
        }

        if(auth()->user()->role_id==4 && auth()->user()->status==1){
            $supervisor=Supervisor::findOrFail(auth()->user()->supervisor->id);

            return view('layouts.backend.dashboard.dashboard', compact('supervisor'));
        }


        elseif(auth()->user()->role_id != 1 && auth()->user()->status == 2 || auth()->user()->status == 3|| auth()->user()->status == 4 ){
            // return abort(403, 'Your account is not activate.');
            return view('layouts.backend.dashboard.not-active');
        }
    }



    // public function userManage($id){
    //     if(auth()->user()->role_id == 1){

    //         $deliveries = Delivery::where('created_by', $id)->count();
    //         $delivered = Delivery::where('created_by', $id)
    //                             ->where('delivery_status', 3)
    //                             ->count();
    //         $pending = Delivery::where('created_by', $id)
    //                             ->whereNotIn('delivery_status', [3])
    //                             ->count();

    //         $paidCount = Delivery::where('created_by', $id)
    //                             // ->where('delivery_status', 3)
    //                             ->where('paid_status', 1)
    //                             ->count();
    //         $unpaidCount = Delivery::where('created_by', $id)
    //                             // ->where('delivery_status', 3)
    //                             ->where('paid_status', 2)
    //                             ->count();
    //         $collection = Delivery::where('created_by', $id)
    //                            ->where('paid_status', 2)
    //                         //    ->whereIn('delivery_status', [2,3])
    //                            ->sum('collect_amount');

    //         $charge = Delivery::where('created_by', $id)
    //                         //    ->whereIn('delivery_status', [2,3])
    //                            ->where('paid_status', 2)
    //                            ->sum('total_charge');

    //         $unpaid = $collection - $charge;

    //         $getCharge = Delivery::where('created_by', $id)
    //                             // ->where('delivery_status', 3)
    //                             ->where('paid_status', 1)
    //                             ->sum('total_charge');

    //         $paidCollection = Delivery::where('created_by', $id)
    //                             ->where('paid_status', 1)
    //                             ->sum('collect_amount');

    //         $paidCharge = Delivery::where('created_by', $id)
    //                             ->where('paid_status', 1)
    //                             ->sum('total_charge');

    //         $paid = $paidCollection - $paidCharge;
    //         // dd($collection, $charge, $paidCollection, $paidCharge);
    //         return view('layouts.backend.merchants.manage', compact('deliveries', 'delivered', 'pending', 'charge', 'unpaid', 'paid', 'id', 'paidCount', 'unpaidCount', 'getCharge'));
    //     }
    //     return back();
    // }

}
