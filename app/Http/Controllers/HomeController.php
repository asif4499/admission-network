<?php

namespace App\Http\Controllers;

use App\Models\AboutUs;
use App\Models\AddRateInfo;
use App\Models\Client;
use App\Models\Country;
use App\Models\CountryImage;
use App\Models\District;
use App\Models\InterCityRate;
use App\Models\SchoolInfo;
use App\Models\Service;
use App\Models\ValuableClient;
use App\Program;
use Dotenv\Regex\Result;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        return view('layouts.frontend.home');
    }

    public function student(){
        return view('layouts.frontend.students');
    }

    public function school(){
        return view('layouts.frontend.schools');
    }

    public function recruiter(){
        return view('layouts.frontend.recruiters');
    }

    public function about(){
        return view('layouts.frontend.about-us');
    }

    public function contact(){
        return view('layouts.frontend.contact');
    }

    public function program($id){
        $data = Program::where('school_info_id', $id)->get()->toArray();

        return view('layouts.frontend.programs');
    }

    public function studyDestination(){
        $datas = SchoolInfo::leftJoin('countries', 'school_infos.country_code', '=', 'countries.id')
                            ->leftJoin('programs', 'school_infos.id', '=', 'programs.school_info_id')
                            ->leftJoin('campuses', 'school_infos.id', '=', 'campuses.school_info_id')
                            ->whereIn('school_infos.id', ['2', '3'])
                            ->orWhereIn('school_infos.country_code', ['1', '2'])
                            ->orWhereIn('programs.id', ['1'])
                            ->orWhereIn('campuses.city', ['Dhaka'])
                            ->orWhereBetween('programs.tution_free', [900, 40001])
                            ->get();

        return view('layouts.frontend.study-destination');
    }

    public function studySearchResult(Request $request){

        $input = $request->input();
        $country = isset($input['country']) ? $input['country'] : [];
        $programm = isset($input['programme']) ? $input['programme'] : [];
        $university = isset($input['university']) ? $input['university'] : [];
        $city = isset($input['city']) ? $input['city'] : [];
        $fee = explode(";", $input['fee']);

        $data = SchoolInfo::leftJoin('countries', 'school_infos.country_code', '=', 'countries.id')
                            ->leftJoin('programs', 'school_infos.id', '=', 'programs.school_info_id')
                            ->leftJoin('campuses', 'school_infos.id', '=', 'campuses.school_info_id')
                            ->whereIn('school_infos.id', $university)
                            ->orWhereIn('school_infos.country_code', $country)
                            ->orWhereIn('programs.id', $programm)
                            ->orWhereIn('campuses.city', $city)
                            ->orWhereBetween('programs.tution_free', [$fee['0'], $fee['1']])
                            ->orderBy('school_infos.school_name')
                            ->select([
                                'school_infos.id as school_id',
                                'school_infos.school_name',
                                'campuses.address',
                                'countries.title as country',
                            ])
                            ->get()
                            ->toArray();

        return DataTables::of($data)
            ->addColumn('data', function ($data) {
                $result = [
                    $data['school_id'],
                    $data['school_name'],
                    $data['address'],
                    $data['country']
                ];
                return $result;
            })
            ->make(true);

    }

    public function countryDetails(Request $request){
        $id = $request->input('country-id');
        $data = Country::where('id', $id)->first();
        $images = CountryImage::where('country_id', $id)->get()->pluck('img_name')->toArray();

        return view('layouts.frontend.country-details', compact('data', 'images'));
    }
}

