<?php

namespace App\Http\Controllers;
use App\Program;
use App\Student;
use App\User;
use App\Models\SchoolInfo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Request as Request1;



class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programs = Program::all();

    return view('layouts.backend.users.program1-list', compact('programs'));
    }

    public function programAdd()
    {
        $programs = Program::all()
                        ->where('school_info_id',auth()->user()->school->id);

        return view('layouts.backend.program.program-index', compact('programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=new Program;
        $input = $request->except(['_token']);
        $school_id=SchoolInfo::all()
                        ->where('user_id',auth()->user()->id)
                        ->first()->id;
        $input['school_info_id'] = $school_id;

        Program::create($input);


        return back()->with('success', 'Successfully Added Program');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student=Student::findOrFail(Request1::get('student_id'));
        $user=User::all();
        $program = Program::findOrFail($id);
        if(Request1::get('not_id')){
            $userUnreadNotification=auth()->user()->unreadNotifications->where('id',Request1::get('not_id'))->first();

            if($userUnreadNotification) {
                $userUnreadNotification->markAsRead();
            }
        }

        return view('layouts.backend.program.show', compact('program','student','user'));
    }

    public function showforall($id)
    {

        //$student=Student::findOrFail(Request1::get('student_id'));
        $user=User::all();
        $program = Program::findOrFail($id);

        return view('layouts.backend.program.showforall', compact('program','user'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit(Program $program)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Program $program)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy(Program $program)
    {
        //
    }

    public function programApply($id,$student_id)
    {
        $program = Program::findOrFail($id);
        if(auth()->user()->role_id==2){
            $student_id= Student::all()
                            ->where ('user_id',auth()->user()->id)
                            ->first()->id;
        }



        $program ->students()->attach($student_id,['created_at'=>date('Y-m-d H:i:s'),'status'=>'Application Placed']);

       return back()->with('success', 'Successfully Applied To the Program');
    }

    public function assignStatus(Request $request,$id,$student_id)
    {
        $program = Program::findOrFail($id);

        $program->students()->updateExistingPivot($student_id,['status'=>$request->status]);

        return back()->with('success', 'Successfully Status Updated');
    }


}
