<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Models\Recruiter;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Student;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class RecruiterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.frontend.forms.recruiter-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $input = new Recruiter();
        
        //File Uploads Start
        // $files1=$request->allFiles();
        // $all_files=array_keys($files1);
        // foreach($all_files as $file_item){
        //         if ($request->file($file_item))
        //         {
        //             $file = $request->file($file_item);
        //             $ext = $file->getClientOriginalExtension();
        //             $filename = time().'.'.$file_item.'.'.$ext;
        //             $file->move('student files/', $filename);
        //             $input[$file_item] = $filename; 
        //         }
        //     }
        $input = $request->except(['_token']);
        
        //$input['created_by'] = auth()->student()->id;
        //$input['updated_at'] = null;
        //Student()->student()->create($input);
        

        $input['user_id']= Auth::id();
        $user = User::findOrFail(Auth::id());
        $user->status=2;
        $user->save();
        
        Recruiter::create($input);
        
        

        return redirect()->route('login')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Recruiter  $recruiter
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recruiter = Recruiter::findOrFail($id);
        $students= Student:: all()
                    ->where('recruiter_id',$id);
        return view('layouts.backend.recruiter.show',compact('recruiter','students'));
    }


    public function studentListForRecruiter(Request $request ){
       
        
        $data = Student::all()
                ->where('recruiter_id',$request->input('id'));
        //dd($data);

        return DataTables::of($data)
            ->editColumn('registered_at', function($data){
                $date= strtotime($data->created_at);
                return date('d-M-Y | h:i A', $date);
            })
            ->editColumn('activated_at', function($data){
                $date= strtotime($data->updated_at);
                return date('d-M-Y | h:i A', $date);
            })

            ->addColumn('action', function($data){
                return $data;
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);           

        return view('layouts.backend.recruiter.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Recruiter  $recruiter
     * @return \Illuminate\Http\Response
     */
    public function edit(Recruiter $recruiter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Recruiter  $recruiter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recruiter $recruiter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Recruiter  $recruiter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recruiter $recruiter)
    {
        //
    }
}
