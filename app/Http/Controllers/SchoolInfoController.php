<?php

namespace App\Http\Controllers;

use App\Models\SchoolInfo;
use App\User;
use App\Campus;
use App\Program;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class SchoolInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id=SchoolInfo::all()
                        ->where('user_id',auth()->user()->id)
                        ->first()->id;
        $campuses = Campus::all()
                    ->where('school_info_id',$school_id);
        $programs = Program::all()
                    ->where('school_info_id',$school_id);

        return view('layouts.frontend.forms.school-afterlogin-form',compact('campuses','programs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     $validatedData = $request->validate([
    //         'name' => 'unique:users',
    //         'email' => 'unique:users',
    //         'phone' => 'unique:users',
    //     ]);

    //     $input = new SchoolInfo();
    //     $input = $request->except(['_token','password-confirm','password']);


    //     $user = new User();
    //     $user->name = $request->school_name;
    //     $user->email = $request->contact_email;
    //     $user->phone = $request->contact_number;
    //     $user->address = $request->address;
    //     $user->password = Hash::make($request->password);
    //     $user->status=3;
    //     $user->save();

    //     $input['user_id']= $user->id;
    //     SchoolInfo::create($input);



    //     return redirect()->route('login')->with('success', 'Successfully Added');
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SchoolInfo  $schoolInfo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $school = SchoolInfo::findOrFail($id);
        $campuses = Campus::all()
                ->where('school_info_id',$school->id);
        $programs = Program::all()
                ->where('school_info_id',$school->id);
        if(auth()->user()->role_id==2){
            $student = Student::find(auth()->user()->student->id);
        }else{
            $student = null;
        }

        return view('layouts.backend.school.show', compact('school','campuses','programs','student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SchoolInfo  $schoolInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolInfo $schoolInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SchoolInfo  $schoolInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $school_id=SchoolInfo::all()
                        ->where('user_id',auth()->user()->id)
                        ->first()->id;

        $input = $request->except(['_token', '_method']);
        $input['institution_type'] = $request->institution_type;
        $input['no_of_campus'] = $request->no_of_campus;
        $input['logo_name'] = $request->logo_name;
        $input['certificate'] = $request->certificate;
        SchoolInfo::where('id', $school_id)->update($input);

        $user = User::find(auth()->user()->id);
        $user->status = 2;
        $user->save();

        return redirect()->route('login')->with('success', 'Successfully Registered');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SchoolInfo  $schoolInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolInfo $schoolInfo)
    {
        //
    }
}
