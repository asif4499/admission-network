<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Student;
use App\User;
use App\Supervisor;
use App\Program;
use App\Campus;
use App\Models\Recruiter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses=Program::select('course_name')->distinct()->get();
        $campuses=Campus::select('state')->distinct()->get();

        if(auth()->user()->role_id==3){
            return view('layouts.frontend.forms.additional-form',compact('courses','campuses'));
        }
        elseif(auth()->user()->role_id!=3){
            return view('layouts.frontend.forms.student-form',compact('courses','campuses'));
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $input = new Student();
        
        //File Uploads Start
        $files1=$request->allFiles();
        $all_files=array_keys($files1);
        foreach($all_files as $file_item){
                if ($request->file($file_item))
                {
                    $file = $request->file($file_item);
                    $ext = $file->getClientOriginalExtension();
                    $filename = time().'.'.$file_item.'.'.$ext;
                    $file->move('student files/', $filename);
                    $input[$file_item] = $filename; 
                }
            }
        $input = $request->except(['_token']);
        
        //$input['created_by'] = auth()->student()->id;
        //$input['updated_at'] = null;
        //Student()->student()->create($input);
        if(auth()->user()->role_id!=3){
            $input['user_id']= Auth::id();
            $user = User::findOrFail(Auth::id());
            $user->status=2;
            $user->save();
            $input['email']=$user->email;
            $input['gender']=$user->gender;
            $input['mobile']=$user->mobile;
            $input['address']=$user->address;
        }
        elseif(auth()->user()->role_id==3){
            
            $input['user_id']= 0;
            
            $recruiter = Recruiter::all()
                                ->where('user_id',auth()->user()->id) 
                                ->first();
                               
            $input['recruiter_id']=$recruiter->id;
        }
        
        
        Student::create($input);
   
        

        return redirect()->route('login')->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
       
        $student=Student::findOrFail($id);
        $supervisors = Supervisor :: all();
       
        return view('layouts.backend.student.show', compact('student','supervisors'));
    }

    public function showProfile($id)
    {  
        if(auth()->user()->id==$id){
            $student=Student::all()
                        ->where('user_id',$id)->first();
                       
            $supervisors = Supervisor :: all();
        
            return view('layouts.backend.student.show', compact('student','supervisors'));
        }
        return back()->with('danger', 'Invalid Link');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }
    public function userApproval($id)
    {
        $user=User::findOrFail($id);
        if($user->status!=1)
        {
            $user->status = 1;
            $user->save();
            return back()->with('success', 'User has been Activated');
        }
        else{
            $user->status = 4;
            $user->save();
            return back()->with('error', 'User has been Deactivated');
        }
     
 
    }
    public function assignSupervisor(Request $request,$id)
    {
        $student=Student::findOrFail($id);
        $student->supervisor_id = $request->supervisor_id;
        $student->save();
        return back()->with('success', 'Successfully Assigned Supervisor');
    }
}
