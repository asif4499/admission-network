<?php

namespace App\Http\Controllers;
use App\User;
use App\Student;
use Illuminate\Support\Facades\Hash;
use App\Supervisor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class SupervisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.backend.supervisor.admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'unique:users',
            'email' => 'unique:users',
            'phone' => 'unique:users',
        ]);
        $user = new Supervisor();
        $input = $request->except(['_token','password_confirmation']);
        $input['password'] = Hash::make($request->password);
        $input['created_by'] = auth()->user()->id;
        $input['updated_at'] = null;

        $user = new User();
        $user->name= $request->name;
        $user->email= $request->email;
        $user->phone= $request->phone;
        $user->gender= $request->gender;
        $user->password= Hash::make($request->password);
        $user->address= $request->address;
        $user->role_id= 4;
        $user->status= 1;
            
        $user->save();
   
        $input['user_id'] = $user->id;
     
        Supervisor::create($input);

        return back()->with('success', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supervisor = Supervisor::findOrFail($id);
        $student_count =  Student::all()->where('supervisor_id',$id)->count();
        
       
        return view('layouts.backend.supervisor.show', compact('supervisor','student_count'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function edit(Supervisor $supervisor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supervisor $supervisor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supervisor $supervisor)
    {
        //
    }


    public function studentListForSupervisor(Request $request ){
       
        
        $data = Student::all()
                ->where('supervisor_id',$request->input('id'));
        //dd($data);
        
        return DataTables::of($data)
            ->editColumn('registered_at', function($data){
                $date= strtotime($data->created_at);
                return date('d-M-Y | h:i A', $date);
            })
            ->editColumn('activated_at', function($data){
                $date= strtotime($data->updated_at);
                return date('d-M-Y | h:i A', $date);
            })

            ->addColumn('action', function($data){
                return $data;
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);           

        return view('layouts.backend.supervisor.show');
    }

}
