<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use App\Models\SchoolInfo;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'name' => 'unique:users',
            'email' => 'unique:users',
            'phone' => 'unique:users',
        ]);
       $user = new User();
       $input = $request->except(['_token','password-confirm']);
       $input['password'] = Hash::make($request->password);
       $input['status'] = 3;
   
    //    $input['created_by'] = auth()->user()->id;
    //    $input['updated_at'] = null;
        //$user->school()->create([
          //  'school_name' => 'Eastern',
         //   'email' => 'all.imam.asif@gmail.com',
       // ]);
       User::create($input);

       return back()->with('success', 'Successfully Added');
    }

    public function storeSchool(Request $request)
    {
        $validatedData = $request->validate([
            'school_name' => 'unique:users,name',
            'contact_email' => 'unique:users,email',
            'contact_number' => 'unique:users,phone',
        ]);
         
        $input = new SchoolInfo();
        $input = $request->except(['_token','password-confirm','password']);
        
        
        $user = new User();
        $user->name = $request->school_name;
        $user->email = $request->contact_email;
        $user->phone = $request->contact_number;
        $user->address = $request->address;
        $user->password = Hash::make($request->password);
        $user->status=3;
        $user->role_id=5;
        $user->save();

        $input['user_id']= $user->id;
        SchoolInfo::create($input);
   
        

        return redirect()->route('login')->with('success', 'Successfully Added');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function registerInstitute()
    {
        return view('auth.registerInstitute');
    }

  

}
