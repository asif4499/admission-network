<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Throwable;

class Country extends Model
{
    protected $guarded = [];

    public static function getAllCountriesArray()
    {
        try {
            // Validate the value...
            $country = Country::all();
            if (count($country) > 0){
                foreach ($country as $item) {
                    $data[$item->id] = $item->title;
                }

                return $data;
            }

        } catch (Throwable $e) {
            return false;
        }
    }

    public function countryImg(){
        return $this->hasMany(CountryImage::class, 'country_id', 'id');
    }
}
