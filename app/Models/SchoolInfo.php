<?php

namespace App\Models;

use App\Campus;
use App\Program;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Throwable;

class SchoolInfo extends Model
{
    protected $guarded = [];

    public function school(){
        return $this->belongsTo(User::class);
    }
    public function campuses(){
        return $this->hasMany(Campus::class);
    }
    public function progrmas(){
        return $this->hasMany(Program::class);
    }

    public static function getAllSchoolInfosArray()
    {
        try {
            // Validate the value...
            $schoolInfo = SchoolInfo::all();
            if (count($schoolInfo) > 0){
                foreach ($schoolInfo as $item) {
                    $data[$item->id] = $item->school_name;
                }

                return $data;
            }

        } catch (Throwable $e) {
            return false;
        }
    }
}

