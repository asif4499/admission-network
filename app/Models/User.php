<?php

namespace App;

use App\Models\AboutUs;
use App\Models\AboutUsCard;
use App\Models\District;
use App\Models\Role;
use App\Models\SchoolInfo;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    const SuperAdmin = '1';
    const School = '2';
    const Student = '3';
    const Recruiter = '4';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password', 'shop_name', 'address', 'phone', 'role_id', 'district_id', 'code'
    // ];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function school()
    {
        return $this->hasOne(SchoolInfo::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    public function student()
    {
        return $this->hasOne('App\Student');
    }
    public function recruiter()
    {
        return $this->hasOne('App\Models\Recruiter');
    }
    public function supervisor()
    {
        return $this->hasOne('App\Supervisor');
    }
}
