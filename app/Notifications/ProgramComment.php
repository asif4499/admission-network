<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ProgramComment extends Notification
{
    use Queueable;
    public $myCommentDetails;
    public $myStudent_id;
    public $myProgram_id;
    public $myProgram_name;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($commentDetails,$student_id,$program_id,$program_name)
    {   
        
        $this->myCommentDetails = $commentDetails;
        $this->myStudent_id = $student_id;
        $this->myProgram_id = $program_id;
        $this->myProgram_name = $program_name;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toDatabase($notifiable)
    {
        return [
            'commented_by'=>$this->myCommentDetails,
            'student_id'=>$this->myStudent_id,
            'program_id'=>$this->myProgram_id,
            'program_name'=>$this->myProgram_name
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
