<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Expr\Throw_;
use Throwable;

class Program extends Model
{
    protected $guarded = [];

    public function schoolInfo()
    {
        return $this->belongsTo('App\Models\SchoolInfo');
    }
    public function students()
    {
        return $this->belongsToMany('App\Student')->withPivot('created_at','updated_at','status');
    }
    public function comments()
    {
        return $this->hasmany('App\comment');
    }

    public static function getAllProgrammesArray()
    {
        try {
            // Validate the value...
            $program = Program::all();
            if (count($program) > 0){
                foreach ($program as $item) {
                    $data[$item->id] = $item->course_name;
                }

                return $data;
            }

        } catch (Throwable $e) {
            return false;
        }
    }

    public static function getAllMaxFeesArray()
    {
        try {
            $program = Program::max('tution_free');
            return $program;
        } catch (Throwable $e) {
            return false;
        }
    }

    public static function getAllMinFeesArray()
    {
        try {
            $program = Program::min('tution_free');
            return $program;
        } catch (Throwable $e) {
            return false;
        }
    }
}
