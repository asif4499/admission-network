<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $guarded = [];
      
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function user1()
    {
        return $this->hasOne('App\User');
    }
    public function recruiter()
    {
        return $this->belongsTo('App\Models\Recruiter');
    }
    public function programs()
    {
        return $this->belongsToMany('App\Program')->withPivot('created_at','updated_at','status');;
    }
    public function supervisor()
    {
        return $this->belongsTo('App\supervisor');
    }
}

    