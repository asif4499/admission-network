<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class comment extends Model
{
    use Notifiable;
    public function program()
    {
        return $this->belongsTo('App\Program');
    }
    
}
