<?php

use App\Campus;
use App\Models\Country;
use App\Models\SchoolInfo;
use App\Program;


function countries(){
    return $data['countries'] = Country::getAllCountriesArray();
}

function programmes(){
    return $data['programmes'] = Program::getAllProgrammesArray();
}

function universities(){
    return $data['universities'] = SchoolInfo::getAllSchoolInfosArray();
}

function cities(){
    return $data['cities'] = Campus::getAllCitiesArray();
}

function maxFees(){
    return $data['maxFees'] = Program::getAllMaxFeesArray();
}

function minFees(){
    return $data['minFees'] = Program::getAllMinFeesArray();
}
