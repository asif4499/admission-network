<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {

            $table->integer('user_id')->unsigned();
            
            $table->string('supervisor_id')->nullable();
            $table->string('recruiter_id')->nullable();

            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->integer('gender')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->string('emergency_contact')->nullable();
            $table->string('address')->nullable();
            $table->string('house_flat_no')->nullable();
            $table->string('street')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('city')->nullable();
            $table->integer('country_of_residence')->nullable();
            $table->integer('country_of_birth')->nullable();
            $table->integer('nationality')->nullable();
            $table->integer('ethnicity')->nullable();
            $table->integer('religion')->nullable();
           

            $table->date('start_study')->nullable();
            $table->string('applying_course')->nullable();
            $table->integer('preferred_country')->nullable();
            $table->integer('preferred_campus_location')->nullable();
            $table->integer('applied_student_visa_befor')->nullable();
            $table->string('student_visa_details')->nullable();
            $table->integer('declaration_of_criminal_record')->nullable();
            $table->string('criminal_record_details')->nullable();

            $table->string('prof_pic_upload')->nullable();
            $table->string('passport_nid_upload')->nullable();
            $table->string('cv_upload')->nullable();
            $table->string('highest_education_certificates_upload')->nullable();
            $table->string('bachelor_degree_certificates_upload')->nullable();
            $table->string('hsc_certificates_upload')->nullable();
            $table->string('ssc_certificates_upload')->nullable();
            $table->string('purpose_of_study_statement')->nullable();
            $table->string('proof_of_english_language')->nullable();
            $table->string('proof_of_address')->nullable();
            $table->string('others_document')->nullable();

            $table->integer('accuracy_of_information')->nullable();
            $table->integer('read_info_flag')->nullable();
            $table->string('comments')->nullable();
            $table->string('Aditional_info')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
