<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecruitersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recruiters', function (Blueprint $table) {
            
            $table->integer('user_id')->unsigned();
            $table->bigIncrements('id');

           

            $table->string('company_name')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('facebook_page_name')->nullable();
            $table->string('instagram_handle')->nullable();
            $table->string('twitter_handle')->nullable();
            $table->string('linkdin_url')->nullable();

            $table->string('password')->nullable(); 

            $table->integer('main_source_of_student')->nullable();
            $table->string('legal_first_name')->nullable();
            $table->string('legal_last_name')->nullable();
            $table->string('street_address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->integer('country')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('phone')->nullable();
            $table->string('cellphone')->nullable();
            $table->string('skype_id')->nullable();
            $table->string('whatsapp_id')->nullable();
            $table->string('referred_info')->nullable();

            $table->string('begin_recruiting_student')->nullable();
            $table->integer('servie_to_clint')->nullable();
            $table->string('inistitutions_representing')->nullable();
            $table->string('educational_associations')->nullable();
            $table->integer('recruit_from')->nullable();
            $table->string('student_count_per_year')->nullable();
            $table->integer('marketing_method')->nullable();
            $table->integer('average_service_free')->nullable();
            $table->integer('estimated_number')->nullable();
            $table->string('additional_comments')->nullable();
            $table->string('reference_name')->nullable();
            $table->string('reference_inistitution_name')->nullable();
            $table->string('reference_business_email')->nullable();
            $table->string('reference_phone')->nullable();
            $table->string('reference_website')->nullable();

            $table->string('company_logo')->nullable();
            $table->string('business_certificate')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recruiters');
    }
}
