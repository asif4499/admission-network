<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupervisorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supervisors', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->bigIncrements('id');

            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->integer('gender')->nullable();
            $table->integer('nationality')->nullable();
            $table->string('password')->nullable();
            $table->string('address')->nullable();
            $table->string('office_address')->nullable();
            $table->string('region')->nullable();
            $table->string('note')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supervisors');
    }
}
