<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_info_id')->nullable();

            $table->string('course_name')->nullable();
            $table->string('programe_level')->nullable();
            $table->string('intakes')->nullable();
            $table->string('course_duration')->nullable();
            $table->string('seasons')->nullable();
            $table->float('tution_free')->nullable();
            $table->float('admission_free')->nullable();
            $table->string('language_profeciency')->nullable();
            $table->string('required_exam_type')->nullable();
            $table->string('required_visa_type')->nullable();
            $table->string('required_grading_scheme')->nullable();
            $table->string('required_grade_scale')->nullable();
            $table->string('required_grade_avg')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
