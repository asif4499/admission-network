/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function(config) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	//config.uiColor = '#ff0000';
	/*config.toolbarGroups = [
    { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
    //{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
    //{ name: 'links' },
    { name: 'insert' },
    //{ name: 'tools' },
    //{ name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
    { name: 'others' },
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
    { name: 'styles' },
    { name: 'colors' },
    //{ name: 'about' }
];
    config.toolbar = [
        { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
    ];*/

    config.toolbar = [
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] , items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ]},
        //{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
        { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
        { name: 'insert', items: [  'Table', 'HorizontalRule', 'SpecialChar' ] },
       // { name: 'tools', items: [ 'Maximize' ] },
        //{ name: 'document', groups: [ 'mode', 'document', 'doctools' ]},
        { name: 'others', items: [ '-' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike','Subscript','Superscript', '-', 'RemoveFormat', 'Underline' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
        '/',
        { name: 'styles', items: [ 'Styles', 'Format' ] },
        { name: 'colors' },
        { name: 'Font', items:['Font']},
        { name: 'FontSize', items:['FontSize']},
        { name: 'TextColor', items:['TextColor','BGColor']}

        //{ name: 'about', items: [ 'About' ] }
    ];
	



    config.fontSize_defaultLabel = '12px';
config.fontSize_sizes = '10/10px;12/12px;14/14px;';  
config.extraPlugins = 'wordcount';

config.wordcount = {

    // Whether or not you want to show the Paragraphs Count
    showParagraphs: true,

    // Whether or not you want to show the Word Count
    showWordCount: true,

    // Whether or not you want to show the Char Count
    showCharCount: true,

    // Whether or not you want to count Spaces as Chars
    countSpacesAsChars: true,

    // Whether or not to include Html chars in the Char Count
    countHTML: false,
    
    // Maximum allowed Word Count
    //maxWordCount: 4,

    // Maximum allowed Char Count
	maxCharCount: 4000
};

config.enterMode = CKEDITOR.ENTER_P;
//config.autoParagraph = true;



//config.extraPlugins = 'pramukhime';


};



