@extends('layouts.auth-app')

@section('content')
    <div class="card card-login">
        <form method="POST" action="{{ route('user.store') }}">
            @csrf
            <div style="margin-left: 0px; margin-right: 0px" class="card-header card-header-info text-center">
                <h4 class="card-title">Register</h4>
                <div class="social-line">
                  {{-- <a target="_blank" href="https://www.facebook.com/city.express.bd.cm" class="btn  btn-link">
                    <i class="material-icons">facebook</i>
                    Connect with our Facebook page
                  </a> --}}
                  {{-- <a href="#pablo" class="btn btn-just-icon btn-link">
                    <i class="fa fa-google-plus"></i>
                  </a> --}}
                </div>
            </div>
              <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">face</i>
                              </span>
                            </div>
                            <input id="name" type="text" placeholder="Enter Name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">mail</i>
                              </span>
                            </div>
                            <input id="email" type="email" placeholder="Enter Email" 
                            class="form-control @error('email') is-invalid @enderror" 
                            name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">phone</i>
                              </span>
                            </div>
                            <input id="phone" type="number" placeholder="Enter Phone (11 Digits)" 
                            class="form-control @error('phone') is-invalid @enderror" name="phone" 
                            value="{{ old('phone') }}" required>

                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">supervised_user_circle</i>
                              </span>
                            </div>
                            <select class="role1 form-control @error('gender') is-invalid @enderror"
                             name="gender" required>
                                <option></option>
                                {{-- <option value="4">Customer</option> --}}
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                            @error('gender')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                  </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">home</i>
                            </span>
                            </div>
                            <input id="address" type="text" placeholder="Enter Address" 
                            class="form-control @error('address') is-invalid @enderror" 
                            name="address" value="{{ old('address') }}" required>

                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">how_to_reg</i>
                            </span>
                            </div>
                            {{-- <input id="role" type="text" placeholder="Enter Address" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required> --}}
                            <select class="role2 form-control @error('rol_id') is-invalid @enderror" 
                            name="role_id" required>
                                <option></option>
                                {{-- <option value="4">Customer</option> --}}
                                <option value="2">Student</option>
                                <option value="3">Recruiter</option>
                            </select>
                            @error('role_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">lock_outline</i>
                              </span>
                            </div>
                            <input id="password" type="password" 
                            placeholder="Enter Password" 
                            class="form-control @error('password') is-invalid @enderror" 
                            name="password" required autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">lock_outline</i>
                              </span>
                            </div>
                            <input id="password-confirm" type="password" 
                            placeholder="Confirm Password"  class="form-control @error('password-confirm') is-invalid @enderror" 
                            name="password-confirm" required autocomplete="new-password">

                            @error('password-confirm')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <span id='message'></span>
                    </div>
                    
                </div>

                <div class="input-group-text">
                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-info">
                                {{ __('Register') }}
                            </button>
                            or
                            <a style="color: #f44336" href="{{ route('login') }}"><b>Login</b></a>
                        </div>
                    </div>
                </div>
                  {{-- <div class="footer text-center">
                    <a href="#pablo" class="btn btn-info btn-link btn-wd btn-lg">Get Started</a>
                  </div> --}}
                </form>
              </div>





<script>
$('#password, #password-confirm').on('keyup', function () {
  if ($('#password').val() == $('#password-confirm').val()) {
    $('#message').html('Password Matched').css('color', 'green');
  } else 
    $('#message').html('Password Do Not Matching').css('color', 'red');
});

var password = document.getElementById("password")
  , confirm_password = document.getElementById("password-confirm");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>
@endsection
