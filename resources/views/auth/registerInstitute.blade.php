@extends('layouts.frontend.layouts.app')
@section('content')
<section style="margin-top:120px; background-color: #0064e1; color:white">
  <div class="container">
      <div class="row">
     

          <div class="col-md-12">
              <h3 style="color: white">
                  WORK WITH ADMISSION NETWORK
              </h3>
              <h1 style="color: white">
                  Partnership Request
              </h1>
              <h5 style="color: white; font-weight:300">
                  Complete the form below and our Partner Relations Team will be in touch soon.
              </h5>

              @include('layouts.frontend.forms.school-form')

          </div>
      </div>
  </div>
</section>

@endsection




@push('scripts')

<script>
    $('#password, #password-confirm').on('keyup', function () {
      if ($('#password').val() == $('#password-confirm').val()) {
        $('#message').html('Password Matched').css('color', 'green');
      } else 
        $('#message').html('Password Do Not Matching').css('color', 'red');
    });
    
    var password = document.getElementById("password")
      , confirm_password = document.getElementById("password-confirm");
    
    function validatePassword(){
      if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
      } else {
        confirm_password.setCustomValidity('');
      }
    }
    
    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>

<script>
    function myFunction() {
      var x = document.getElementById("myDIV");
      if (x.style.display === "block") {
        x.style.display = "none";
      } else {
        x.style.display = "block";
      }
    }
</script>

@endpush
