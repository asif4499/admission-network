@extends('layouts.backend.layouts.app')
@section('content')

<div class="container-fluid">
<div class="row">
    <div class="card">
      {{-- <div class="card-header card-header-text card-header-primary">
          <div class="card-text">
            <h4 class="card-title">Here is the Text</h4>
          </div>
      </div> --}}
      <div class="card-body table-responsive">


<div class="card-body table-responsive" >
    <button type="button" class="btn btn-info  pull-right" 
      data-toggle="modal" data-target="#exampleModalLongTitle" >
      Add Campus</button>
      <div class="clearfix"></div>
</div>
<div class="table-responsive">
    <table class="table table-bordered table-hover" 
     width="100%">
     <thead class="card-header-info no-shadow">
          <tr style="text-align: center" >
            <th scope="col">SL</th>
            <th scope="col" >Address</th>
            <th scope="col" >Zip</th>
            <th scope="col" >City</th>
            <th scope="col" >State</th>
            <th scope="col"  >Country</th>
            <th scope="col"  >Action</th>
          </tr>
        </thead>
        <tbody>
            @php
            $i = 1
            @endphp
     @foreach ($campuses as $campus) 
        
            <tr style="text-align: center" >
                <td>{{$i++}}</td>
                <td>{{$campus->address}}</td>
                <td>{{$campus->zip}}</td>
                <td>{{$campus->city}}</td>                        
                <td>{{$campus->state}}</td>
                <td>{{$campus->country}}</td>
                <td class="td-actions">1
                    {{-- <a class="btn btn-primary btn-link" href="{{ route('admin-client.edit', $row->id) }}">
                        <i class="material-icons">edit</i>
                    </a>

                    <form class="form-delete" action="{{ route('admin-client.destroy', $row->id) }}" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" title="Delete" class="delete btn btn-danger btn-link">
                            <i class="material-icons">close</i>
                          </button>
                    </form> --}}


                  </td>
            </tr>
    @endforeach
          </tr>
        </tbody>
      </table>
    </div>
</div>
      </div>
    </div>
</div>
</div>
@include('layouts.frontend.forms.create1-modal')
@endsection