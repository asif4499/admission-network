<div class="modal fade create" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    {{-- <div class="modal-dialog modal-lg modal-dialog-centered"> --}}
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Add Country</h5>
            <button style="color: white" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>


        <div class="modal-body">
            <form action="{{route('country.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label for="countryName">Country Name <span style="color: red">*</span></label>
                            <br>
                            <input id="countryName" name="title" type="text" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <div class="form-group">
                            <label for="countryName">Country Code</label>
                            <br>
                            <input id="countryCode" name="code" type="text" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <div class="form-group">
                            <label for="aboutCountry">About Country</label>
                            <br>
                            <textarea name="details" class="form-control ckeditor" id="aboutCountry" rows="5" spellcheck="false"></textarea>
                            </div>
                        </div>
                        <br>
                        <div style="text-align: left" class="col-md-12">
                            <label for="countryImg">Image</label>
                            <input id="countryImg" name="country_img[]" style="width: 100%" type="file" multiple>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info pull-right">Create</button>
                    <div class="clearfix"></div>
            </form>
        {{-- form End--}}

        </div>
      </div>
    </div>
  </div>
