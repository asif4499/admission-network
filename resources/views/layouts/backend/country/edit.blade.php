@extends('layouts.backend.layouts.app')
@section('content')
    <div class="container-fluid">
      <div class="row">
        {{-- <div class="col-md-8"> --}}
          <div class="card">
            <div class="card-header card-header-info">
              <h4 class="card-title">Update Country Info</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('country.update', $country->id) }}" method="POST" enctype="multipart/form-data">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                <label for="countryName">Country Name <span style="color: red">*</span></label>
                                <br>
                                <input value="{{$country->title}}" id="countryName" name="title" type="text" class="form-control" autocomplete="off" required>
                                </div>
                            </div>
                            <br>
                            <div class="col-md-12">
                                <div class="form-group">
                                <label for="countryName">Country Code</label>
                                <br>
                                <input value="{{$country->code}}" id="countryCode" name="code" type="text" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <br>
                            <div class="col-md-12">
                                <div class="form-group">
                                <label for="aboutCountry">About Country</label>
                                <br>
                                <textarea name="details" class="form-control ckeditor" id="aboutCountry" rows="5" spellcheck="false">
                                    {{$country->details}}
                                </textarea>
                                </div>
                            </div>
                            <br>
                            <div style="text-align: left" class="col-md-12">
                                <label for="countryImg">Image</label>
                                <input id="countryImg" name="country_img[]" style="width: 100%" type="file" multiple>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-info pull-right">Update</button>
                        <a class="btn btn-danger pull-right" href="{{route('country.index')}}">Back</a>
                        {{-- <button type="submit" class="btn btn-danger pull-right">Update</button> --}}
                        <div class="clearfix"></div>
                </form>
            </div>
          </div>
        {{-- </div> --}}
      </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset('backend/js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('backend/js/ckeditor/adapters/jquery.js') }}"></script>
<script>
     $('.ckeditor').ckeditor();
</script>
@endpush
