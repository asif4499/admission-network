@extends('layouts.backend.layouts.app')
@section('content')
    <div class="container-fluid">
      <div class="row">
        {{-- <div class="col-md-8"> --}}
          <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Country List</h4>
              </div>
            <div class="card-body">
                <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target=".create">Create New Country</button>
                <div class="clearfix"></div>
                <br>


                <table class="table table-bordered" class="display nowrap" id="get-country-table"  width="100%">
                    <thead style="background: linear-gradient(60deg, #26c6da, #00acc1); color: white;">
                    <tr style="text-align: center">
                        <th scope="col">SL</th>
                        <th scope="col">Title</th>
                        <th scope="col">Code</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Updated At</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
          </div>
        {{-- </div> --}}
      </div>
    </div>


    <div class="modal fade delete" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <p>Are you sure you want to delete this?</p>
            </div>

            <div class="modal-footer">
                {{-- <form action="{{route('country.destroy')}}" method="POST"> --}}
                    @csrf
                    <input type="text" name="id" id="delete" value="" hidden>
                        <button type="button" class="btn btn-sm btn-default pull-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-danger">Yes, Delete</button>
                </form>

            </div>
          </div>
        </div>
    </div>
{{-- @include('layouts.backend.country.delete-modal') --}}
@include('layouts.backend.country.create-modal')
@include('layouts.backend.country.view')

@endsection

@push('scripts')
    <!--check editor js start-->
    <script src="{{ asset('backend/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('backend/js/ckeditor/adapters/jquery.js') }}"></script>
    <script>
         $('.ckeditor').ckeditor();

    $(function() {
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        $('#get-country-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '{{ url('get-country-list') }}',
            columns: [
                { data: 'DT_RowIndex', name: 'id', orderable: false },
                { data: 'title', name: 'title', orderable: false },
                { data: 'code', name: 'code', orderable: false },
                { data: 'created_at', name: 'created_at', orderable: false },
                { data: 'updated_at', name: 'updated_at', orderable: false },
                {data: 'action', name: 'action', orderable: false,
                        render:
                            function(data, type, row) {
                                // return '<span class="btn btn-primary" id="modalView" onclick="journalView('+data+')">Details</span>';
                                return '<div style="text-align:center" class="action-btn">'+
                                            '<span class="btn btn-rose btn-sm" id="modalView" onclick="countryView('+row.id+')">View</span>'+
                                            '<a class="btn btn-info btn-sm" href="country/'+row.id+'/edit">Edit</a>'+
                                            // '<a class="btn btn-rose btn-link" href="/country/'+row.id+'">Delete</a>'+
                                            // '<span class="btn btn-danger btn-sm" data-target=".delete" onclick="countryDelete('+row.id+')">Delete</span>'+
                                        '</div>';
                            }
                },
            ],
        });

    });



    function countryView(data){
        $.ajax({
                url: '{{ route("get-country-info") }}',
                type: 'post',
                data: {'id': data },
                success: function (response) {

                    var obj = jQuery.parseJSON(response);
                    console.log(data);

                    // varTitle = $('<textarea />').html(obj.details).text();

                    $("#c-name").text(obj['0'].title);
                    $("#c-code").text(obj['0'].code);
                    $("#c-about").html(obj['0'].details).text();
                    $("#c-image").empty();
                    $.each(obj['1'], function() {
                    $("#c-image").append("<img style='width:40%;margin:.5em' class='rounded img-fluid' src='country-img/" + this + "' alt='' />");
                    });
                    $('#myModal').modal({
                        show: true,
                    });
                }
            });

        }
    </script>
@endpush
