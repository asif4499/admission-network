  <div class="modal fade create bd-example-modal-lg" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          {{-- <h3 class="modal-title">Delivery Information</h3> --}}
          {{-- <a class="btn btn-rose btn-sm" href="{{route('deliveries.print', '1')}}">Print only delivery Info</a>
          <a class="btn btn-danger btn-sm" href="{{route('deliveries.pickup.print', '2')}}">Print with pickup Info</a> --}}
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
          </button>
        </div>
        <div class="modal-body">
            <div style="width: 80%; margin:auto" class="row">
                <p class="btn-link">Country Details</p>
                <table style="font-size: .9rem" class="table table-bordered">
                    <tbody style="text-align: left">
                      <tr>
                        <td style="font-weight: bold">Country Name</td>
                        <td>
                            <span id="c-name"></span>
                        </td>
                      </tr>

                      <tr>
                        <td style="font-weight: bold">Code</td>
                        <td>
                            <span id="c-code"></span>
                        </td>
                      </tr>

                      <tr>
                        <td style="font-weight: bold">Country About</td>
                        <td>
                            <span id="c-about"></span>
                        </td>
                      </tr>

                      <tr>
                        <td style="font-weight: bold">Country Image</td>
                        <td style="text-align: center">
                            <span id="c-image"></span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


