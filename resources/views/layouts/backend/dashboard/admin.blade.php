<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-header card-header-warning card-header-icon">
          <div class="card-icon">
            <i class="material-icons">device_hub</i>
          </div>
          <p class="card-category" style="margin-right:20px ">Total User </p>
          <h3 class="card-title" style="margin-right:20px ">
            {{$user->count()}}
          </h3>
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons text-danger">link</i>
            <a href="javascript:;">See List</a>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-header card-header-success card-header-icon">
          <div class="card-icon">
            <i class="material-icons">store</i>
          </div>
          <p class="card-category" style="margin-right:20px ">Recruiter Count</p>
          <h3 class="card-title" style="margin-right:20px ">
          {{$recruiter->count()}}
          </h3>
        </div>
        <div class="card-footer">
            <div class="stats">
              <i class="material-icons text-danger">link</i>
              <a href="{{route('merchants.index')}}">See List</a>
            </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-header card-header-info card-header-icon">
          <div class="card-icon">
            <i class="material-icons">how_to_reg</i>
          </div>
          <p class="card-category" style="margin-right:20px ">Student Count</p>
          <h3 class="card-title" style="margin-right:20px ">
            {{$student->count()}}
          </h3>
        </div>
        <div class="card-footer">
            <div class="stats">
              <i class="material-icons text-danger">link</i>
              <a href="{{route('customers.index')}}">See List</a>
            </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-header card-header-danger card-header-icon">
          <div class="card-icon">
            <i class="material-icons" >admin_panel_settings</i>
          </div>
          <p class="card-category" style="margin-right:20px">Superviser Count</p>
          <h3 class="card-title" style="margin-right:20px">
            {{$supervisor->count()}}
          </h3>
        </div>
        <div class="card-footer">
            <div class="stats">
              <i class="material-icons text-danger">link</i>
            <a href="{{route('role.index')}}">See List</a>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="card card-chart">
        <div class="card-header card-header-success">
          <div class="ct-chart" id="dailySalesChart"></div>
        </div>
        <div class="card-body">
          <h4 class="card-title">Student Details</h4>
          <p class="card-category">

            Total Student count: {{$student->count()}}

          </p>
          <p class="card-category">
            Student Directly Applied: {{$student->where('recruiter_id',null)->count()}}
          </p>
          <p class="card-category">
            Student Applied via Recruiter: {{$student->where('recruiter_id','!=',null)->count()}}
          </p>

        </div>
        <div class="card-footer">
          <div class="stats">
            Last Register at: <i class="material-icons">access_time</i>
            @if($student->first()==null)
            No Data Yet
            @else
            {{$student->first()->created_at->format('dS M,Y h:i:s a')}}
            @endif
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-chart">
        <div class="card-header card-header-warning">
          <div class="ct-chart" id="websiteViewsChart"></div>
        </div>
        <div class="card-body">
          <h4 class="card-title">Institution And Program</h4>
          <p class="card-category">Institution Count: {{$school->count()}}</p>
          <p class="card-category">Total Program: {{$program->count()}}</p>
          <p class="card-category">Total Applications on Programs:
            @php($i=0)
            @foreach($student as $stud)

              @php($i=$i+$stud->programs()->count())

            @endforeach
          {{$i}}
          </p>
        </div>
        <div class="card-footer">
          <div class="stats">
            Last Register at: <i class="material-icons">access_time</i>
            @if($school->first()==null)
            No Data Yet
            @else
           {{$school->first()->created_at->format('dS M,Y h:i:s a')}}
           @endif
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-chart">
        <div class="card-header card-header-danger">
          <div class="ct-chart" id="completedTasksChart"></div>
        </div>
        <div class="card-body">
          <h4 class="card-title">Applied Program Status</h4>
          <p class="card-category">
            @php($i=0)
            @foreach($student as $stud)
              @php($i=$i+$stud->programs()->where('status','Under Processing')->count())
            @endforeach
            Under Processing : {{$i}}
          </p>
          <p class="card-category">
            @php($i=0)
            @foreach($student as $stud)
              @php($i=$i+$stud->programs()->where('status','Need More Document')->count())
            @endforeach
            Need More Document: {{$i}}
          </p>
          <p class="card-category">
            @php($i=0)
            @foreach($student as $stud)
              @php($i=$i+$stud->programs()->where('status','Accepted By Institute')->count())
            @endforeach
            Accepted By Institute : {{$i}}
          </p>
          <p class="card-category">
            @php($i=0)
            @foreach($student as $stud)
              @php($i=$i+$stud->programs()->where('status','Rejected By Institute')->count())
            @endforeach
            Rejected By Institute : {{$i}}
          </p>
        </div>
        <div class="card-footer">
          <div class="stats">
            Last Accepted at: <i class="material-icons">access_time</i>
            @php($i=0)
            @php($j=0)

            @foreach($student as $stud)
            @if($stud->programs()->orderBy('updated_at','desc')->where('status','Accepted By Institute')->count()!=0)
              @php($j=$stud->programs()->orderBy('updated_at','desc')->where('status','Accepted By Institute')->first()->pivot->updated_at->format('dS M,Y h:i:s a'))

            @endif
            @if($j>$i)
              @php($i=$j)
            @endif
            @endforeach
            @if($i==0)
            No Data Yet
            @else
            {{$i}}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
