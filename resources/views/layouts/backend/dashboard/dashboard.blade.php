@extends('layouts.backend.layouts.app')
@section('content')
    @if (auth()->user()->role_id == 1)
        @include('layouts.backend.dashboard.admin')
    @endif
    @if (auth()->user()->role_id == 2)
        @include('layouts.backend.dashboard.student')
    @endif
    @if (auth()->user()->role_id == 3)
        @include('layouts.backend.dashboard.recruiter')
    @endif
    @if (auth()->user()->role_id == 5)
        @include('layouts.backend.dashboard.school')
    @endif

    @if (auth()->user()->role_id == 4)
        @include('layouts.backend.dashboard.supervisor')
    @endif
@endsection
