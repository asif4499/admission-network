  <div class="row">
    <div class="col-md-4">
      <div class="card card-chart">
        <div class="card-header card-header-info">
          <div class="ct-chart" id="dailySalesChart"></div>
        </div>
        <div class="card-body">
          <h4 class="card-title">Total TEST <b>0</b></h4>
            {{-- <h5 class="card-title">0</h5> --}}
          <p class="card-category">
            <span class="text-success"></span> Total TEST</p>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-chart">
        <div class="card-header card-header-info">
          <div class="ct-chart" id="websiteViewsChart"></div>
        </div>
        <div class="card-body">
            <h4 class="card-title">Total TEST <b>0</b></h4>
          <p class="card-category">Total TEST</p>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-chart">
        <div class="card-header card-header-info">
          <div class="ct-chart" id="completedTasksChart"></div>
        </div>
        <div class="card-body">
          <h4 class="card-title">Total TEST <b>0</b></h4>
          <p class="card-category">Total TEST</p>
        </div>
      </div>
    </div>
    {{-- <div class="col-md-3">
        <div class="card card-chart">
          <div class="card-header card-header-info">
            <div class="ct-chart" id="completedTasksChart"></div>
          </div>
          <div class="card-body">
            <h5 class="card-title">Paid After Delivery <b>{{$paidCount}}</b></h5>
            <h5 class="card-title">Unpaid After Delivery <b>{{$unpaidCount}}</b></h5>
          </div>
        </div>
      </div> --}}
  </div>


  <div class="row">
    @if(auth()->user()->role_id == 1)
    <div class="col-md-4">
        <div class="card card-chart">
          <div class="card-header card-header-info">
            <div class="ct-chart" id="dailySalesChart"></div>
          </div>
          <div class="card-body">
          @if(auth()->user()->role_id == 1)
              {{-- <h4 class="card-title">Payable Delivery Charge <b>0</b> TK</h4> --}}
              <h4 class="card-title">Total TEST <b>{{$getCharge}}</b> TK</h4>
          @endif
          </div>
        </div>
      </div>
      @endif
    <div class="col-md-4">
      <div class="card card-chart">
        <div class="card-header card-header-info">
          <div class="ct-chart" id="dailySalesChart"></div>
        </div>
        <div class="card-body">
        @if(auth()->user()->role_id != 1)
            <h4 class="card-title">Total TEST <b>0</b> TK</h4>
        @elseif(auth()->user()->role_id == 1)
            <h4 class="card-title">Total TEST <b>0</b> TK</h4>
            {{-- <h4 class="card-title">Payment Complete Till Now <b>{{$getCharge}}</b> TK</h4> --}}
        @endif
          <p class="card-category">
            @if(auth()->user()->role_id != 1)
                Total TEST
            @endif
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-chart">
        <div class="card-header card-header-info">
          <div class="ct-chart" id="websiteViewsChart"></div>
        </div>
        <div class="card-body">
          <h4 class="card-title">Total TEST <b>0</b> TK</h4>
          @if(auth()->user()->role_id == 3)
            <p class="card-category">Total TEST</p>
          @elseif(auth()->user()->role_id == 4)
            <p class="card-category">Total TEST</p>
          @endif
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-chart">
        <div class="card-header card-header-info">
          <div class="ct-chart" id="completedTasksChart"></div>
        </div>
        <div class="card-body">
           <h4 class="card-title">Total TEST <b>0</b> TK</h4>
           @if(auth()->user()->role_id == 3)
            <p class="card-category">Total TEST</p>
           @elseif(auth()->user()->role_id == 4)
            <p class="card-category">Total TEST</p>
           @endif
        </div>
      </div>
    </div>
  </div>

@if(auth()->user()->role_id != 1)
  @php
  $i = 1;
 @endphp
 <div class="card">
    <div class="card-body table-responsive">
     <h3>Last 10 Test</h3>
        <table class="table table-bordered table-hover" data-form="deleteForm">
            <thead class="card-header-primary no-shadow">
            <tr style="text-align: center">
                <th scope="col">TEST</th>
                <th scope="col">TEST</th>
                <th scope="col">TEST</th>
                <th scope="col">TEST</th>
                <th scope="col">TEST</th>
                <th scope="col">TEST</th>
                <th scope="col">TEST</th>
                <th scope="col">TEST</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            </tr>
            </tbody>
        </table>
    </div>
</div>
@endif
