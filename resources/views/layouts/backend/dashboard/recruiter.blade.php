<div class="row">
    <div class="col-md-4">
      <div class="card card-stats">
        <div class="card-header card-header-warning card-header-icon">
          <div class="card-icon">
            <i class="material-icons">supervisor_account</i>
          </div>
          <p class="card-category" style="margin-right:20px ">Total Student</p>
          <h3 class="card-title" style="margin-right:20px ">
            @if($recruiter->students()->count()!=0)
            {{$recruiter->students()->count()}}</h3>
            @endif
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons text-danger">link</i>
            <a href="#pablo">List</a>
          </div>
        </div>
      </div>
      {{-- <div class="card card-chart">
        <div class="card-header card-header-info">
          <h4>Number of Programs Applied On </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6" style="text-align: right">
                    
                </div>
                <div class="col-md-6" style="text-align: left">
                    <h4 class="card-title" 
                    style="font-size:  28px; text-align: right;"><b>{{$student->programs()->count()}}</b></h4>
                </div>
            </div>
          
            
          <p class="card-category">
            <span class="text-success"></span> Till Now</p>
        </div>
      </div> --}}
    </div>

    <div class="col-md-4">
      <div class="card card-stats">
        <div class="card-header card-header-primary card-header-icon">
          <div class="card-icon">
            <i class="material-icons">last_page</i>
          </div>
          <p class="card-category" style="margin-right:20px ">Last Added New Student On</p>
          <h3 class="card-title" style="margin-right:20px ">
            @if($recruiter->students()->count()!=0)
            {{$recruiter->students()->orderBy('id','desc')->first()->created_at->format('dS M,Y')}}
          @endif</h3>
        </div>
        <div class="card-footer">
          <div class="stats">
            At:@if($recruiter->students()->count()!=0)
              {{$recruiter->students()->orderBy('id','desc')->first()->created_at->format('h:i:s a')}}
            @endif
          </div>
        </div>
      </div>
        {{-- <div class="card card-chart">
          <div class="card-header card-header-info">
            <h4>Last Applied On </h4>
          </div>
          <div class="card-body">
              <div class="row">
                  <div class="col-md-12" style="text-align: left">
                  <h4 class="card-title" style="font-size: 28px; text-align: right;">
                  
                    @if($student->programs()->count()!=0)
                      <b>{{$student->programs()->orderBy('id','asc')->first()->pivot->created_at}}</b>
                    @endif
                    </h4>
                  </div>
              </div>
            
              
            <p class="card-category">
              <span class="text-success"></span> Till Now</p>
          </div>
        </div> --}}
      </div>


      <div class="col-md-4">
        <div class="card card-stats">
          <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
              <i class="material-icons">leaderboard</i>
            </div>
            <p class="card-category" style="margin-right:20px ">Total Application placed</p>
            <h3 class="card-title" style="margin-right:20px ">
            @php
                  $i=0;
                  
              @endphp
              @if($recruiter->students()->count()!=0)
              @foreach ($recruiter->students as $student)
              @php
                 $i=$i+$student->programs()->count();
              @endphp
                
              @endforeach
              @endif

              {{$i}}
          </h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <div class="stats">
                <i class="material-icons text-danger">link</i>
                <a href="#pablo">List</a>
              </div>
            </div>
          </div>
        </div>