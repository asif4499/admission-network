<div class="row">
    <div class="col-md-4">
      <div class="card card-stats">
        <div class="card-header card-header-warning card-header-icon">
          <div class="card-icon">
            <i class="material-icons">supervisor_account</i>
          </div>
          <p class="card-category" style="margin-right:20px ">Total Student Applied</p>
          <h3 class="card-title" style="margin-right:20px ">
            
            {{$school_student->groupby('student_id')->count()}}</h3>
        
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons text-danger">link</i>
            <a href="#pablo">List</a>
          </div>
        </div>
      </div>
      {{-- <div class="card card-chart">
        <div class="card-header card-header-info">
          <h4>Number of Programs Applied On </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6" style="text-align: right">
                    
                </div>
                <div class="col-md-6" style="text-align: left">
                    <h4 class="card-title" 
                    style="font-size:  28px; text-align: right;"><b>{{$student->programs()->count()}}</b></h4>
                </div>
            </div>
          
            
          <p class="card-category">
            <span class="text-success"></span> Till Now</p>
        </div>
      </div> --}}
    </div>

    <div class="col-md-4">
      <div class="card card-stats">
        <div class="card-header card-header-primary card-header-icon">
          <div class="card-icon">
            <i class="material-icons">how_to_reg</i>
          </div>
          <p class="card-category" style="margin-right:20px ">Total Application Placed</p>
          <h3 class="card-title" style="margin-right:20px ">
        
            {{$school_student->count()}}
       
        </div>
        <div class="card-footer">
          <div class="stats">
           
            <i class="material-icons text-danger">link</i>
            <a href="#pablo">List</a>
         
          </div>
        </div>
      </div>
        {{-- <div class="card card-chart">
          <div class="card-header card-header-info">
            <h4>Last Applied On </h4>
          </div>
          <div class="card-body">
              <div class="row">
                  <div class="col-md-12" style="text-align: left">
                  <h4 class="card-title" style="font-size: 28px; text-align: right;">
                  
                    @if($student->programs()->count()!=0)
                      <b>{{$student->programs()->orderBy('id','asc')->first()->pivot->created_at}}</b>
                    @endif
                    </h4>
                  </div>
              </div>
            
              
            <p class="card-category">
              <span class="text-success"></span> Till Now</p>
          </div>
        </div> --}}
      </div>


      <div class="col-md-4">
        <div class="card card-stats">
          <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
              <i class="material-icons">leaderboard</i>
            </div>
            <p class="card-category" style="margin-right:20px ">Total Programs Offered</p>
            <h3 class="card-title" style="margin-right:20px ">
                {{$school_programs->count()}}
          </h3>
          </div>
          <div class="card-footer">
         
              <div class="stats">
                <i class="material-icons text-danger">link</i>
                <a href="#pablo">List</a>
              </div>
            </div>
       
        </div>
      </div>

@foreach ($school_programs as $program)

    <div class="col-md-6">
        <div class="card card-stats">
        <div class="card-header card-header-danger card-header-icon">
            <div class="card-icon">
            <i class="material-icons">library_books</i>
            </div>
            <p class="card-category" style="margin-right:20px ">{{$program->course_name}}</p>
            <h3 class="card-title" style="margin-right:20px ">
               Applied Students : {{$school_student->where('program_id',$program->id)->count()}}
        </h3>
        </div>
        <div class="card-footer">
           
                <div class="stats">
                
                      Opened At - {{$program->created_at->format('d M,Y || h:m a')}}
              
                  </div>
         
        </div>
        </div>
    </div>
@endforeach
      