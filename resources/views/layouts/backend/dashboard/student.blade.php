<div class="row">
    <div class="col-md-4">
      <div class="card card-stats">
        <div class="card-header card-header-warning card-header-icon">
          <div class="card-icon">
            <i class="material-icons">spellcheck</i>
          </div>
          <p class="card-category" style="margin-right:20px ">Programs Applied On</p>
          <h3 class="card-title" style="margin-right:20px ">{{$student->programs()->count()}}</h3>
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons text-danger">link</i>
            <a href="#pablo">List</a>
          </div>
        </div>
      </div>
      {{-- <div class="card card-chart">
        <div class="card-header card-header-info">
          <h4>Number of Programs Applied On </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6" style="text-align: right">

                </div>
                <div class="col-md-6" style="text-align: left">
                    <h4 class="card-title"
                    style="font-size:  28px; text-align: right;"><b>{{$student->programs()->count()}}</b></h4>
                </div>
            </div>


          <p class="card-category">
            <span class="text-success"></span> Till Now</p>
        </div>
      </div> --}}
    </div>

    <div class="col-md-4">
      <div class="card card-stats">
        <div class="card-header card-header-primary card-header-icon">
          <div class="card-icon">
            <i class="material-icons">last_page</i>
          </div>
          <p class="card-category" style="margin-right:20px ">Last Applied On</p>
          <h3 class="card-title" style="margin-right:20px ">@if($student->programs()->count()!=0)
            {{$student->programs()->orderBy('id','desc')->first()->pivot->created_at->format('dS M,Y')}}
          @endif</h3>
        </div>
        <div class="card-footer">
          <div class="stats">
            At:@if($student->programs()->count()!=0)
              {{$student->programs()->orderBy('id','desc')->first()->pivot->created_at->format('h:i:s a')}}
            @endif
          </div>
        </div>
      </div>
        {{-- <div class="card card-chart">
          <div class="card-header card-header-info">
            <h4>Last Applied On </h4>
          </div>
          <div class="card-body">
              <div class="row">
                  <div class="col-md-12" style="text-align: left">
                  <h4 class="card-title" style="font-size: 28px; text-align: right;">

                    @if($student->programs()->count()!=0)
                      <b>{{$student->programs()->orderBy('id','asc')->first()->pivot->created_at}}</b>
                    @endif
                    </h4>
                  </div>
              </div>


            <p class="card-category">
              <span class="text-success"></span> Till Now</p>
          </div>
        </div> --}}
      </div>


      <div class="col-md-4">
        <div class="card card-stats">
          <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
              <i class="material-icons">sync</i>
            </div>
            <p class="card-category" style="margin-right:20px ">Application In Process</p>
            <h3 class="card-title" style="margin-right:20px ">
              @if($student->programs()->count()!=0)
                  {{$student->programs()->where('status','!=','Completed')->count()}}
              @endif
          </h3>
          </div>
<<<<<<< HEAD
          <div class="card-body">
              <div class="row">
                  <div class="col-md-12" style="text-align: left">
                      <h4 class="card-title" style="font-size: 28px; text-align: right;">
                      @if($student->programs()->count()!=0)
                        {{$student->programs()->where('status','!=','Completed')->count()}}
                      @endif
                      </h4>
                  </div>
              </div>

=======
          <div class="card-footer">
            <div class="stats">
              <div class="stats">
                <i class="material-icons text-danger">link</i>
                <a href="#pablo">List</a>
              </div>
            </div>
          </div>
        </div>
        {{-- <div class="card card-chart">
          <div class="card-header card-header-info">
            <h4>Number of Application In Process</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12" style="text-align: left">
                        <h4 class="card-title" style="font-size: 28px; text-align: right;">

                        @if($student->programs()->count()!=0)
                         <b> {{$student->programs()->where('status','!=','Completed')->count()}}</b>
                        @endif
                        </h4>
                    </div>
                </div>

>>>>>>> f5917fa08caf02bc28da431e13662708d37f07ef
            <p class="card-category">
              <span class="text-success"></span> Till Now</p>
          </div>
        </div> --}}
      </div>
  </div>



  <div class="row">
    @foreach($student->programs as $program)
    <div class="col-md-6">
      <div class="card card-stats">
        <div class="card-header card-header-danger card-header-icon">
          <div class="card-icon">
            <i class="material-icons">school</i>
          </div>
<<<<<<< HEAD
          <div class="card-body">
              <div class="row">
                  <div class="col-md-12" style="text-align: left">
                  <h4 class="card-title" style="font-size: 19px; text-align: right;"><b>Status: {{$program->pivot->status}}</b></h4>
                  </div>
              </div>

              {{-- <h5 class="card-title">0</h5> --}}
            <p class="card-category">
              <span class="text-success"></span>{{$program->pivot->created_at}}</p>
=======
          <p class="card-category" style="margin-right:20px ">{{$program->course_name}}</p>
          <h3 class="card-title" style="margin-right:20px ">
            Status: {{$program->pivot->status}}
        </h3>
        </div>
        <div class="card-footer">
          <div class="stats">
            <div class="stats">
              Last Updated On:
              {{$program->pivot->created_at->format('d(D) M,Y')}}
            </div>
>>>>>>> f5917fa08caf02bc28da431e13662708d37f07ef
          </div>
        </div>
      </div>
      </div>
      @endforeach
    </div>



