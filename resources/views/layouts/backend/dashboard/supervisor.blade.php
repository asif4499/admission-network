<div class="row">
    <div class="col-md-4">
      <div class="card card-stats">
        <div class="card-header card-header-warning card-header-icon">
          <div class="card-icon">
            <i class="material-icons">spellcheck</i>
          </div>
          <p class="card-category" style="margin-right:20px ">Total Student Assigned</p>
          <h3 class="card-title" style="margin-right:20px ">{{$supervisor->students()->count()}}</h3>
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons text-danger">link</i>
            <a href="#pablo">List</a>
          </div>
        </div>
      </div>
  
    </div>

    <div class="col-md-4">
      <div class="card card-stats">
        <div class="card-header card-header-primary card-header-icon">
          <div class="card-icon">
            <i class="material-icons">notification_important</i>
          </div>
          <p class="card-category" style="margin-right:20px ">Pending Notifications</p>
          <h3 class="card-title" style="margin-right:20px ">
            {{auth()->user()->unreadNotifications->where('read_at',null)->count()}}
          </h3>
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons text-danger">link</i>
            <a href="#pablo">List</a>
            
          </div>
        </div>
      </div>
  
      </div>


      <div class="col-md-4">
        <div class="card card-stats">
          <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
              <i class="material-icons">sync</i>
            </div>
            @php
                $i=0;
            @endphp
            <p class="card-category" style="margin-right:20px ">Total Program Applied On</p>
            <h3 class="card-title" style="margin-right:20px ">
     
              @foreach($supervisor->students()->get() as $student)
                @php($i=$i+$student->programs()->count())
              
              @endforeach
            {{$i}}
            </h3>
              
          </h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <div class="stats">
                <i class="material-icons text-danger">link</i>
                <a href="#pablo">List</a>
              </div>
            </div>
          </div>
        </div>

      </div>
  </div>


  
  <div class="row">
    <div class="col-md-3">
      <div class="card card-stats">
        <div class="card-header card-header-info card-header-icon">
          <div class="card-icon">
            <i class="material-icons">redo</i>
          </div>
          <p class="card-category" style="margin-right:20px ">Under Processing</p>
          <h3 class="card-title" style="margin-right:20px ">
            @php($i=0)
            @foreach ($supervisor->students()->get() as $student)
              @foreach ($student->programs()->get() as $program)
              @if($program->pivot->status=="Under Processing")
                @php($i=$i+1)
              @endif
              @endforeach
            @endforeach
          {{$i}}</h3>
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons text-danger">link</i>
            <a href="#pablo">List</a>
          </div>
        </div>
      </div>
  
    </div>

    <div class="col-md-3">
      <div class="card card-stats">
        <div class="card-header card-header-primary card-header-icon">
          <div class="card-icon">
            <i class="material-icons">text_snippet</i>
          </div>
          <p class="card-category" style="margin-right:20px ">Need Document</p>
          <h3 class="card-title" style="margin-right:20px ">
            @php($i=0)
                @foreach ($supervisor->students()->get() as $student)
                  @foreach ($student->programs()->get() as $program)
                  @if($program->pivot->status=="Need More Document")
                    @php($i=$i+1)
                  @endif
                  @endforeach
                @endforeach
              {{$i}}
          </h3>
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons text-danger">link</i>
            <a href="#pablo">List</a>
            
          </div>
        </div>
      </div>
  
      </div>


      <div class="col-md-3">
        <div class="card card-stats">
          <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
              <i class="material-icons">check_box</i>
            </div>
            
            <p class="card-category" style="margin-right:20px ">Accepted</p>
            <h3 class="card-title" style="margin-right:20px ">
              
              @php($i=0)
                @foreach ($supervisor->students()->get() as $student)
                  @foreach ($student->programs()->get() as $program)
                  @if($program->pivot->status=="Accepted By Institute")
                    @php($i=$i+1)
                  @endif
                  @endforeach
                @endforeach
              {{$i}}

            </h3>
              
        
          </div>
          <div class="card-footer">
            <div class="stats">
              <div class="stats">
                <i class="material-icons text-danger">link</i>
                <a href="#pablo">List</a>
              </div>
            </div>
          </div>
        </div>

      </div>

      <div class="col-md-3">
        <div class="card card-stats">
          <div class="card-header card-header-danger card-header-icon">
            <div class="card-icon">
              <i class="material-icons">cancel</i>
            </div>
       
            <p class="card-category" style="margin-right:20px ">Rejected</p>
            <h3 class="card-title" style="margin-right:20px ">
     
              @php($i=0)
              @foreach ($supervisor->students()->get() as $student)
                @foreach ($student->programs()->get() as $program)
                @if($program->pivot->status=="Rejected By Institute")
                  @php($i=$i+1)
                @endif
                @endforeach
              @endforeach
            {{$i}}

          </h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <div class="stats">
                <i class="material-icons text-danger">link</i>
                <a href="#pablo">List</a>
              </div>
            </div>
          </div>
        </div>

      </div>
  </div>

