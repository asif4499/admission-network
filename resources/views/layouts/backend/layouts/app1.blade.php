<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="{{asset('/frontend/img/LOGO-FINAL.png')}}">
  <link href="{{asset('/backend/css/select2.min.css')}}" rel="stylesheet" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>
    Admission Network
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  @include('layouts.backend.partials.global-styles')
  @stack('styles')

</head>

<body class="">
    <!-- preloader -->
<div class="preloader">
    <div class="status">
        <div class="status-mes"></div>
    </div>
</div>

  <div class="wrapper ">
    
   
      <div class="content">
        <div class="container-fluid">



            @yield('content')





        </div>
    </div>



<!--   Core JS Files   -->

<!--   Core JS Files   -->
@include('layouts.backend.partials.global-scripts')
@stack('scripts')

<script>
        $('.role').select2({
          placeholder: "Select an option",
          allowClear: true
        });
</script>
</body>

</html>