<script src="{{asset('backend/js/core/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/js/jquery-3.5.1.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/js/dataTables.responsive.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/js/checkAll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/js/plugins/moment.min.js')}}"></script>
<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
<script src="{{asset('backend/js/plugins/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
  <!--  Preloader    -->
<script src="{{asset('backend/js/resource_js_preloader.js')}}" type="text/javascript"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{asset('backend/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
<script src="{{asset('backend/js/material-kit.js?v=2.0.7')}}" type="text/javascript"></script>
<script src="{{asset('backend/js/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/js/select2.min.js')}}" type="text/javascript"></script>


<script>
  $(document).ready(function() {
    //init DateTimePickers
    materialKit.initFormExtendedDatetimepickers();

    // Sliders Init
    materialKit.initSliders();
  });


  function scrollToDownload() {
    if ($('.section-download').length != 0) {
      $("html, body").animate({
        scrollTop: $('.section-download').offset().top
      }, 1000);
    }
  }

</script>

<script>
    $('.select2').select2({
      placeholder: "Select an option",
      allowClear: true
    });
</script>

<script type="text/javascript">
    $('table[data-form="deleteForm"]').on('click', '.form-delete', function(e){
        e.preventDefault();
        var $form=$(this);
        $('#confirm').modal({ backdrop: 'static', keyboard: false })
            .on('click', '#delete-btn', function(){
                $form.submit();
            });
    });
</script>

@if (Session::has('success'))
    <script>
        toastr.success("{!!Session::get('success')!!}")
    </script>
@elseif (Session::has('error'))
    <script>
        toastr.error("{!!Session::get('error')!!}")
    </script>
@endif

