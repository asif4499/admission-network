<link rel="stylesheet" type="text/css" href="{{asset('/backend/css/google-font.css')}}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{asset('/backend/css/font-awesome.min.css')}}" rel="stylesheet" />
<!-- CSS Files -->
<link href="{{asset('/backend/css/material-dashboard.css?v=2.1.2')}}" rel="stylesheet" />
<link href="{{asset('/backend/css/toastr.min.css')}}" rel="stylesheet" />
<link href="{{asset('/backend/css/resource_css_preloader.css')}}" rel="stylesheet" />
<link href="{{asset('/backend/css/select2.min.css')}}" rel="stylesheet" />


<!-- CSS Just for demo purpose, don't include it in your project -->
<link href="{{asset('/backend/demo/demo.css')}}" rel="stylesheet" />
<link href="{{asset('/backend/css/jquery.dataTables.min.css')}}" rel="stylesheet" />
<link href="{{asset('/backend/css/responsive.dataTables.min.css')}}" rel="stylesheet" />
<link href="{{asset('/backend/css/custom.css')}}" rel="stylesheet" />
