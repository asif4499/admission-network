{{-- <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top "> --}}
<nav class="navbar navbar-expand-lg" >
    <div class="container-fluid" >
      <div class="navbar-wrapper">
            <h4 class="navbar-brand" >{{auth()->user()->name}}</h4>
            <p>{{auth()->user()->code}}</p>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="sr-only">Toggle navigation</span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <div>
            <hr>
        {{-- logout btn --}}
            <a class="dropdown-item t-res" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        {{-- logout btn --}}
        </div>
      </button>
      <div class="collapse navbar-collapse" >
        <ul class="navbar-nav ml-auto" >
           <li class="nav-item dropdown">
            <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="material-icons">notifications</i>
              <span class="notification" id=noti_number>{{auth()->user()->unreadNotifications->where('read_at',null)->count()}}</span> 
              <p class="d-lg-none d-md-block">
                Some Actions
              </p>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink" >
              
              @foreach(auth()->user()->unreadNotifications->where('read_at',null) as $notification)
              <a class="dropdown-item" 
              href={{route('program.show',[ 'program'=>$notification->data['program_id'],'student_id' => $notification->data['student_id'],'not_id'=>$notification->id] )}}>
                <div class="row" style="width: 400px">
                  <div class="col-md-12" style="text-align: left">
                  <b>{{$notification->data['commented_by']}}</b> Has Commented On <b>{{$notification->data['program_name']}}</b>
                  </div>
                  <div class="col-md-12" style="text-align: right">
                <label ><i class="material-icons" style="font-size:15px; margin-bottom:3px ">schedule</i> {{$notification->created_at->format('dS M,Y h:i:s a')}}</label>
                  </div>
                </div>
              </a>  
              
              @endforeach 
              @if(auth()->user()->unreadNotifications->where('read_at',null)->count()==0)
              <a class="dropdown-item"  style="width: 400px"><b>... There is No Notification Now ...</b></a>
              @endif
            </div>
          </li> 
          <a class="dropdown-item" href="{{ route('logout') }}"
          onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
           {{ __('Logout') }}
       </a>

       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
           @csrf
       </form>
          <li class="nav-item dropdown">
            <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="material-icons">person</i>
              <p class="d-lg-none d-md-block">
                Account
              </p>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
              @if(auth()->user()->role_id == 3)
              <a class="dropdown-item" href="{{route('merchants.edit', auth()->user()->id)}}">Settings</a>
              @endif
              <div class="dropdown-divider">
                
              </div>
              {{-- logout btn --}}
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                {{-- logout btn --}}
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
{{-- @push('scripts')
<script>
    function loadDoc() {

        setInterval(function(){

            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                document.getElementById("noti_number").innerHTML = this.responseText;
                }
            };
            xhttp.open("GET", "ajax_info.txt", true);
            xhttp.send();
        }, 1000);

    }
    loadDoc();
</script>
@endpush --}}
