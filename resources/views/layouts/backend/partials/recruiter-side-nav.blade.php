<div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item {{ (request()->is('dashboard')) ? 'active' : '' }}">
      <a class="nav-link" href="{{route('dashboard')}}">
          <i class="material-icons">dashboard</i>
          <p>Overview</p>
        </a>
      </li>
      @php
        $i=auth()->user()->id ;
      @endphp
      <li class="nav-item {{ (request()->is('dashboard/user-index')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('recruiters-form.show',[ 'recruiters_form' => auth()->user()->recruiter->id ])}}">
            <i class="material-icons">assistant_photo</i>
            <p>Recruiter Profile</p>
          </a>
        </li>
        <li class="nav-item {{ (request()->is('students-form/create')) ? 'active' : '' }}">
          <a class="nav-link" href="{{route('students-form.create')}}">
              <i class="material-icons">how_to_reg</i>
              <p>Register New Students</p>
            </a>
        </li>
        <li class="nav-item {{ (request()->is('dashboard/student-index')) ? 'active' : '' }}">
          <a class="nav-link" href="{{route('student.index')}}">
              <i class="material-icons">assignment_ind</i>
              <p>Student List</p>
            </a>
        </li>
        <li class="nav-item {{ (request()->is('/dashboard/school-index')) ? 'active' : '' }}">
          <a class="nav-link" href="{{route('school.index')}}">
              <i class="material-icons">book</i>
              <p>Institues And Programs</p>
            </a>
        </li> 
    </ul>
</div>