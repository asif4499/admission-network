<div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item {{ (request()->is('dashboard')) ? 'active' : '' }}">
      <a class="nav-link" href="{{route('dashboard')}}">
          <i class="material-icons">dashboard</i>
          <p>Overview</p>
        </a>
      </li>
      @php
        $i=auth()->user()->school->id ;
      @endphp
      <li class="nav-item {{ (request()->is('school-form/'.$i.'')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('school-form.show', $i)}}">
            <i class="material-icons">perm_identity</i>
            <p>Institution Profile</p>
          </a>
      </li>
      <li class="nav-item {{ (request()->is('dashboard/student-index')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('student.index')}}">
            <i class="material-icons">assignment_ind</i>
            <p>Student List</p>
          </a>
      </li>
      
      <li class="nav-item {{ (request()->is('program-add')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('addprogram.index')}}">
            <i class="material-icons">import_contacts</i>
            <p>List of Programs</p>
          </a>
      </li>
      <li class="nav-item {{ (request()->is('campus-add')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('addcampus.index')}}">
            <i class="material-icons">location_on</i>
            <p>All Campuses</p>
          </a>
      </li>
    </ul>
</div>