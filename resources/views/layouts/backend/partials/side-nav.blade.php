@if(auth()->user()->role_id==2)
@include('layouts.backend.partials.student-side-nav')

@elseif(auth()->user()->role_id==3)
@include('layouts.backend.partials.recruiter-side-nav')

@elseif(auth()->user()->role_id==4)
@include('layouts.backend.partials.supervisor-side-nav')

@elseif(auth()->user()->role_id==5)
@include('layouts.backend.partials.school-side-nav')

@else
<div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item {{ (request()->is('dashboard')) ? 'active' : '' }}">
      <a class="nav-link" href="{{route('dashboard')}}">
          <i class="material-icons">dashboard</i>
          <p>Overview</p>
        </a>
      </li>
      <li class="nav-item {{ (request()->is('dashboard/user-index')) ? 'active' : '' }}">
      <a class="nav-link" href="{{route('alluser.index',[ 'role_id' => 1 ])}}">
          <i class="material-icons">account_box</i>
          <p>User List</p>
        </a>
      </li>
      <li class="nav-item {{ (request()->is('dashboard/student-index')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('student.index')}}">
            <i class="material-icons">assignment_ind</i>
            <p>Student List</p>
          </a>
      </li>
      <li class="nav-item {{ (request()->is('dashboard/recruiter-index')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('recruiter.index')}}">
            <i class="material-icons">contact_mail</i>
            <p>Recruiter List</p>
          </a>
      </li>
      <li class="nav-item {{ (request()->is('dashboard/supervisor-index')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('supervisor1.index')}}">
            <i class="material-icons">contact_mail</i>
            <p>Supervisor Managment</p>
          </a>
      </li>
      {{-- <li class="nav-item {{ (request()->is('students-form/create')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('students-form.create')}}">
            <i class="material-icons">how_to_reg</i>
            <p>Register New Students</p>
          </a>
      </li> --}}
      <li class="nav-item {{ (request()->is('dashboard/school-index')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('school.index')}}">
            <i class="material-icons">school</i>
            <p>Institute And Programs</p>
          </a>
      </li>
      <li class="nav-item {{ (request()->is('country')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('country.index')}}">
            <i class="material-icons">flag</i>
            <p>Country List</p>
          </a>
      </li>
    </ul>
</div>
@endif
