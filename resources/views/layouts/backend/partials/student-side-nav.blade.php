<div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item {{ (request()->is('dashboard')) ? 'active' : '' }}">
      <a class="nav-link" href="{{route('dashboard')}}">
          <i class="material-icons">dashboard</i>
          <p>Overview</p>
        </a>
      </li>
      @php
        $i=auth()->user()->id ;
      @endphp
      <li class="nav-item {{ (request()->is('student/show-profile/'.$i.'')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('students-form.showProfile',['id'=> $i])}}">
            <i class="material-icons">perm_identity</i>
            <p>Student Profile</p>
          </a>
      </li>
      <li class="nav-item {{ (request()->is('/dashboard/school-index')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('school.index')}}">
            <i class="material-icons">book</i>
            <p>Institues And Programs</p>
          </a>
      </li>
    </ul>
</div>