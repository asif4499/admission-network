<div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item {{ (request()->is('dashboard')) ? 'active' : '' }}">
      <a class="nav-link" href="{{route('dashboard')}}">
          <i class="material-icons">dashboard</i>
          <p>Overview</p>
        </a>
      </li>
      @php
        $i=auth()->user()->supervisor->id ;
      @endphp
      <li class="nav-item {{ (request()->is('supervisor/'.$i.'')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('supervisor.show',['supervisor'=> $i])}}">
            <i class="material-icons">perm_identity</i>
            <p>Supervisor Profile</p>
          </a>
      </li>
      <li class="nav-item {{ (request()->is('dashboard/student-index')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('student.index')}}">
            <i class="material-icons">assignment_ind</i>
            <p>Student List</p>
          </a>
      </li>
      <li class="nav-item {{ (request()->is('/dashboard/school-index')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('school.index')}}">
            <i class="material-icons">book</i>
            <p>Institues And Programs</p>
          </a>
      </li>
    </ul>
</div>