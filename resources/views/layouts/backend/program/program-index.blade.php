@extends('layouts.backend.layouts.app')
@include('layouts.frontend.forms.create1-modal')
@section('content')
<div class="container-fluid">               
  <div class="row">
      <div class="card">
        {{-- <div class="card-header card-header-text card-header-primary">
            <div class="card-text">
              <h4 class="card-title">Here is the Text</h4>
            </div>
        </div> --}}
        <div class="card-body table-responsive">

    <button type="button" class="btn btn-info pull-right" 
      data-toggle="modal" data-target="#exampleModalLongTitle1" >
      Add Program</button>
      <div class="clearfix"></div>
</div>  
@php
 $i=0;   
@endphp
<h4><b>List of Courses</b></h4>
<div class="table-responsive">
  <table class="table table-bordered table-hover" 
   width="100%">
   <thead class="card-header-info no-shadow">
      <tr style="text-align: center">
        <th scope="col">SL</th>
        <th scope="col">Course Name</th>
        <th scope="col">Programe Level</th>
        <th scope="col">Intakes</th>
        <th scope="col">Course Duration</th>
        <th scope="col">Seasons</th>
        <th scope="col">Tution Free</th>
        <th scope="col">Admission Free</th>
        <th scope="col">Language Profeciency(English)</th>
        <th scope="col">Required English Exam Type</th>
        <th scope="col">Visa Type Required </th>
        <th scope="col">Required Grading Scheme</th>
        <th scope="col">Required Grade Scale*(out of)</th>
        <th scope="col">Required Grade Average</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($programs as $program) 
        <tr style="text-align: center" >
            <td>{{$i++}}</td>
            <td>{{$program->course_name}}</td>
            <td>{{$program->programe_level}}</td>
            <td>{{$program->intakes}}</td>
            <td>{{$program->course_duration}}</td>                       
            <td>{{$program->seasons}}</td>
            <td>{{$program->tution_free}}</td>
            <td>{{$program->admission_free}}</td>
            <td>{{$program->language_profeciency}}</td>
            <td>{{$program->required_exam_type}}</td>
            <td>{{$program->required_visa_type}}</td>
            <td>{{$program->required_grading_scheme}}</td>
            <td>{{$program->required_grade_scale}}</td>
            <td>{{$program->required_grade_avg}}</td>
          
        </tr>
   @endforeach
    </tbody>
  </table>
</div>
</div>
</div>
</div>


@endsection


@push('scripts')

@endpush