@extends('layouts.backend.layouts.app')
@push('styles')
    <link href="{{ asset('/backend/css/table.css') }}" rel="stylesheet" />
    <link href="{{ asset('/backend/css/comment.css') }}" rel="stylesheet" />
@endpush
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="card card-nav-tabs">
                <h4 class="card-header card-header-info">
                    <div class="row">
                            <div class="col-md-6" style="text-align:left" >
                                <b>Institute Name :{{$program->schoolInfo->school_name}}  </b>
                            </div>
                            <div class="col-md-6" style="text-align:right">
                            <b>Applied At : {{$program->students()->where('student_id',$student->id)->first()
                                                    ->pivot->created_at->format('dS M,Y h:i:s a')}}  </b>
                            </div>
                    </div>
                </h4>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title" style="text-align:left"><b>Coures Name :{{ $program->course_name }}
                            </b></h4>
                        </div>
                        
                        <div class="col-md-6">
                            @php
                             $s=$program->students()->where('student_id',$student->id)->first()
                            ->pivot->status
                            @endphp
                            @if(auth()->user()->role_id==4)
                            <form action="{{route('program.assign-status',[ 'id' => $program->id ,'student_id'=>$student->id])}}" method="POST">
                                @csrf
                                @method('PUT')
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h4 class="card-title" style="text-align:right"><b>
                                                <div class="input-group">
                                                    <select class="role form-control
                                                    @error('status') is-invalid @enderror" name="status" required>
                                                        <option></option>
                                                        <option value="Under Processing" {{("Under Processing"==$s) ?'selected':''}}>Under Processing</option>
                                                        <option value="Need More Document" {{("Need More Document"==$s) ?'selected':''}}>Need More Document</option>
                                                        <option value="Accepted By Institute" {{("Accepted By Institute"==$s) ?'selected':''}}>Accepted By Institute</option>
                                                        <option value="Rejected By Institute" {{("Rejected By Institute"==$s) ?'selected':''}}>Rejected By Institute</option>
                
                                                    </select>
                                                </div>
                                            </b></h4>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submite" class="btn btn-danger btn-sm">Submit</button>
                                        </div>
                                    </div>
                            </form>
                                            
                                            @else
                                                <h4 class="card-title" style="text-align:right"><b>ID :{{ $program->id }} </b></h4>
                                            @endif

                                        </div>
                                    </div>
                    <p class="card-text">

                        <div class="card-body">

                                        <table class="table">

                                            <tbody>
                                                <tr >

                                                   
                                                    <td colspan="4" class="card-header-info" style="font-weight: bold; font-size: 20px; " >Program Details</td>

                                                </tr>
                                                <tr>

                                                    <td style="font-weight: bold; text-align:right; ">Course Name :</td>
                                                    <td style="text-align:left">{{ $program->course_name }}</td>

                        
                                                    <td style="font-weight: bold; text-align:right">Program Level :</td>
                                                    <td style="text-align:left">{{ $program->programe_level }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold; text-align:right">Intake :</td>
                                                    <td style="text-align:left">{{ $program->intakes }}</td>

                                              

                                                    <td style="font-weight: bold; text-align:right">Course Duration :</td>
                                                    <td style="text-align:left">{{ $program->course_duration }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold; text-align:right">Seasons :</td>
                                                    <td style="text-align:left">{{ $program->seasons }}</td>

                                                

                                                    <td style="font-weight: bold; text-align:right">Tution Free :</td>
                                                    <td style="text-align:left">{{ $program->tution_free }}</td>

                                                </tr>

                                                 <tr>

                                                    <td style="font-weight: bold; text-align:right">Admission Free:</td>
                                                    <td style="text-align:left">{{ $program->admission_free }}</td>

                                               

                                                    <td style="font-weight: bold; text-align:right">Language Profeciency :</td>
                                                    <td style="text-align:left">{{ $program->language_profeciency }}</td>

                                                </tr>


                                                 <tr>

                                                    <td style="font-weight: bold; text-align:right">Required Exam Type :</td>
                                                    <td style="text-align:left">{{ $program->required_exam_type }}</td>

                                              

                                                    <td style="font-weight: bold; text-align:right">Required Visa Type :</td>
                                                    <td style="text-align:left">{{ $program->required_visa_type }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold; text-align:right">Required Grading Scheme :</td>
                                                    <td style="text-align:left">{{ $program->required_grading_scheme }}</td>

                                                

                                                    <td style="font-weight: bold; text-align:right">Required Grade Scale :</td>
                                                    <td style="text-align:left">{{ $program->required_grade_scale }}</td>

                                                </tr>
                                                <tr>

                                                    <td style="font-weight: bold; text-align:right">Required Grade Avg :</td>
                                                    <td style="text-align:left">{{ $program->required_grade_avg }}</td>

                                              
                                                    <td style="font-weight: bold; text-align:right"></td>
                                                    <td style="text-align:left"></td>

                                                </tr>
                                               


                                            </tbody>
                                        </table>
                                    </div>
                                   
                               

                    </p>

                    
                    </div>
                </div>
            </div>
            
            @if(auth()->user()->role_id == 2 || auth()->user()->role_id==4 || auth()->user()->role_id==3)
            @if($student->supervisor_id!=null )
            <div class="card card-nav-tabs">
                <div class="card-header card-header-primary" style="text-align: left; font-size:22px">
                <div class="row">
                    <div class="col-md-6" style="text-align: left;">
               
                 Comunications
                </div>
                    
                <div class="col-md-6" style="text-align: right;">
                
                    Comments({{$program->comments()->where('commented_by',$student->id)->count()}})
                   
                </div>

                </div>
                </div>
                
                    
                    <div class="container">
                   
                        
                       @foreach ($program->comments()->where('comment_group_id',$student->id)->get() as $comment)
                            
                            <div style="margin: 10px">
                                <div class="row">
                                <div class="col-md-6" style="text-align: left; ">
                                        
                                            <div style="font-weight: 500; font-size:18px">
                                                {{$user->where('id',$comment->commented_by)->first()->name}},
                                                @if($user->where('id',$comment->commented_by)->first()->role_id==4)
                                                    <span class="material-icons badge badge-danger">
                                                        supervised_user_circle                         
                                                    </span>
                                                @else
                                                    <span class="material-icons badge badge-primary">
                                                        face
                                                    </span>
                                                @endif
                                                
                                            </div>
                                   
                                    </div>
                                    <div class="col-md-6" style="text-align: right;">
                                        <span class="be-comment-time">
                                            <i class="material-icons" style=" font-size:14px;">
                                                query_builder
                                            </i>
                                            {{$comment->created_at->format('dS M,Y h:i:s a')}}
                                          
                                        </span>
                                        </div>
                                </div>
                        
                           
                            <p class="be-comment-text" style="font-weight: 300; fornt-size: 15px">
                                {{$comment->comment_details}}
                            </p>
                     
                        </div>
                       @endforeach 
                        

                            <form  action="{{route('comment.store',['program_id'=>$program->id,'student_id'=>$student->id])}}" method="POST">
                                @csrf
                                <div style="text-align: left;">
                                <label >Write your comment here :</label>
                                <textarea class="form-control" name="comment_details" style="margin: 10px 0px;" row="3"> </textarea>
                                <div style="text-align: right;">
                                <button type="submit" class="btn btn-primary btn-md" >Submit</button>
                            </div>
                                </div>
                            </form>
                    
                    </div>
               
                </div>]
                @else
                <h3 class="danger">Can Not Comment Now, As No Supervisor is Assigned Yet</h3>
            
                @endif
                @endif
            
    </div>
            
        @endsection


        @push('scripts')
            {{-- <script>
                    window.onload = function() {
                        if(!window.location.hash) {
                            window.location = window.location + '#loaded';
                            window.location.reload();
                        }
                    }
            </script> --}}
        @endpush
