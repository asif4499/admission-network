@extends('layouts.backend.layouts.app')
@push('styles')
    <link href="{{ asset('/backend/css/table.css') }}" rel="stylesheet" />
    <link href="{{ asset('/backend/css/comment.css') }}" rel="stylesheet" />
@endpush
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="card card-nav-tabs">
                <h4 class="card-header card-header-info">
                    <div class="row">
                            <div class="col-md-6" style="text-align:left" >
                                <b>Institute Name :{{$program->schoolInfo->school_name}}  </b>
                            </div>
                            
                    </div>
                </h4>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title" style="text-align:left"><b>Coures Name :{{ $program->course_name }}
                            </b></h4>
                        </div>
                        <div class="col-md-6">
                            <h4 class="card-title" style="text-align:right"><b>ID :{{ $program->id }} </b></h4>
                        </div>
                    </div>
                    <p class="card-text">

                        <div class="card-body">

                                        <table class="table">

                                            <tbody>
                                                <tr >

                                                   
                                                    <td colspan="4" class="card-header-info" style="font-weight: bold; font-size: 20px; " >Program Details</td>

                                                </tr>
                                                <tr>

                                                    <td style="font-weight: bold; text-align:right; ">Course Name :</td>
                                                    <td style="text-align:left">{{ $program->course_name }}</td>

                        
                                                    <td style="font-weight: bold; text-align:right">Program Level :</td>
                                                    <td style="text-align:left">{{ $program->programe_level }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold; text-align:right">Intake :</td>
                                                    <td style="text-align:left">{{ $program->intakes }}</td>

                                              

                                                    <td style="font-weight: bold; text-align:right">Course Duration :</td>
                                                    <td style="text-align:left">{{ $program->course_duration }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold; text-align:right">Seasons :</td>
                                                    <td style="text-align:left">{{ $program->seasons }}</td>

                                                

                                                    <td style="font-weight: bold; text-align:right">Tution Free :</td>
                                                    <td style="text-align:left">{{ $program->tution_free }}</td>

                                                </tr>

                                                 <tr>

                                                    <td style="font-weight: bold; text-align:right">Admission Free:</td>
                                                    <td style="text-align:left">{{ $program->admission_free }}</td>

                                               

                                                    <td style="font-weight: bold; text-align:right">Language Profeciency :</td>
                                                    <td style="text-align:left">{{ $program->language_profeciency }}</td>

                                                </tr>


                                                 <tr>

                                                    <td style="font-weight: bold; text-align:right">Required Exam Type :</td>
                                                    <td style="text-align:left">{{ $program->required_exam_type }}</td>

                                              

                                                    <td style="font-weight: bold; text-align:right">Required Visa Type :</td>
                                                    <td style="text-align:left">{{ $program->required_visa_type }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold; text-align:right">Required Grading Scheme :</td>
                                                    <td style="text-align:left">{{ $program->required_grading_scheme }}</td>

                                                

                                                    <td style="font-weight: bold; text-align:right">Required Grade Scale :</td>
                                                    <td style="text-align:left">{{ $program->required_grade_scale }}</td>

                                                </tr>
                                                <tr>

                                                    <td style="font-weight: bold; text-align:right">Required Grade Avg :</td>
                                                    <td style="text-align:left">{{ $program->required_grade_avg }}</td>

                                              
                                                    <td style="font-weight: bold; text-align:right"></td>
                                                    <td style="text-align:left"></td>

                                                </tr>
                                               


                                            </tbody>
                                        </table>
                                    </div>
                                   
                               

                    </p>

                    
                    </div>
                </div>
            </div>
           
    </div>
            
        @endsection


        @push('scripts')

        @endpush
