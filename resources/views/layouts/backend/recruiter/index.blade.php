<div class="container-fluid">               
    <div class="row">
        <div class="card">
          {{-- <div class="card-header card-header-text card-header-primary">
              <div class="card-text">
                <h4 class="card-title">Here is the Text</h4>
              </div>
          </div> --}}
          <div class="card-body table-responsive">
              {{-- <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".create">Create New Role</button> --}}
             
              <table class="table table-bordered table-hover" 
              id="get-student-list"  width="100%">
                 <thead class="card-header-info no-shadow">
                  <tr style="text-align: center">
                        <th scope="col">SL</th>
                        <th scope="col">Title</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Middle Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Gender</th>
                        <th scope="col">DOB</th>
                        <th scope="col">Email</th>
                        <th scope="col">Mobile No.</th>
                        <th scope="col">Emergency Contact</th>
                        <th scope="col">Assigned Supervisor</th>
                        <th scope="col">Registered At</th>
                        <th scope="col">Activated At</th>
                        <th scope="col" style="min-width:150px">Action</th>
                  </tr>
                </thead>
              </table>
          </div>
        </div>
    </div>
  </div>


  