@extends('layouts.backend.layouts.app')
@push('styles')
    <link href="{{ asset('/backend/css/table.css') }}" rel="stylesheet" />
@endpush
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="card card-nav-tabs">
                <h4 class="card-header card-header-info">
            

                 <div class="row">
                        <div class="col-md-6" style="text-align:left" >
                            <b>Recruiter Status : {{auth()->user()->status==1?'Active':'Deactive' }} </b>
                        </div>
                        <div class="col-md-6" style="text-align:right">
                           <b>Applied Count : 2 </b>
                        </div>
                   
                </div>

               

                </h4>
                
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title" style="text-align:left">
                                
                                <b>Name :{{ $recruiter->user()->first()->name }}</b>             
                            </h4>
                        </div>
                        <div class="col-md-6">
                            <h4 class="card-title" style="text-align:right"><b >ID : {{ $recruiter->id }}</b></h4>
                        </div>
                    </div>
                    <p class="card-text">

                        {{-- Start of Collaps --}}

                    <div id="accordion" role="tablist" aria-multiselectable="false" class="card-collapse">
                        <div class="card card-plain">
                            <div class="card-header" role="tab" id="headingOne">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"
                                    aria-controls="collapseOne">
                                    <h4>Company Info
                                        <i class="material-icons">keyboard_arrow_down</i>
                                    </h4>
                                </a>
                            </div>

                            <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-body">



                                    <table class="table">

                                        <tbody>
                                            <tr>

                                                <td style="font-weight: bold; min-width: 145px;">Company Name :</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $recruiter->company_name }}</td>
                                                <td style="font-weight: bold">Email :</td>
                                                <td style="text-align:left">{{ $recruiter->email }}</td>

                                            </tr>

                                            <tr>

                                                <td style="font-weight: bold">Website:</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $recruiter->website }}</td>
                                                <td style="font-weight: bold">Facebook Page Name :</td>
                                                <td style="text-align:left">{{ $recruiter->facebook_page_name }}</td>

                                            </tr>

                                            <tr>

                                                <td style="font-weight: bold">Instagram Handle:</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $recruiter->instagram_handle }}</td>
                                                <td style="font-weight: bold">Twitter Handle :</td>
                                                <td style="text-align:left">{{ $recruiter->twitter_handle }}</td>

                                            </tr>

                                            <tr>

                                                <td style="font-weight: bold">Linkedin URL :</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $recruiter->linkdin_url }}</td>

                                                    <td style="font-weight: bold"></td>
                                                    <td style="text-align:left"></td>               
                                               
                                            </tr>


                                            



                                        </tbody>
                                    </table>



                                </div>
                            </div>
                          </div>


                            <div class="card card-plain">
                                <div class="card-header" role="tab" id="headingOne1">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="false"
                                        aria-controls="collapseOne1">
                                        <h4>Contact Info 
                                            <i class="material-icons">keyboard_arrow_down</i>
                                        </h4>
                                    </a>
                                </div>
    
                                <div id="collapseOne1" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-body">
    
    
    
                                        <table class="table">
    
                                            <tbody>
                                                <tr>
    
                                                    <td style="font-weight: bold; min-width: 145px;">Main Source of Students :</td>
                                                    <td style="border-right: 1px solid gray; text-align:left">
                                                        {{ $recruiter->main_source_of_student }}</td>
                                                    <td style="font-weight: bold">Legal First Name :</td>
                                                    <td style="text-align:left">{{ $recruiter->legal_first_name }}</td>
    
                                                </tr>
    
                                                <tr>
    
                                                    <td style="font-weight: bold">Legal Last Name :</td>
                                                    <td style="border-right: 1px solid gray; text-align:left">
                                                        {{ $recruiter->legal_last_name }}</td>
                                                    <td style="font-weight: bold">Street Address :</td>
                                                    <td style="text-align:left">{{ $recruiter->street_address }}</td>
    
                                                </tr>
    
                                                <tr>
    
                                                    <td style="font-weight: bold">City :</td>
                                                    <td style="border-right: 1px solid gray; text-align:left">
                                                        {{ $recruiter->city }}</td>
                                                    <td style="font-weight: bold">State/Province :</td>
                                                    <td style="text-align:left">{{ $recruiter->state }}</td>
    
                                                </tr>
    
                                                <tr>
    
                                                    <td style="font-weight: bold">Country :</td>
                                                    <td style="border-right: 1px solid gray; text-align:left">
                                                        {{ $recruiter->country }}</td>
                                                    <td style="font-weight: bold">Postal Code :</td>
                                                    <td style="text-align:left">{{ $recruiter->postal_code }}</td>
    
                                                </tr>

                                                <tr>
    
                                                    <td style="font-weight: bold; min-width: 145px;">Phone :</td>
                                                    <td style="border-right: 1px solid gray; text-align:left">
                                                        {{ $recruiter->phone }}</td>
                                                    <td style="font-weight: bold">Cell Phone :</td>
                                                    <td style="text-align:left">{{ $recruiter->cellphone }}</td>
    
                                                </tr>
    
                                                <tr>
    
                                                    <td style="font-weight: bold">Skype Id:</td>
                                                    <td style="border-right: 1px solid gray; text-align:left">
                                                        {{ $recruiter->skype_id }}</td>
                                                    <td style="font-weight: bold">Whatsapp Id :</td>
                                                    <td style="text-align:left">{{ $recruiter->whatsapp_id }}</td>
    
                                                </tr>
    
                                                <tr>
    
                                                    <td colspan = "2" style="font-weight: bold">Has anyone from Admission network helped or referred you?</td>
                                                    <td colspan = "2">
                                                        {{ $recruiter->referred_info }}</td>

                               
                                                </tr>
    
                                                              
    
    
    
                                            </tbody>
                                        </table>
    
    
    
                                    </div>
                                </div>
                              </div>


                            <div class="card card-plain">
                                <div class="card-header" role="tab" id="headingTwo">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                                        aria-expanded="false" aria-controls="collapseTwo">
                                        <h4>Recruitment Details
                                            <i class="material-icons">keyboard_arrow_down</i>
                                        </h4>

                                    </a>
                                </div>
                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="card-body">

                                        <table class="table">

                                            <tbody>
                                                <tr>

                                                    <td style="font-weight: bold">When did you begin recruiting students? </td>
                                                    <td>{{ $recruiter->begin_recruiting_student }}</td>

                                                </tr>
                                                <tr>

                                                    <td style="font-weight: bold">What services do you provide to your clients?</td>
                                                    <td>{{ $recruiter->servie_to_clint }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">What institutions are you representing?</td>
                                                    <td>{{ $recruiter->inistitutions_representing }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">What educational associations or groups does your organization belong to? </td>
                                                    <td>{{ $recruiter->educational_associations }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Where do you recruit from?
                                                        course ? </td>
                                                    <td>{{ $recruiter->recruit_from }}</td>

                                                </tr>


                                                <tr>

                                                    <td style="font-weight: bold">Approximately how many students do you send abroad per year? </td>
                                                    <td>{{ $recruiter->student_count_per_year }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">What type of marketing methods do you undertake?</td>
                                                    <td>{{ $recruiter->marketing_method }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Average Service Fee : </td>
                                                    <td>{{ $recruiter->average_service_free }}</td>

                                                </tr>
                                                <tr>

                                                    <td style="font-weight: bold">Please provide an estimate of the number of students you will refer to  :</td>
                                                    <td>{{ $recruiter->estimated_number }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Additional Comments :</td>
                                                    <td>{{ $recruiter->additional_comments }}</td>

                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        

                                                <div class="card card-plain">
                                                    <div class="card-header" role="tab" id="headingTwo1">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo1"
                                                            aria-expanded="false" aria-controls="collapseTwo1">
                                                            <h4>Reference Details
                                                                <i class="material-icons">keyboard_arrow_down</i>
                                                            </h4>
                    
                                                        </a>
                                                    </div>
                                                    <div id="collapseTwo1" class="collapse" role="tabpanel" aria-labelledby="headingTwo1">
                                                        <div class="card-body">
                    
                                                            <table class="table">
                    
                                                                <tbody>
                                                <tr>

                                                    <td style="font-weight: bold">Reference Name :</td>
                                                    <td>{{ $recruiter->reference_name }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">IReference Institution Name : </td>
                                                    <td>{{ $recruiter->reference_inistitution_name }}</td>

                                                </tr>

                        

                                                <tr>

                                                    <td style="font-weight: bold">Reference Business Email : </td>
                                                    <td>{{ $recruiter->reference_business_email }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Reference Phone :</td>
                                                    <td>{{ $recruiter->reference_phone }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Reference Website :</td>
                                                    <td>{{ $recruiter->reference_website }}</td>

                                                </tr>

                                                
                                                <tr>

                                                    <td style="font-weight: bold">Company Logo:</td>
                                                    <td>{{ $recruiter->reference_phone }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Business Certificate :</td>
                                                    <td>{{ $recruiter->reference_website }}</td>

                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @include('layouts.backend.recruiter.index')
                            
                        </div>
                        </p>

                    </div>
                </div>
            </div>
            <div id="mydiv" data-myval="{{$recruiter->id}}"></div>
            
        @endsection


        @push('scripts')
            
        <script>
            $(function() {
                var id = $('#mydiv').data('myval');
                $('#get-student-list').DataTable({
                    
                      processing: true,
                      serverSide: true,
                      responsive: true,
                      //scrollX: true,
                      // paging: false,
                    ajax:{
                        url:'{{ route("get-studentRecruiter-list") }}',
                        data:{'id':id},
                    } ,
                    
                    columns: [
                     
                      { data: 'DT_RowIndex', name: 'id', orderable: false },
                      {data:'title',name:'title',orderable:false},
                      {data:'first_name',name:'first_name',orderable:false},
                      {data:'middle_name',name:'middle_name',orderable:false},
                      {data:'last_name',name:'last_name',orderable:false},
                      {data:'gender',name:'gender',orderable:false,
                       render:
                                function(data, type, row) {
                                    if(row.gender == 1){
                                        return 'Male';
                                    }
                                    if(row.gender == 2){
                                        return 'Female';
                                    }
                                }
                      },
                      {data:'date_of_birth',name:'date_of_birth',orderable:false},
                      {data:'email',name:'email',orderable:false},
                      {data:'mobile',name:'mobile',orderable:false},
                      {data:'emergency_contact',name:'emergency_contact',orderable:false},
                      {data:'supervisor_id',name:'supervisor_id',orderable:false},
                      { data: 'registered_at', name: 'registered_at', orderable: false },
                      { data: 'activated_at', name: 'activated_at', orderable: false },
                     
                      
                      
                
                      
                      {data: 'action', name: 'action', orderable: false,
                            render:
                                function(data, type, row) {
                                      return '<div style="text-align:center" class="action-btn">'+
                                                  '<a class="btn btn-primary btn-sm" href="/students-form/'+row.id+'">View<a>'+
                                              '</div>';
      
                                }
                        },
                    ],
                initComplete: function () {
                      $('.role').select2({
                        placeholder: "Select an option",
                        allowClear: true
                      });
                  },
      
                });
      
            });
            
        </script>
         
        <script src="{{asset('backend/js/select2.min.js')}}" type="text/javascript"></script>
        @endpush
