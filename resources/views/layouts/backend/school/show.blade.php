@extends('layouts.backend.layouts.app')
@push('styles')
    <link href="{{ asset('/backend/css/table.css') }}" rel="stylesheet" />
@endpush
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="card card-nav-tabs">
                <h4 class="card-header card-header-info">
                
                 <div class="row">
                        <div class="col-md-6" style="text-align:left" >
                            <b>Institution Status : {{auth()->user()->status==1?'Active':'Deactive' }} </b>
                        </div>
                        <div class="col-md-6" style="text-align:right">
                           <b>Applied Count : 2 </b>
                        </div>
                   
                </div>
        

               

                </h4>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title" style="text-align:left">
            
                                <b>Institution Name :{{ $school->school_name }}</b>
                               
                            </h4>
                        </div>
                        <div class="col-md-6">
                            <h4 class="card-title" style="text-align:right"><b>ID : {{ $school->id }}</b></h4>
                        </div>
                    </div>
                    <p class="card-text">

                        {{-- Start of Collaps --}}

                    <div id="accordion" role="tablist" aria-multiselectable="true" class="card-collapse">


                        <div class="card card-plain">
                            <div class="card-header" role="tab" id="headingOne">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"
                                    aria-controls="collapseOne">
                                    <h4>Institution Info
                                        <i class="material-icons">keyboard_arrow_down</i>
                                    </h4>
                                </a>
                            </div>

                            <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-body">



                                    <table class="table">

                                        <tbody>
                                            <tr>

                                                <td style="font-weight: bold; min-width: 145px;">School Name :</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $school->school_name }}</td>
                                                <td style="font-weight: bold">Contact Email:</td>
                                                <td style="text-align:left">{{ $school->contact_email }}</td>

                                            </tr>

                                            <tr>
                                                
                                                <td style="font-weight: bold">Contact First Name:</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $school->contact_first_name }}</td>
                                                <td style="font-weight: bold">Contact Last Name :</td>
                                                <td style="text-align:left">{{ $school->contact_last_name }}</td>

                                            </tr>

                                            <tr>

                                                <td style="font-weight: bold">Contact Number :</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $school->contact_number }}</td>
                                                <td style="font-weight: bold">Contact Title :</td>
                                                <td style="text-align:left">{{ $school->contact_title }}</td>

                                            </tr>

                                            <tr>

                                                <td style="font-weight: bold">Comment :</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $school->comment }}</td>
                                                <td style="font-weight: bold">Agreed to Other Comunication :</td>
                                                <td style="text-align:left">{{ $school->agree_communication }}</td>

                                            </tr>


                                            <tr>

                                                <td style="font-weight: bold">Reference Name:</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $school->ref_name }}</td>
                                                <td style="font-weight: bold">Reference Email:</td>
                                                <td style="text-align:left">{{ $school->ref_email }}</td>

                                            </tr>

                                            <tr>

                                                <td style="font-weight: bold">Type Of Institution:</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $school->institution_type }}</td>
                                                <td style="font-weight: bold">Number of Campus :</td>
                                                <td style="text-align:left">{{ $school->no_of_campus }}</td>

                                            </tr>

                                        </tbody>
                                    </table>



                                </div>
                            </div>
                          </div>


                            <div class="card card-plain">
                                <div class="card-header" role="tab" id="headingTwo">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                                        aria-expanded="false" aria-controls="collapseTwo">
                                        <h4>Campus Details
                                            <i class="material-icons">keyboard_arrow_down</i>
                                        </h4>

                                    </a>
                                </div>
                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="card-body">

                                        <div class="card">
                                            
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead class="card-header-info">
                                                    <tr>
                                                        <th style="font-weight: bold" colspan="7">List of Campus</th>
                                                    </tr>
                                                  <tr style="text-align: center" >
                                                    <th scope="col">SL</th>
                                                    <th scope="col" >Address</th>
                                                    <th scope="col" >Zip</th>
                                                    <th scope="col" >City</th>
                                                    <th scope="col" >State</th>
                                                    <th scope="col"  >Country</th>
                                                    <th scope="col"  >Action</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                    $i = 1
                                                    @endphp
                                             @foreach ($campuses as $campus) 
                                                
                                                    <tr style="text-align: center" >
                                                        <td>{{$i++}}</td>
                                                        <td>{{$campus->address}}</td>
                                                        <td>{{$campus->zip}}</td>
                                                        <td>{{$campus->city}}</td>                        
                                                        <td>{{$campus->state}}</td>
                                                        <td>{{$campus->country}}</td>
                                                        <td class="td-actions">1
                                                            {{-- <a class="btn btn-primary btn-link" href="{{ route('admin-client.edit', $row->id) }}">
                                                                <i class="material-icons">edit</i>
                                                            </a>
                                
                                                            <form class="form-delete" action="{{ route('admin-client.destroy', $row->id) }}" method="POST">
                                                                {{ method_field('DELETE') }}
                                                                {{ csrf_field() }}
                                                                <button type="submit" title="Delete" class="delete btn btn-danger btn-link">
                                                                    <i class="material-icons">close</i>
                                                                  </button>
                                                            </form> --}}
                                
                                
                                                          </td>
                                                    </tr>
                                            @endforeach
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                        </div>
                                


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>




                            <div class="card card-plain">
                                <div class="card-header" role="tab" id="headingThree">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                        href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <h4>List Of All The Courses
                                            <i class="material-icons">keyboard_arrow_down</i>
                                        </h4>
                                    </a>
                                </div>
                                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                            

                                        <div class="container-fluid content-row">
                                           
                                                    @foreach ($programs as $program) 
                                                    
                                                       
                                                        
                                                          
                                                          <div >
                                                            <table class="table" style="table-layout: fixed">
                                                                <thead class="card-header-info">
                                                                    <tr>
                                                                        <th style="font-weight: bold" colspan="4">
                                                                            <span class="material-icons">
                                                                                school
                                                                                </span>
                                                                                Course: {{$program->course_name}} </th>
                                                                    </tr>
                                                                </thead>
                                                              <tbody>
                                                                <tr>
                                                                    
                                                                  </tr>
                                                                <tr>
                                                                  <th>Programe Level</th>
                                                                  <td>{{$program->programe_level}}</td>
                                                                
                                                                    <th>Intakes</th>
                                                                    <td>{{$program->intakes}}</td>
                                                                  </tr>
                                                                  <tr>
                                                                    <th>Course Duration</th>
                                                                    <td>{{$program->course_duration}}</td>
                                                                 
                                                                    <th>Tution Free</th>
                                                                    <td>{{$program->tution_free}}</td>
                                                                  </tr>
                                                                  <tr>
                                                                    <th>Admission Free</th>
                                                                    <td>{{$program->admission_free}}</td>
                                                                    <th>Language Profeciency(English)</th>
                                                                    <td>{{$program->language_profeciency}}</td>
                                                                  </tr>
                                                                <tr>
                                                                @if(auth()->user()->role_id==2)
                                                                    <td colspan="4" class="td-actions">
                                                                      
                                                                       
                                                                        <form action="{{route('program.apply',[ 'id'=>$program->id,'student_id' => $student->id ])}}" method="POST">
                                                                            @csrf
                                                                            @method('PUT')
                                                                            @php($a=false)
                                                                            @if($student->programs->count()==0)
                                                                              <button type="submit" class="btn btn-primary btn-sm" >Apply</button>
                                                                            @else
                                                                              @foreach($student->programs as $iteam)
                                                                                @if($iteam->id==$program->id)
                                                                                  @php($a=true)
                                                                                @endif
                                                                              @endforeach
                                                                              <button type="submit" class="btn btn-primary btn-sm" {{$a?'disabled':''}} >Apply</button>
                                                                            @endif
                                                                            <a rel="tooltip" href="{{route('program.showforall',[ 'id' => $program->id ])}}" class="btn btn-info">
                                                                                <i class="material-icons">visibility</i>
                                                                            </a>

                                                                              @if(auth()->user()->role_id==3)
                                                                            <button rel="tooltip" class="btn btn-danger">
                                                                              <i class="material-icons">close</i>
                                                                            </button>
                                                                            @endif
                                                                            
                                                                        </form>
                                                                        @endif
                                                                       
                                                                    </td>
                                                                  </tr>
                                                                
                                                              </tbody>
                                                            </table>
                                                          </div>
                                                    
                                        
                                                      @endforeach
                                                </div> 
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>


                    
                        </div>
                        </p>

                    </div>
                </div>
            </div>


        @endsection


        @push('scripts')

        @endpush
