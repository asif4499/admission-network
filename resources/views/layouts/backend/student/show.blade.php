@extends('layouts.backend.layouts.app')
@push('styles')
    <link href="{{ asset('/backend/css/table.css') }}" rel="stylesheet" />
@endpush
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="card card-nav-tabs">
                <h4 class="card-header card-header-info">
                @if(auth()->user()->role_id == 1)
                <form action="{{route('student-form.assignSupervisor',$student->id)}}" method="POST">
                         @csrf
                         @method('PUT')
                    <div class="row">
                        <div class="col-md-5" style="margin-top:5px">
                            <b>Assign Supervisor :</b>
                        </div>
                    
                        <div class="col-md-5" style="margin-top:5px">
                            <div class="input-group">
                                <select class="role form-control
                                @error('supervisor_id') is-invalid @enderror" name="supervisor_id" required>
                                
                                    <option></option>
                                    @foreach ($supervisors as $supervisor )
                                         <option value="{{$supervisor->id}}" {{($supervisor->id==$student->supervisor_id) ?'selected':''}}>{{$supervisor->name}}</option>
                                    @endforeach

                                </select>
                                @error('supervisor_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submite" class="btn btn-danger btn-sm">Assign</button>
                        </div>
                   
                    </div>
                </form>
                @else
                 <div class="row">
                        <div class="col-md-6" style="text-align:left" >
                            <b>Student Status : {{auth()->user()->status==1?'Active':'Deactive' }} </b>
                        </div>
                        <div class="col-md-6" style="text-align:right">
                           <b>Applied Count : 2 </b>
                        </div>
                   
                </div>
                @endif

               

                </h4>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title" style="text-align:left">
                                @if($student->recruiter_id==null)
                                <b>Name :{{ $student->user()->first()->name }}</b>
                                @else
                                <b>Recruiter Name: {{$student->recruiter()->first()->company_name}}</b>
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-6">
                            <h4 class="card-title" style="text-align:right"><b>ID : {{ $student->id }}</b></h4>
                        </div>
                    </div>
                    <p class="card-text">

                        {{-- Start of Collaps --}}

                    <div id="accordion" role="tablist" aria-multiselectable="true" class="card-collapse">


                        <div class="card card-plain">
                            <div class="card-header" role="tab" id="headingOne">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"
                                    aria-controls="collapseOne">
                                    <h4>BASIC INFO
                                        <i class="material-icons">keyboard_arrow_down</i>
                                    </h4>
                                </a>
                            </div>

                            <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-body">



                                    <table class="table">

                                        <tbody>
                                            <tr>

                                                <td style="font-weight: bold; min-width: 145px;">Title :</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $student->title }}</td>
                                                <td style="font-weight: bold">First Name :</td>
                                                <td style="text-align:left">{{ $student->first_name }}</td>

                                            </tr>

                                            <tr>

                                                <td style="font-weight: bold">Middle Name :</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $student->middle_name }}</td>
                                                <td style="font-weight: bold">Last Name :</td>
                                                <td style="text-align:left">{{ $student->middle_name }}</td>

                                            </tr>

                                            <tr>

                                                <td style="font-weight: bold">Gender :</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $student->gender }}</td>
                                                <td style="font-weight: bold">Date Of Birth :</td>
                                                <td style="text-align:left">{{ $student->date_of_birth }}</td>

                                            </tr>

                                            <tr>

                                                <td style="font-weight: bold">Email :</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $student->email }}</td>
                                                <td style="font-weight: bold">Mobile No. :</td>
                                                <td style="text-align:left">{{ $student->mobile }}</td>

                                            </tr>


                                            <tr>

                                                <td style="font-weight: bold">Emergency Contact:</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $student->emergency_contact }}</td>
                                                <td style="font-weight: bold">Address :</td>
                                                <td style="text-align:left">{{ $student->address }}</td>

                                            </tr>

                                            <tr>

                                                <td style="font-weight: bold">House No :</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $student->house_flat_no }}</td>
                                                <td style="font-weight: bold">Street :</td>
                                                <td style="text-align:left">{{ $student->street }}</td>

                                            </tr>


                                            <tr>

                                                <td style="font-weight: bold">Postal Code :</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $student->postal_code }}</td>
                                                <td style="font-weight: bold">City :</td>
                                                <td style="text-align:left">{{ $student->city }}</td>

                                            </tr>

                                            <tr>

                                                <td style="font-weight: bold">Country Of Birth :</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $student->country_of_birth }}</td>
                                                <td style="font-weight: bold">Nationality :</td>
                                                <td style="text-align:left">{{ $student->nationality }}</td>

                                            </tr>

                                            <tr>

                                                <td style="font-weight: bold">Ethnicity :</td>
                                                <td style="border-right: 1px solid gray; text-align:left">
                                                    {{ $student->ethnicity }}</td>
                                                <td style="font-weight: bold">Religion :</td>
                                                <td style="text-align:left">{{ $student->religion }}</td>

                                            </tr>



                                        </tbody>
                                    </table>



                                </div>
                            </div>
                          </div>


                            <div class="card card-plain">
                                <div class="card-header" role="tab" id="headingTwo">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                                        aria-expanded="false" aria-controls="collapseTwo">
                                        <h4>QUERYS
                                            <i class="material-icons">keyboard_arrow_down</i>
                                        </h4>

                                    </a>
                                </div>
                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="card-body">

                                        <table class="table">

                                            <tbody>
                                                <tr>

                                                    <td style="font-weight: bold">Which course are you applying for ? </td>
                                                    <td>{{ $student->start_study }}</td>

                                                </tr>
                                                <tr>

                                                    <td style="font-weight: bold">Preferred Country :</td>
                                                    <td>{{ $student->preferred_country }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Preferred Campus Location :</td>
                                                    <td>{{ $student->preferred_campus_location }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Have you ever applied for student VISA
                                                        before ? </td>
                                                    <td>{{ $student->applied_student_visa_before }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">If yes, please specify when and for what
                                                        course ? </td>
                                                    <td>{{ $student->student_visa_details }}</td>

                                                </tr>

                                                <tr>
                                                    <td colspan="2" style="font-weight: bold; text-decoration:underline;">
                                                        Declaration of Criminal Record</td>
                                                <tr>

                                                <tr>

                                                    <td style="font-weight: bold">Do you have any spent/unspent criminal
                                                        conviction? </td>
                                                    <td>{{ $student->declaration_of_criminal_record }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">If yes, please give details :</td>
                                                    <td>{{ $student->criminal_record_details }}</td>

                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>




                            <div class="card card-plain">
                                <div class="card-header" role="tab" id="headingThree">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                        href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <h4>Uplaoded Files
                                            <i class="material-icons">keyboard_arrow_down</i>
                                        </h4>
                                    </a>
                                </div>
                                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="card-body">

                                        <table class="table">

                                            <tbody>
                                                <tr>

                                                    <td style="font-weight: bold">Profile Picture </td>
                                                    <td>{{ $student->prof_pic_upload }}</td>
                                                    <td><button type="button" class="btn btn-info btn-sm">
                                                            <span class="material-icons">
                                                                get_app
                                                            </span>
                                                            Downlaod
                                                        </button></td>


                                                </tr>
                                                <tr>

                                                    <td style="font-weight: bold">Passport / National ID </td>
                                                    <td>{{ $student->passport_nid_upload }}</td>
                                                    <td><button type="button" class="btn btn-info btn-sm">
                                                            <span class="material-icons">
                                                                get_app
                                                            </span>
                                                            Downlaod
                                                        </button></td>


                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Full CV </td>
                                                    <td>{{ $student->cv_upload }}</td>
                                                    <td><button type="button" class="btn btn-info btn-sm">
                                                            <span class="material-icons">
                                                                get_app
                                                            </span>
                                                            Downlaod
                                                        </button></td>


                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Work Experience  </td>
                                                    <td>{{ $student->work_experience_upload }}</td>
                                                    <td><button type="button" class="btn btn-info btn-sm">
                                                            <span class="material-icons">
                                                                get_app
                                                            </span>
                                                            Downlaod
                                                        </button></td>


                                                </tr>


                                                <tr>

                                                    <td style="font-weight: bold">Purpose of Study statement </td>
                                                    <td>{{ $student->purpose_of_study_statement }}</td>
                                                    <td><button type="button" class="btn btn-info btn-sm">
                                                            <span class="material-icons">
                                                                get_app
                                                            </span>
                                                            Downlaod
                                                        </button></td>


                                                </tr>


                                                <tr>

                                                    <td style="font-weight: bold">Previous Qualification</td>
                                                    <td>{{ $student->previous_qualification_upload }}</td>
                                                    <td><button type="button" class="btn btn-info btn-sm">
                                                            <span class="material-icons">
                                                                get_app
                                                            </span>
                                                            Downlaod
                                                        </button></td>


                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Proof of English language </td>
                                                    <td>{{ $student->proof_of_english_language }}</td>
                                                    <td><button type="button" class="btn btn-info btn-sm">
                                                            <span class="material-icons">
                                                                get_app
                                                            </span>
                                                            Downlaod
                                                        </button></td>


                                                </tr>

                                                 <tr>

                                                    <td style="font-weight: bold">Proof of address </td>
                                                    <td>{{ $student->proof_of_address }}</td>
                                                    <td><button type="button" class="btn btn-info btn-sm">
                                                            <span class="material-icons">
                                                                get_app
                                                            </span>
                                                            Downlaod
                                                        </button></td>


                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Other Documents </td>
                                                    <td>{{ $student->others_document }}</td>
                                                    <td><button type="button" class="btn btn-info btn-sm">
                                                            <span class="material-icons">
                                                                get_app
                                                            </span>
                                                            Downlaod
                                                        </button></td>


                                                </tr>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>


                            <div class="card card-plain">
                                <div class="card-header" role="tab" id="headingFour">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                        href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <h4>Declaration
                                            <i class="material-icons">keyboard_arrow_down</i>
                                        </h4>
                                    </a>
                                </div>
                                <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="card-body">

                                        <table class="table">

                                            <tbody>
                                                 <tr>

                                                    <td style="font-weight: bold">Accuracy of information on this application form : </td>
                                                    <td>{{ $student->accuracy_of_information }}</td>

                                                </tr>
                                                <tr>

                                                    <td style="font-weight: bold">Please choose after reading info :</td>
                                                    <td>{{ $student->read_info_flag }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Comments :</td>
                                                    <td>{{ $student->read_info_flag }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Additional Information (if any) :</td>
                                                    <td>{{ $student->read_info_flag }}</td>

                                                </tr>


                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                        </p>

                    </div>
                </div>
            </div>

            @if(auth()->user()->role_id==2 || auth()->user()->role_id==3)
                <a type="button" class="btn btn-info pull-right" 
                        href="{{route('programRecruiter.index',[ 'id' => $student->id ])}}">
                            Add New Program
                </a>
            @endif
            <div class="card " style="width: 100%;" >

                {{-- <table class="table" id="test"> --}}
                    <table class="table">
                    <thead>
                        <tr>
                            <th colspan='5' class="card-header-info">List Of Applied Programs</th>
                          
                            </th>
                        </tr>
                        <tr class="card-header-info">
                            <th >institute</th>
                          
                            </th>
                            <th>Program Name</th>
                          
                            </th>
                            <th >Status</th>
                          
                            </th>
                            <th>last Updated On</th>
                          
                            </th>
                            <th >Action</th>
                          
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($student->programs as $program)
                            
                   
                        <tr>
                            <td class="text-center" >
                               
                                         {{$program->schoolInfo->school_name}} 
                            </td>
                            <td class="text-center" >
                                    
                                    {{$program->course_name}}
                                   
                                    </td>
                                    <td class="text-center" >
                                        {{$program->pivot->status}}
                                    </td>
                                    <td class="text-center" >
                                        {{$program->pivot->updated_at}}
                                    </td>
                                    <td class="text-center" >
                                        <a class="btn btn-sm btn-primary" href={{route('program.show',[ 'program' => $program->id ,'student_id' => $student->id] )}}>View</a>
                                    </td>
                                    
                                </div>
                            </td>
                           
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
    </div>


        @endsection


        @push('scripts')
        {{-- <script>
            $(function () {
               $('#test').DataTable();
            });
        </script> --}}
        @endpush
