@extends('layouts.backend.layouts.app')
@section('content')
@include('layouts.backend.supervisor.create-modal')
<div class="container-fluid">
    <div class="row">
      {{-- <div class="col-md-8"> --}}
        <div class="card">
         
          <div class="card-body table-responsive">
              <button type="button" class="btn btn-info pull-right" 
                data-toggle="modal" data-target=".create">
                Create New Supervisor</button>
                <div class="clearfix"></div>

              <br>
             
              <table class="table table-bordered table-hover" style="" id="get-supervisor-list"
                width="100%">
                <thead class="card-header-info no-shadow">
                  <tr style="text-align: center">
                        <th scope="col">SL</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Address</th>
                        <th scope="col">Office Address</th>
                        <th scope="col">Region</th>
                        <th scope="col">Note</th>
                        <th scope="col">Created By</th>
                        <th scope="col">Updated By</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Created At</th>
                        <th scope="col" style="min-width:220px">Action</th>
                  </tr>
                  </thead>
              </table>
          </div>
        </div>
      {{-- </div> --}}
    </div>
  </div>


@endsection

@push('scripts')
  <script>
      $(function() {
          $('#get-supervisor-list').DataTable({
                processing: true,
                serverSide: true,
                fixedHeader: true,
                responsive: true,
                //scrollX: true,
                //paging: false,
              ajax: '{{ url('get-supervisor-list') }}',
              columns: [
                  { data: 'DT_RowIndex', name: 'id', orderable: false },
                  { data: 'name', name: 'name', orderable: false },
                  { data: 'email', name: 'email', orderable: false },
                  { data: 'phone', name: 'phone', orderable: false },
                  { data: 'gender', name: 'gender', orderable: false,
                   render:
                          function(data, type, row) {
                              if(row.gender == 1){
                                  return 'Male';
                              }
                              if(row.gender == 2){
                                  return 'Female';
                              }
                          }
                   },
                  { data: 'address', name: 'address', orderable: false },
                  { data: 'office_address', name: 'office_address', orderable: false },
                  { data: 'region', name: 'region', orderable: false },
                  { data: 'note', name: 'note', orderable: false },
                  { data: 'created_by', name: 'created_by', orderable: false },
                  { data: 'updated_by', name: 'updated_by', orderable: false },
                  { data: 'registered_at', name: 'registered_at', orderable: false },
                  { data: 'activated_at', name: 'activated_at', orderable: false },
                //   { data: 'action', name: 'action', orderable: false },
                  {data: 'action', name: 'action', orderable: false,
                      render:
                          function(data, type, row) {
                                return '<div style="text-align:center" class="action-btn">'+
                                            '<a class="btn btn-primary btn-sm" href="/supervisor/'+row.id+'">View<a>'+
                                            // '<a class="btn btn-info btn-sm" href="deliveries/'+row.id+'/edit">Edit</a>'+
                                            // '<a class="btn btn-rose btn-sm" target="_blank" href="/print/delivery-info/'+row.id+'"><span class="material-icons">delete</span></a>'+
                                        '</div>';
                                
                                                         }
                  },
              ],
              columnDefs: [
                { 
                   // visible: false, 
                   //targets: [1 ,2],
                   // className: 'last-colum'
                }
              ],

          });

      });
  </script>
@endpush
