<div class="modal fade create" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    {{-- <div class="modal-dialog modal-lg modal-dialog-centered"> --}}
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Create New Supervisor</h5>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>

        <div class="modal-body">
            <form action="{{route('supervisor.store')}}" method="POST">
                @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label class="bmd-label-floating">Supervisor Name 
                            <span style="color: red">*</span></label>
                            <input name="name" type="text" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label class="bmd-label-floating">Email Address 
                            <span style="color: red">*</span></label>
                            <input name="email" type="email" class="form-control
                             @error('email') is-invalid @enderror" 
                             value="{{ old('email') }}" required>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label class="bmd-label-floating">Mobile Number
                            <span style="color: red">*</span></label>
                            <input name="phone" type="number" 
                              class="form-control @error('phone') is-invalid @enderror"
                              value="{{ old('phone') }}" required>

                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                            </div>
                        </div>
                        <div class="col-md-6">
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                            <label>Gender</label>
                            <select style="width: 100%" class="select2" name="gender">
                                {{-- @if (auth()->user()->role_id == 1) --}}
                                     <option ></option>
                                    <option value="1">Male</option>
                                     <option value="2">Female</option>
                                {{-- @endif --}}
                            </select>
                            </div>

                            <div class="form-group">
                                <label class="bmd-label-floating">Address </label>
                                <textarea name="address" class="form-control" 
                                id="exampleFormControlTextarea1" rows="5"
                                 spellcheck="false"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label>Nationality <span style="color: red">*</span></label>
                            <select style="width: 100%" class="select2" name="nationality" required>
                                    <option ></option>
                                    <option value="1">Enhgland</option>
                                    <option value="2">USA</option>
                               
                            </select>
                            </div>

                            <div class="form-group">
                                <label class="bmd-label-floating">Office Address 
                                </label>
                                <textarea name="office_address" class="form-control" 
                                id="exampleFormControlTextarea1" rows="5" spellcheck="false" 
                                ></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label class="bmd-label-floating">Supervisor Region 
                            <span style="color: red">*</span></label>
                            <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="radio" 
                                  name="region" id="exampleRadios1" value="1" checked>
                                  Asia
                                  <span class="circle">
                                    <span class="check"></span>
                                  </span>
                                </label>
                              </div>
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="radio" 
                                  name="region" id="exampleRadios2" value="2">
                                  UK
                                  <span class="circle">
                                    <span class="check"></span>
                                  </span>
                                </label>
                              </div>
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="radio" 
                                  name="region" id="exampleRadios2" value="3">
                                  America
                                  <span class="circle">
                                    <span class="check"></span>
                                  </span>
                                </label>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label class="bmd-label-floating">Note </label>
                            <textarea name="note" class="form-control" 
                            id="exampleFormControlTextarea1" rows="5" 
                            spellcheck="false"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">lock_outline</i>
                              </span>
                            </div>
                            <input id="password" type="password" placeholder="Enter Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">lock_outline</i>
                              </span>
                            </div>
                            <input id="password-confirm"  equalto="#password" type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                        <span id='message'></span>
                    </div>
                    
                </div>
                
                   <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <button type="reset" class="btn btn-danger pull-right">Reset</button>
                </div>
                   
                    
            </form>
        {{-- form End--}}

        </div>
      </div>
    </div>
  </div>
@push('scripts')
<script>
$('#password, #password-confirm').on('keyup', function () {
  if ($('#password').val() == $('#password-confirm').val()) {
    $('#message').html('Password Matched').css('color', 'green');
  } else 
    $('#message').html('Password Did Not Matched').css('color', 'red');
});
</script>
@endpush
