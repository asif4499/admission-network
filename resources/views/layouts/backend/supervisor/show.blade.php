@extends('layouts.backend.layouts.app')
@push('styles')
    <link href="{{ asset('/backend/css/table.css') }}" rel="stylesheet" />
@endpush
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="card card-nav-tabs">
                <h4 class="card-header card-header-info">
                    <div class="row">
                            <div class="col-md-6" style="text-align:left" >
                                <b>Supervisor Status : {{auth()->user()->status==1?'Active':'Deactive' }} </b>
                            </div>
                            <div class="col-md-6" style="text-align:right">
                            <b>Assigned Student Count : {{$student_count}} </b>
                            </div>
                    </div>
                </h4>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title" style="text-align:left"><b>Name :
                                    {{ $supervisor->user()->first()->name }}</b></h4>
                        </div>
                        <div class="col-md-6">
                            <h4 class="card-title" style="text-align:right"><b>ID : {{ $supervisor->id }}</b></h4>
                        </div>
                    </div>
                    <p class="card-text">

                        <div class="card-body">

                                        <table class="table">

                                            <tbody>
                                                <tr >

                                                   
                                                    <td colspan="2" style="background-color:#b7ebed; font-weight: bold; font-size :20px">Supervisor Details</td>

                                                </tr>
                                                <tr>

                                                    <td style="font-weight: bold; ">Supervisor Name :</td>
                                                    <td style="text-align:left">{{ $supervisor->name }}</td>

                                                </tr>
                                                <tr>

                                                    <td style="font-weight: bold">Email Address :</td>
                                                    <td style="text-align:left">{{ $supervisor->email }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Mobile Number :</td>
                                                    <td style="text-align:left">{{ $supervisor->phone }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Gender :</td>
                                                    <td style="text-align:left">{{ $supervisor->gender }}</td>

                                                </tr>

                                                <tr>

                                                    <td style="font-weight: bold">Address :</td>
                                                    <td style="text-align:left">{{ $supervisor->address }}</td>

                                                </tr>


                                                 <tr>

                                                    <td style="font-weight: bold">Nationality :</td>
                                                    <td style="text-align:left">{{ $supervisor->nationality }}</td>

                                                </tr>

                                                 <tr>

                                                    <td style="font-weight: bold">Office Address :</td>
                                                    <td style="text-align:left">{{ $supervisor->address }}</td>

                                                </tr>

                                                 <tr>

                                                    <td style="font-weight: bold">Address :</td>
                                                    <td style="text-align:left">{{ $supervisor->office_address }}</td>

                                                </tr>


                                                 <tr>

                                                    <td style="font-weight: bold">Supervisor Region :</td>
                                                    <td style="text-align:left">{{ $supervisor->region }}</td>

                                                </tr>

 

                                                <tr>

                                                    <td style="font-weight: bold">Note :</td>
                                                    <td style="text-align:left">{{ $supervisor->note }}</td>

                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
              

                    </p>
                    @include('layouts.backend.recruiter.index')
                    </div>
                    
                </div>
            </div>
            <div id="mydiv" data-myval="{{$supervisor->id}}"></div>

        @endsection


        @push('scripts')
        <script>
            $(function() {
                var id = $('#mydiv').data('myval');
                $('#get-student-list').DataTable({
                    
                      processing: true,
                      serverSide: true,
                      responsive: true,
                      //scrollX: true,
                      // paging: false,
                    ajax:{
                        url:'{{ route("get-studentSupervisor-list") }}',
                        data:{'id':id},
                    } ,
                    
                    columns: [
                     
                      { data: 'DT_RowIndex', name: 'id', orderable: false },
                      {data:'title',name:'title',orderable:false},
                      {data:'first_name',name:'first_name',orderable:false},
                      {data:'middle_name',name:'middle_name',orderable:false},
                      {data:'last_name',name:'last_name',orderable:false},
                      {data:'gender',name:'gender',orderable:false,
                       render:
                                function(data, type, row) {
                                    if(row.gender == 1){
                                        return 'Male';
                                    }
                                    if(row.gender == 2){
                                        return 'Female';
                                    }
                                }
                      },
                      {data:'date_of_birth',name:'date_of_birth',orderable:false},
                      {data:'email',name:'email',orderable:false},
                      {data:'mobile',name:'mobile',orderable:false},
                      {data:'emergency_contact',name:'emergency_contact',orderable:false},
                      {data:'supervisor_id',name:'supervisor_id',orderable:false},
                      { data: 'registered_at', name: 'registered_at', orderable: false },
                      { data: 'activated_at', name: 'activated_at', orderable: false },
                     
                      
                      
                
                      
                      {data: 'action', name: 'action', orderable: false,
                            render:
                                function(data, type, row) {
                                      return '<div style="text-align:center" class="action-btn">'+
                                                  '<a class="btn btn-primary btn-sm" href="/students-form/'+row.id+'">View<a>'+
                                              '</div>';
      
                                }
                        },
                    ],
                initComplete: function () {
                      $('.role').select2({
                        placeholder: "Select an option",
                        allowClear: true
                      });
                  },
      
                });
      
            });
            
        </script>
        @endpush
