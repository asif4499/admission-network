@extends('layouts.backend.layouts.app')
@section('content')
<div class="container-fluid">
    
    <div class="row">
      <div class="card" style="width: 100%">
        <div class="card-body " >

          <div class="card-header card-header-info">
            <ul class="nav nav-tabs">
              <li class="nav-item">
                <a class="nav-link {{ (request()->is('dashboard/school-index')) ? 'active' : '' }}" 
                  href="{{route('school.index')}}" ><b>Institutions</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link {{ (request()->is('program')) ? 'active' : '' }}" 
                  href="{{route('program.index')}}"><b>Programs</b></a>
              </li>
           
            </ul>
          </div>
        </div>
        @php
           
        @endphp
      <div style="margin: 10px">
        <table  id="test"><thead><tr><th>
                          <h3> List Of All The  Programs : </h3>
                        </thead></th></tr>
                        
                                  <tbody>    
                                    <tr><td></td></tr> 
                                @foreach ($programs as $program) 
                                <tr><td>
                                    <div class="card-header card-header-info">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4 style="font-weight: 600; text-align: left">Institution:  {{$program->schoolInfo->school_name}}
                                                </h4></div>
                                            <div class="col-sm-4">
                                                 <h5 style="font-weight: 600; text-align: center">Course:  {{$program->course_name}}
                                                 </h5></div>
                                            <div class="col-sm-4">
                                              @if(auth()->user()->role_id==2)
                                                <form action="{{route('program.apply',[ 'id'=>$program->id,'student_id' => auth()->user()->student->id ])}}" method="POST">
                                                 
                                                    @csrf
                                                    @method('PUT')
                                                    @php($a=false)
                                                    @if(auth()->user()->student->programs->count()==0)
                                                      <button type="submit" class="btn btn-danger btn-sm" >Apply</button>
                                                    @else
                                                      @foreach(auth()->user()->student->programs as $iteam)
                                                        @if($iteam->id==$program->id)
                                                          @php($a=true)
                                                       
                                                        @endif
                                                       
                                                      @endforeach
                                                      <button type="submit" class="btn btn-danger btn-sm" {{$a?'disabled':''}} >Apply</button>
                                                    @endif
                                                   
                                                    <a type="button" href="{{route('program.showforall',[ 'id' => $program->id ])}}" class="btn btn-primary btn-sm" >View</a>
                                                    
                                                </form>
                                               @endif
                                            </div>
                                        </div>
                                       
                                            
                                    </div>
                                    
                                      
                                      <div >
                                        <table class="table" style="table-layout: fixed">
                                          
                                          <tbody>
                                            <tr>
                                                
                                              </tr>
                                            <tr>
                                              <th>Programe Level</th>
                                              <td>{{$program->programe_level}}</td>
                                            
                                                <th>Intakes</th>
                                                <td>{{$program->intakes}}</td>
                                              </tr>
                                              <tr>
                                                <th>Course Duration</th>
                                                <td>{{$program->course_duration}}</td>
                                             
                                                <th>Tution Free</th>
                                                <td>{{$program->tution_free}}</td>
                                              </tr>
                                              <tr>
                                                <th>Admission Free</th>
                                                <td>{{$program->admission_free}}</td>
                                                <th>Language Profeciency(English)</th>
                                                <td>{{$program->language_profeciency}}</td>
                                              </tr>

                                          </tbody>
                                        </table>
                                      </div>
                                  
                                    
                                  </td></tr> 
                                  @endforeach
                                </tbody></table>
                              </div>
                                </div>
                            </div> 
                        </div>
@endsection

