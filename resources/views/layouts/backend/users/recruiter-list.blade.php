@extends('layouts.backend.layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
      {{-- <div class="col-md-8"> --}}
        <div class="card">
         
          <div class="card-body table-responsive">
              {{-- <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".create">Create New Role</button> --}}
              <div class="clearfix"></div>
              <br>
             
              <table class="table table-bordered table-hover" style="" id="get-recruiter-list"  width="100%">
                  <thead class="card-header-info no-shadow">
                  <tr style="text-align: center">
                      <th scope="col">SL</th>
                      <th scope="col">Company Name</th>
                      <th scope="col">Email</th>
                      <th scope="col">Website</th>
                      <th scope="col">Facebook Page</th>
                      <th scope="col">Registered At</th>
                      <th scope="col">Activated At</th>
                      <th scope="col">Action</th>
                  </tr>
                  </thead>
              </table>
          </div>
        </div>
      {{-- </div> --}}
    </div>
  </div>


@include('layouts.backend.deliveries.delete-modal')
@include('layouts.backend.deliveries.view-admin')

@endsection

@push('scripts')
  <script>
      $(function() {
          $('#get-recruiter-list').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                //scrollX: true,
                // paging: false,
              ajax: '{{ url('get-recruiter-list') }}',
              columns: [
                  { data: 'DT_RowIndex', name: 'id', orderable: false },
                  { data: 'company_name', name: 'company_name', orderable: false },
                  { data: 'email', name: 'email', orderable: false },
                  { data: 'website', name: 'website', orderable: false },
                  { data: 'facebook_page_name', name: 'facebook_page_name', orderable: false },
                      
                  { data: 'registered_at', name: 'registered_at', orderable: false },
                  { data: 'activated_at', name: 'activated_at', orderable: false },
                //   { data: 'action', name: 'action', orderable: false },
                  {data: 'action', name: 'action', orderable: false,
                      render:
                          function(data, type, row) {
                                return '<div style="text-align:center" class="action-btn">'+
                                            '<a class="btn btn-primary btn-sm" href="/recruiters-form/'+row.id+'">View<a>'+
                                            // '<a class="btn btn-info btn-sm" href="deliveries/'+row.id+'/edit">Edit</a>'+
                                            // '<a class="btn btn-rose btn-sm" target="_blank" href="/print/delivery-info/'+row.id+'"><span class="material-icons">delete</span></a>'+
                                        '</div>';

                          }
                  },
              ],
              columnDefs: [
                { 
                   // visible: false, 
                   //targets: [1 ,2],
                   // className: 'last-colum'
                }
              ],

          });

      });
  </script>
@endpush
