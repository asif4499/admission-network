@extends('layouts.backend.layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
      {{-- <div class="col-md-8"> --}}
        <div class="card">
          <div class="card-body ">

            <div class="card-header card-header-info">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a class="nav-link {{ (request()->is('dashboard/school-index')) ? 'active' : '' }}" 
                    href="{{route('school.index')}}" ><b>Institutions</b></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ (request()->is('dashboard/program-index')) ? 'active' : '' }}" 
                    href="{{route('program.index')}}"><b>Programs</b></a>
                </li>
             
              </ul>
            </div>
          </div>



          <div class="card-body table-responsive">
              {{-- <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".create">Create New Role</button> --}}
              <div class="clearfix"></div>
              <br>
             
              <table class="table table-bordered table-hover" style="" id="get-school-list"  width="100%">
                  <thead class="card-header-info no-shadow">
                  <tr style="text-align: center">
                      <th scope="col">SL</th>
                      <th scope="col">Destination Country</th>
                      <th scope="col">Intitution Name</th>
                      <th scope="col">Institution Type</th>
                      @if(auth()->user()->role_id==1)
                      <th scope="col">Email</th>
                      <th scope="col">Number</th>
                      <th scope="col">Registered At</th>
                      <th scope="col">Activated At</th>
                      @endif
                      <th scope="col">View</th>
                  </tr>
                  </thead>
              </table>
          </div>
        </div>
      {{-- </div> --}}
    </div>
    </div>
  </div>



@endsection

@push('scripts')
  <script>
      $(function() {
          $('#get-school-list').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                //scrollX: true,
                // paging: false,
              ajax: '{{ url('get-school-list') }}',
              columns: [
                  { data: 'DT_RowIndex', name: 'id', orderable: false },
                  { data: 'country_code', name: 'country_code', orderable: false },
                  { data: 'school_name', name: 'school_name', orderable: false },
                  { data: 'institution_type', name: 'institution_type', orderable: false },
                  @if(auth()->user()->role_id==1)
                  { data: 'contact_email', name: 'contact_email', orderable: false },
                  { data: 'contact_number', name: 'contact_number', orderable: false },
                      
                  { data: 'registered_at', name: 'registered_at', orderable: false },
                  { data: 'activated_at', name: 'activated_at', orderable: false },
                  @endif
                //   { data: 'action', name: 'action', orderable: false },
                  {data: 'action', name: 'action', orderable: false,
                      render:
                          function(data, type, row) {
                                return '<div style="text-align:center" class="action-btn">'+
                                            '<a class="btn btn-primary btn-sm" href="/school-form/'+row.id+'">View<a>'+
                                            // '<a class="btn btn-info btn-sm" href="deliveries/'+row.id+'/edit">Edit</a>'+
                                            // '<a class="btn btn-rose btn-sm" target="_blank" href="/print/delivery-info/'+row.id+'"><span class="material-icons">delete</span></a>'+
                                        '</div>';

                          }
                  },
              ],
              columnDefs: [
                { 
                   // visible: false, 
                   //targets: [1 ,2],
                   // className: 'last-colum'
                }
              ],

          });

      });
  </script>
@endpush
