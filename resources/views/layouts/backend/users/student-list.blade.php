@extends('layouts.backend.layouts.app')
@section('content')
<div class="container-fluid">               
    <div class="row">
        <div class="card">
          {{-- <div class="card-header card-header-text card-header-primary">
              <div class="card-text">
                <h4 class="card-title">Here is the Text</h4>
              </div>
          </div> --}}
          <div class="card-body table-responsive">
              {{-- <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".create">Create New Role</button> --}}
             
              <table class="table table-bordered table-hover" 
              id="get-student-list"  width="100%">
                 <thead class="card-header-info no-shadow">
                  <tr style="text-align: center">
                        <th scope="col">SL</th>
                        <th scope="col">Title</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Middle Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Gender</th>
                        <th scope="col">DOB</th>
                        <th scope="col">Email</th>
                        <th scope="col">Mobile No.</th>
                        <th scope="col">Emergency Contact</th>
                        <th scope="col">Assigned Supervisor</th>
                        <th scope="col">Registered At</th>
                        <th scope="col">Activated At</th>
                        <th scope="col" style="min-width:150px">Action</th>
                  </tr>
                </thead>
              </table>
          </div>
        </div>
    </div>
  </div>


@endsection

@push('scripts')
  <script>
      $(function() {
          $('#get-student-list').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                //scrollX: true,
                // paging: false,
              ajax: '{{ url('get-student-list') }}',
              columns: [
               
                { data: 'DT_RowIndex', name: 'id', orderable: false },
                {data:'title',name:'title',orderable:false},
                {data:'first_name',name:'first_name',orderable:false},
                {data:'middle_name',name:'middle_name',orderable:false},
                {data:'last_name',name:'last_name',orderable:false},
                {data:'gender',name:'gender',orderable:false,
                 render:
                          function(data, type, row) {
                              if(row.gender == 1){
                                  return 'Male';
                              }
                              if(row.gender == 2){
                                  return 'Female';
                              }
                          }
                },
                {data:'date_of_birth',name:'date_of_birth',orderable:false},
                {data:'email',name:'email',orderable:false},
                {data:'mobile',name:'mobile',orderable:false},
                {data:'emergency_contact',name:'emergency_contact',orderable:false},
                {data:'supervisor_id',name:'supervisor_id',orderable:false},
                { data: 'registered_at', name: 'registered_at', orderable: false },
                { data: 'activated_at', name: 'activated_at', orderable: false },
               
                
                
          
                
                {data: 'action', name: 'action', orderable: false,
                      render:
                          function(data, type, row) {
                                return '<div style="text-align:center" class="action-btn">'+
                                            '<a class="btn btn-primary btn-sm" href="/students-form/'+row.id+'">View<a>'+
                                        '</div>';

                          }
                  },
              ],
          initComplete: function () {
                $('.role').select2({
                  placeholder: "Select an option",
                  allowClear: true
                });
            },

          });

      });
      
  </script>
   
  <script src="{{asset('backend/js/select2.min.js')}}" type="text/javascript"></script>
@endpush
