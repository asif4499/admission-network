@extends('layouts.backend.layouts.app')
@section('content')


<div class="container-fluid">
    <div class="row">
      {{-- <div class="col-md-8"> --}}
        <div class="card">
          <div class="card-body ">
                <div class="card-header card-header-info">
                  <ul class="nav nav-tabs">
                    <li class="nav-item">
                      <a class="nav-link {{ (request()->is('dashboard/allusrIndex/1')) ? 'active' : '' }}" 
                        href="{{route('alluser.index',[ 'role_id' => 1 ])}}" ><b>All Users</b></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link {{ (request()->is('dashboard/allusrIndex/3')) ? 'active' : '' }}" 
                        href="{{route('alluser.index',[ 'role_id' => 3 ])}}"><b>Recruiters</b></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link {{ (request()->is('dashboard/allusrIndex/2')) ? 'active' : '' }}" 
                        href="{{route('alluser.index',[ 'role_id' => 2 ])}}"><b>Students</b></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('dashboard/allusrIndex/5')) ? 'active' : '' }}" 
                          href="{{route('alluser.index',[ 'role_id' => 5 ])}}"><b>Schools and Universities</b></a>
                      </li>
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('dashboard/allusrIndex/4')) ? 'active' : '' }}" 
                          href="{{route('alluser.index',[ 'role_id' => 4 ])}}"><b>Supervisors</b></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{ (request()->is('dashboard/allusrIndex/10')) ? 'active' : '' }}" 
                          href="{{route('alluser.index',[ 'role_id' => 10 ])}}"><b>Requested User List</b></a>
                      </li>
                  </ul>
                </div>
          <div class="card-body table-responsive">
              {{-- <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".create">Create New Role</button> --}}
              <div class="clearfix"></div>
              <br>
             
              <table class="table table-bordered table-hover" style="" id="get-user-list"
                width="100%">
                  <thead class="card-header-info no-shadow">
                  <tr style="text-align: center">
                      <th scope="col">SL</th>
                      <th scope="col">Name</th>
                      <th scope="col">Email</th>
                      <th scope="col">Phone</th>
                      <th scope="col">Gender</th>
                      <th scope="col">Addresse</th>
                      {{-- <th scope="col">Address</th>
                      <th scope="col">Phone</th> --}}
                      <th scope="col">Role</th>
                      <th scope="col">Status</th>
                      <th scope="col">Registered At</th>
                      <th scope="col">Activated At</th>
                      <th scope="col">Action</th>
                  </tr>
                  </thead>
              </table>
          </div>
        </div>
      {{-- </div> --}}
    </div>
  </div>

  <div id="mydiv" data-myval="{{$role_id}}"></div>
@include('layouts.backend.deliveries.delete-modal')
@include('layouts.backend.deliveries.view-admin')

@endsection

@push('scripts')
  <script>
      $(function() {
        var role_id = $('#mydiv').data('myval');

          $('#get-user-list').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                //scrollX: true,
                // paging: false,
                ajax:{
                        url:'{{ url('get-user-list') }}',
                        data:{'role_id':role_id},
                    } ,
              columns: [
                  { data: 'DT_RowIndex', name: 'id', orderable: false },
                  { data: 'name', name: 'name', orderable: false },
                  { data: 'email', name: 'email', orderable: false },
                  { data: 'phone', name: 'phone', orderable: false },
                  { data: 'gender', name: 'gender', orderable: false,
                   render:
                          function(data, type, row) {
                              if(row.gender == 1){
                                  return 'Male';
                              }
                              if(row.gender == 2){
                                  return 'Female';
                              }
                              if(row.gender == null){
                                  return 'X';
                              }
                          }
                   },
                  { data: 'address', name: 'address', orderable: false },
                  { data: 'role_id', name: 'role_id', orderable: false,
                  render:
                          function(data, type, row) {
                              if(row.role_id == 2){
                                  return 'Student';
                              }
                              if(row.role_id == 3){
                                  return 'Recruiter';
                              }
                              if(row.role_id == 4){
                                  return 'Supervisor';
                              }
                              if(row.role_id == 5){
                                  return 'Institute';
                              }
                              if(row.role_id == 1){
                                  return 'Admin';
                              }
                              if(row.role_id == null){
                                  return 'X';
                              }
                          }
                   },
                  { data: 'status', name: 'status', orderable: false ,
                      render:
                          function(data, type, row) {
                              if(row.status == 1){
                                  return '<span class="badge badge-pill badge-success">Active</span>';
                              }
                              if(row.status == 4){
                                  return '<span class="badge badge-pill badge-danger">Deactive</span>';
                              }
                              if(row.status == 3){
                                  return '<span class="badge badge-pill badge-danger">Incomplete Registration</span>';
                              }
                              if(row.status != 4 && row.status != 1 ){
                                  return '<span class="badge badge-pill badge-info">Waiting for Approval</span>';
                              }

                          }
                  },
                  { data: 'registered_at', name: 'registered_at', orderable: false },
                  { data: 'activated_at', name: 'activated_at', orderable: false },
                //   { data: 'action', name: 'action', orderable: false },
                  {data: 'action', name: 'action', orderable: false,
                      render:
                          function(data, type, row) {
                                return "<form action=\"user-approval/" + row.id + "\" method=\"POST\"><input type=\"hidden\" name=\"_method\" value=\"PUT\">"+
                                "<input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token() }}\"><input class='"+((row.status==1)?'btn btn-sm btn-danger':'btn btn-sm btn-info')+"' type=\"submit\" value='"+ ((row.status!=3)?((row.status==1)?'Deactivate':'Activate'):'Cant Perform any Action') +"' "+ ((row.status==3)?'disabled':'') +"></input>"+
                                "</form>";
                                                         }
                  },
              ],
              columnDefs: [
                { 
                   // visible: false, 
                   //targets: [1 ,2],
                   // className: 'last-colum'
                }
              ],
         

          });

      });
  </script>
@endpush
