@extends('layouts.frontend.layouts.app')
@section('content')
<section style="padding-top: 10em;">
    <div style="padding: 5em 0;height: 90vh;background-color: #0064e1; background-image: url('frontend/img/About_Hero_uirv.png');background-repeat:no-repeat;background-size:cover">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="">
                        <h1 style="color: #fff">
                            Admission Network
                        </h1>
                        <h2 style="color:#fff; font-weight:300">
                            We encourage, advise and give our students the opportunity to access higher education in a range of prestigious Universities and Colleges around the world to give them a better prospect for their future endeavours
                        </h2>
                    </div>
                    <br>
                    <button style="background-color: #fff; color:#0064e1" type="button" class="btn btn-lg">Our Story <i class='bx bx-right-arrow-circle'></i></button>
                </div>
                <div class="col-md-6">
                    {{-- <img src="{{asset('frontend/img/Students_Hero_Image_uirv.png')}}" class="img-fluid" alt=""> --}}
                </div>
            </div>
        </div>
    </div>
</section>

<section style="padding: 5em 0;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="center">
                    <h1 style="color: #0064e1">
                        What We Do
                    </h1>
                    <hr>
                    <div id="accordion">
                        <div class="card">
                          <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                              <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Students
                              </button>
                            </h5>
                          </div>

                          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Using the Admission Network Platform, students discover programs and apply to study at institutions that best meet their background and interests. Our team of experts provide students with support every step of the way including reviewing their applications, ensuring all documents are submitted, communicating with the institutions, assisting with the visa process, and starting their journey abroad!
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Schools
                              </button>
                            </h5>
                          </div>
                          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                By working with Admission Network, educational institutions gain access to Admission Network's managed network of 4,000 recruitment partners around the world. Admission Network actively recruits students from 110+ countries to bring culture and diversity to campuses. Admission Network reviews all applications before submission, ensuring that students are qualified and that applications adhere to the school's guidelines.
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Recruitment Partners
                              </button>
                            </h5>
                          </div>
                          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Admission Network works with 4,000+ recruitment partners to make education accessible to students. Through the Admission Network Platform, recruitment partners can manage their student applications and information while staying up-to-date on application statuses. Admission Network submits the student's application to the school and provides commissions once the student is enrolled.
                            </div>
                          </div>
                        </div>
                      </div>
                </div>
            </div>

            <div class="col-md-6">
                <img src="{{asset('frontend/img/What_We_Do_uirv.png')}}" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</section>


{{-- <section style="padding: 5em 0" class="container"> --}}
<section class="container">
    <div style="text-align: center">
        <h1>
            Our Values
        </h1>
        <h5 style="color:#6F6F6F; font-weight:300">
            At Admission Network, we commit to:
        </h5>
    </div>

    <div class="row">
        <div style="text-align: center" class="col-md-4">
            <div class="card shadow">
                <div class="card-body">
                  <img class="img-fluid" src="{{asset('frontend/img/Student_Success_uirv.png')}}" alt="">
                  <br>
                  <br>
                <h4>
                    Helping Students Achieve Success
                </h4>
                </div>
            </div>
        </div>

        <div style="text-align: center" class="col-md-4">
            <div class="card shadow">
                <div class="card-body">
                <img class="img-fluid" src="{{asset('frontend/img/Caring_uirv.png')}}" alt="">
                <br>
                <br>
                <h4>
                    Caring About Each Other
                </h4>
                </div>
            </div>
        </div>

        <div style="text-align: center" class="col-md-4">
            <div class="card shadow">
                <div class="card-body">
                    <img class="img-fluid" src="{{asset('frontend/img/Customer_Experience_uirv.png')}}" alt="">
                    <br>
                    <br>
                 <h4>
                    Delivering an A+ Customer Experience
                 </h4>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div style="text-align: center" class="col-md-4">
            <div class="card shadow">
                <div class="card-body">
                  <img class="img-fluid" src="{{asset('frontend/img/Ownership_uirv.png')}}" alt="">
                  <br>
                  <br>
                  <h4>
                    Taking
                    Ownership
                  </h4>
                </div>
            </div>
        </div>

        <div style="text-align: center" class="col-md-4">
            <div class="card shadow">
                <div class="card-body">
                <img class="img-fluid" src="{{asset('frontend/img/Innovating_uirv.png')}}" alt="">
                <br>
                <br>
                 <h4>
                    Innovating and Improving
                 </h4>
                </div>
            </div>
        </div>

        <div style="text-align: center" class="col-md-4">
            <div class="card shadow">
                <div class="card-body">
                    <img class="img-fluid" src="{{asset('frontend/img/SFun_uirv.png')}}" alt="">
                    <br>
                    <br>
                    <h4>
                        Making Work Fun
                    </h4>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
