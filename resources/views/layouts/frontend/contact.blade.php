@extends('layouts.frontend.layouts.app')
@section('content')
<section style="padding-top: 10em;">
    <div style="padding: 5em 0">
        <div class="container">
            <h1 style="text-align: center">
                HEAD OFFICE
            </h1>
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                          <h4 style="text-align: center">
                              Email
                          </h4>
                          <address style="text-align: center">
                            For General enquiries <a href="enquiries@admissionnetwork.net">enquiries@admissionnetwork.net</a><br>
                          </address>
                          <address style="text-align: center">
                            For enquiries from the UK <a href="UK@admissionnetwork.net">UK@admissionnetwork.net</a><br>
                          </address>
                        </div>
                      </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 style="text-align: center">
                                Phone
                            </h4>
                            <p style="text-align: center">
                                T: 0203 00 23 555
                            </p>
                            <p style="text-align: center">
                                M: 07961434456 / 07949779012
                            </p>
                          </div>
                      </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 style="text-align: center">
                                Addresss
                            </h4>
                            <p style="text-align: center">
                                160-170 Cannon Street Road (First Floor),London E1 2LH
                            </p>
                          </div>
                      </div>
                </div>
            </div>
            <div class="row">
                <p>
                    If you are an <b>existing customer</b>, contact your Account Representative for assistance. For questions about student applications, please leave a message in the Notes of the application and our Customer Experience team will respond.
                </p>
            </div>
        </div>
    </div>
</section>
@endsection
