@extends('layouts.frontend.layouts.app')
@section('content')
    <section style="padding-top: 10em;">
        <div class="card">
            <br>
            <div class="card-body">
                <h3 style="text-align: center; color:#43a047">
                    {{$data->title}}
                </h3>
            </div>
            @if($data->details)
            <div class="container card">
                <div class="card-body">
                  <h5 class="card-title"><span style="color: #444444">About</span> <span style="color:#43a047">{{$data->title}}</span></h5>
                  <hr>
                  <p class="card-text">
                      {!! $data->details !!}
                  </p>
                </div>
                <br>
                <div class="owl-carousel">
                    @foreach($images as $image)
                    <div>
                        <img src="/country-img/{{$image}}" alt="">
                    </div>
                    @endforeach
                  </div>
              </div>
              @else
              <br>
              <h4 style="text-align:center; color:#e53935">
                No details available
              </h4>
              @endif
          </div>
    </section>

@push('styles')
<link href="{{asset('owl-carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
<link href="{{asset('owl-carousel/assets/owl.theme.default.css')}}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{asset('owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script>

<script>
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        items:3,
        loop:true,
        margin:10,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        lazyLoad: true,
        rtl:true,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    });
</script>
@endpush
