<div id="exampleModalLongTitle" class="modal fade create" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    {{-- <div class="modal-dialog modal-lg modal-dialog-centered"> --}}
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" >Add Campus Details</h5>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>

        <div class="modal-body">
            <form action="{{route('campus.store')}}" method="POST">
                @csrf
                <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                      <label class="bmd-label-floating">Campus Address
                      <span style="color: red">*</span></label>
                      <input name="address" type="text" 
                      class="form-control" required>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                      <label class="bmd-label-floating">Zip
                      <span style="color: red">*</span></label>
                      <input name="zip" type="zip" class="form-control
                       @error('zip') is-invalid @enderror" 
                       value="{{ old('zip') }}" required>

                      @error('zip')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                      @enderror

                      </div>
                  </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label class="bmd-label-floating">City
                    <span style="color: red">*</span></label>
                    <input name="city" type="text" 
                    class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                  <label class="bmd-label-floating">State
                  <span style="color: red">*</span></label>
                  <input name="state" type="text" 
                  class="form-control" required>
                  </div>
              </div>
                
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                <label>Country</label>
                <select style="width: 100%" class="select2" name="Country">
                          <option ></option>
                        <option value="1">Bangladesh</option>
                          <option value="2">Australia</option>
                </select>
                </div>
              </div>
            </div>
                   <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <button type="reset" class="btn btn-danger pull-right">Reset</button>
              
                   
                    
            </form>
        {{-- form End--}}

        </div>
      </div>
    </div>
  </div>


  <div id="exampleModalLongTitle1" class="modal fade create" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    {{-- <div class="modal-dialog modal-lg modal-dialog-centered"> --}}
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" >Add Program</h5>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>

        <div class="modal-body">
            <form action="{{route('program.store')}}" method="POST">
                @csrf
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Course Name
                      <span style="color: red">*</span></label>
                      <input name="course_name" type="text" 
                      class="form-control" 
                      value="{{ old('course_name') }}" required>

                      @error('course_name')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                  </div>
                <div class="col-md-6">
                  <div class="form-group">
                  <label>Program Level</label>
                  <select style="width: 100%" class="select2" name="programe_level">
                            <option ></option>
                            <option value="GRADE-1">GRADE-1</option>
                            <option value="GRADE-2">GRADE-2</option>
                            <option value="GRADE-3">GRADE-3</option>
                            <option value="GRADE-4">GRADE-4</option>
                            <option value="GRADE-5">GRADE-5</option>
                            <option value="GRADE-6">GRADE-6</option>
                            <option value="GRADE-7">GRADE-7</option>
                            <option value="1-Year Post-Secondary Certificate">1-Year Post-Secondary Certificate</option>
                            <option value="2-Year Under Graduate Diploma">2-Year Under Graduate Diploma</option>
                            <option value="3-Year Under Graduate Advance Diploma">3-Year Under Graduate Advance Diploma</option>
                            <option value="3- Year Bachelors Degree">3- Year Bachelors Degree</option>
                            <option value="4- Year Bachelors Degree">4- Year Bachelors Degree</option>
                            <option value="PostGraduate Certificate/Diploma">PostGraduate Certificate/Diploma</option>
                            <option value="Master Degree">Master Degree</option>
                            <option value="Doctoral Degree (Phd, M.D., Others)">Doctoral Degree (Phd, M.D., Others)</option>
                  </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Intakes</label>
                    <select style="width: 100%" class="select2" name="intakes">
                              <option ></option>
                              <option value="Summer">Summer</option>
                              <option value="Spring">Spring</option>
                              <option value="Fall">Fall</option> 
                    </select>
                  </div>
                </div>
                
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Course Duration
                      <span style="color: red">*</span></label>
                      <input name="course_duration" type="text" 
                      class="form-control" 
                      value="{{ old('course_duration') }}" required>

                      @error('course_duration')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                  </div>
              
                
            </div>


            
            <div class="row">
                    
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Seasons
                          <span style="color: red">*</span></label>
                          <input name="seasons" type="text" 
                          class="form-control" 
                          value="{{ old('seasons') }}" required>

                          @error('seasons')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                        </div>
                      </div>
                  
                  
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">Tuition Fee
                        <span style="color: red">*</span></label>
                        <input name="tution_free" type="text" 
                        class="form-control" 
                        value="{{ old('tution_free') }}" required>

                        @error('tution_free')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                      </div>
                    </div>
                
            </div>

            <div class="row">

              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Admission Fee
                  <span style="color: red">*</span></label>
                  <input name="admission_free" type="text" 
                  class="form-control" 
                  value="{{ old('admission_free') }}" required>

                  @error('admission_free')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>Language Profeciency (English)</label>
                  <select style="width: 100%" class="select2" name="language_profeciency">
                            <option ></option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                  </select>
                </div>
              </div>

              
            </div>


            <div class="row">

              <div class="col-md-6">
                <div class="form-group">
                  <label>Required English Exam Type</label>
                  <select style="width: 100%" class="select2" name="required_exam_type">
                            <option ></option>
                            <option value="I Don't Have This ">I Don't Have This </option>
                            <option value="I Will Provide this">I Will Provide this</option>
                            <option value="TOFEEL ">TOFEEL </option>
                            <option value="IELTS">IELTS</option>
                            <option value="Doulingo English Test">Doulingo English Test</option>
                  </select>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>Visa Type Required </label>
                  <select style="width: 100%" class="select2" name="required_visa_type">
                            <option ></option>
                            <option value="I Don't Have This">I Don't Have This</option>
                            <option value="USA F1 VISA">USA F1 VISA</option>
                            <option value="SCanadian Stydy Permit Visa Or Visitor Visapring">Canadian Stydy Permit Visa Or Visitor Visa</option>
                            <option value="UK Student Visa (Tier -4)">UK Student Visa (Tier -4)</option>
                            <option value="Short Term Study Visa"> Short Term Study Visa</option>
                            <option value="Australian Study Visa">Australian Study Visa</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Required Grading Scheme</label>
                  <select style="width: 100%" class="select2" name="required_grading_scheme">
                            <option ></option>
                            <option value="0-100">0-100</option>
                            <option value="No">Other</option>
                  </select>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>Required Grade Scale*(out of)</label>
                  <select style="width: 100%" class="select2" name="required_grade_scale">
                            <option ></option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="100">100</option>
                  </select>
                </div>
              </div>

              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Required Grade Average
                    <span style="color: red">*</span></label>
                    <input name="required_grade_avg" type="text" 
                    class="form-control" 
                    value="{{ old('required_grade_avg') }}" required>

                    @error('required_grade_avg')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>

              </div>
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <button type="reset" class="btn btn-danger pull-right">Reset</button>
                  </form>
            </div>
          </div> 
     
            
        {{-- form End--}}

        </div>
      </div>
    </div>
  </div>
@push('scripts')
<script>
$('#password, #password-confirm').on('keyup', function () {
  if ($('#password').val() == $('#password-confirm').val()) {
    $('#message').html('Password Matched').css('color', 'green');
  } else 
    $('#message').html('Password Did Not Matched').css('color', 'red');
});
</script>
@endpush
