<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  {{-- <link rel="apple-touch-icon" sizes="76x76" href="{{asset('/mg/apple-icon.png')}}"> --}}
  {{-- <link rel="icon" type="image/png" href="{{asset('/img/favicon.png')}}"> --}}
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>
    Admission Network
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <link href="{{asset('/backend/css/toastr.min.css')}}" rel="stylesheet" />
  <link href="{{asset('/backend/css/select2.min.css')}}" rel="stylesheet" />

  <link href="{{asset('/backend/css/resource_css_preloader.css')}}" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="{{asset('/backend/css/material-kit.css?v=2.0.7')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('/backend/css/custom.css')}}" rel="stylesheet" /> 

  
</head>
<body >

    <div class="card card-login" style="background-image: url({{asset('backend/img/iStock-52037437.jpg')}});
    background-size: cover; background-position: top center;">
        <form id="testing1" action="{{ route('recruiters-form.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div style="margin-left: 0px; margin-right: 0px" class="card-header card-header-info text-center">
                <h3 class="card-title">Recruiter Register</h3>
            </div>

   
    <div id="svg_wrap"></div>
    
    <section id="s1" style="background-color:white;">
        
        <h2> Company Information </h2>
 
        <div class="row">
           
            <div class="col-md-6">
                <div class="input-group">
                    <input id="company_name" type="text" placeholder="Enter Company Name"
                        class="form-control @error('company_name')
                         is-invalid @enderror" name="company_name"
                        value="{{ old('company_name') }}" required autocomplete="company_name">

                    @error('company_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div class="input-group">
                    <input id="email" type="email" placeholder="Enter Alternative Email Address"
                        class="form-control @error('email') 
                        is-invalid @enderror" name="email"
                        value="{{ old('email') }}" requir autocomplete="email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <div class="input-group">
                    <input id="website" type="text" placeholder="Enter Website Name"
                        class="form-control @error('website') 
                        is-invalid @enderror" name="website"
                        value="{{ old('website') }}" requir autocomplete="website">

                    @error('website')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>


            <div class="col-md-6">
                <div class="input-group">
                    <input id="facebook_page_name" type="text" placeholder="Enter Facebook Page Name"
                        class="form-control @error('facebook_page_name')
                         is-invalid @enderror" name="facebook_page_name"
                        value="{{ old('facebook_page_name') }}" requir autocomplete="facebook_page_name">

                    @error('facebook_page_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <div class="input-group">
                    <input id="instagram_handle" type="text" placeholder="Enter Instragram Page Name"
                        class="form-control @error('instagram_handle')
                         is-invalid @enderror" name="instagram_handle"
                        value="{{ old('instagram_handle') }}" requir autocomplete="instagram_handle">

                    @error('instagram_handle')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div class="input-group">
                    <input id="twitter_handle" type="text" placeholder="Enter Twitter Page Name"
                        class="form-control @error('twitter_handle')
                         is-invalid @enderror" name="twitter_handle"
                        value="{{ old('twitter_handle') }}" requir autocomplete="facebook_page_name">

                    @error('twitter_handle')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

        </div>


        <div class="row">

            <div class="col-md-6">
                <div class="input-group">
                    <input id="linkdin_url" type="text" placeholder="Enter Linkdin Page URL"
                        class="form-control @error('linkdin_url') is-invalid @enderror" name="linkdin_url"
                        value="{{ old('linkdin_url') }}" requir autocomplete="linkdin_url">

                    @error('linkdin_url')
                        <span class="invalid-feedback" role="linkdin_url">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div class="input-group">
                    <input id="password" type="password" placeholder="Enter Password"
                        class="form-control @error('linkdin_url') is-invalid @enderror" name="password"
                        value="{{ old('password') }}" requir autocomplete="password">

                    @error('password')
                        <span class="invalid-feedback" role="password">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

          </div>
            
    </section>

    <section style="background-color:white;">
        
        <h2>Contact Info </h2>
            <div class="row">

            <div class="col-md-6">
                <div class="input-group">
                    <select class="role form-control @error('main_source_of_student') is-invalid @enderror" 
                        name="main_source_of_student" requir>
                            <option value="0">Select main source of students</option>
                            <option value="1">America</option>
                            <option value="2">USA</option>
                        </select>
                        @error('main_source_of_student')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div class="input-group">
                    <input id="legal_first_name" type="text" placeholder="Enter Legal First Name"
                        class="form-control @error('legal_first_name')
                         is-invalid @enderror" name="legal_first_name"
                        value="{{ old('legal_first_name') }}" requir autocomplete="legal_first_name">

                    @error('legal_first_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-md-6">
                <div class="input-group">
                    <input id="legal_last_name" type="text" placeholder="Enter Legal Last Name"
                        class="form-control @error('legal_last_name')
                         is-invalid @enderror" name="instagram_handle"
                        value="{{ old('legal_last_name') }}" requir autocomplete="legal_last_name">

                    @error('legal_last_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div class="input-group">
                    <input id="street_address" type="text" placeholder="Enter Twitter Page Name"
                        class="form-control @error('street_address')
                         is-invalid @enderror" name="street_address"
                        value="{{ old('street_address') }}" requir autocomplete="street_address">

                    @error('street_address')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-md-6">
                <div class="input-group">
                    <input id="city" type="text" placeholder="Enter City Name"
                        class="form-control @error('city')
                         is-invalid @enderror" name="city"
                        value="{{ old('city') }}" requir autocomplete="city">

                    @error('city')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div class="input-group">
                    <input id="state" type="text" placeholder="Enter State Name"
                        class="form-control @error('state')
                         is-invalid @enderror" name="state"
                        value="{{ old('state') }}" requir autocomplete="state">

                    @error('state')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

        </div>



        <div class="row">
           
            <div class="col-md-6">
                <div class="input-group">
                    <select class="role form-control @error('country') is-invalid @enderror" 
                        name="country" requir>
                            <option value="0">Select Country Name</option>
                            <option value="1">America</option>
                            <option value="2">USA</option>
                        </select>
                        @error('country')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div class="input-group">
                    <input id="postal_code" type="text" placeholder="Enter Postal Code"
                        class="form-control @error('postal_code') 
                        is-invalid @enderror" name="postal_code"
                        value="{{ old('postal_code') }}" requir autocomplete="postal_code">

                    @error('postal_code')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <div class="input-group">
                    <input id="phone" type="text" placeholder="Enter Phone Number"
                        class="form-control @error('phone')
                         is-invalid @enderror" name="Phone"
                        value="{{ old('phone') }}" requir autocomplete="phone">

                    @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div class="input-group">
                    <input id="cellphone" type="text" placeholder="Enter Cellphone Name"
                        class="form-control @error('cellphone')
                         is-invalid @enderror" name="cellphone"
                        value="{{ old('cellphone') }}" requir autocomplete="cellphone">

                    @error('cellphone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

        </div>


         <div class="row">

            <div class="col-md-6">
                <div class="input-group">
                    <input id="skype_id" type="text" placeholder="Enter Skype Id"
                        class="form-control @error('skype_id')
                         is-invalid @enderror" name="skype_id"
                        value="{{ old('skype_id') }}" requir autocomplete="skype_id">

                    @error('skype_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div class="input-group">
                    <input id="whatsapp_id" type="text" placeholder="Enter Whatsapp Id"
                        class="form-control @error('whatsapp_id')
                         is-invalid @enderror" name="whatsapp_id"
                        value="{{ old('whatsapp_id') }}" requir autocomplete=" whatsapp_id">

                    @error('whatsapp_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                            <div class="input-group" >
                                <label class="labeltext" >
                                Has anyone from Admission network helped or referred you? </label>
                            </div>
                    </div>

            <div class="col-md-12">
                <div class="input-group">
                    <div class="col-md-12">   
                       <textarea class="form-control" name="referred_info" rows="2"></textarea>
                    </div> 
                </div>
            </div>

        </div>

    </section>

    <section style="background-color:white;">
        <h2>Recruitment Details</h2>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group" >
                                <label class="labeltext">
                                When did you begin recruiting students?  </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <input id="begin_recruiting_student" type="text" 
                                placeholder="Mention the time"
                                    class="form-control @error('begin_recruiting_student')
                                    is-invalid @enderror" name="begin_recruiting_student"
                                    value="{{ old('begin_recruiting_student') }}" requir autocomplete=" whatsapp_id">

                                @error('begin_recruiting_student')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div> 

                     <div class="row">
                        <div class="col-md-6">
                            <div class="input-group" >
                                <label class="labeltext">
                                What services do you provide to your clients?  </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                          <div class="input-group">
                                 <select class="role form-control @error('servie_to_clint') is-invalid @enderror" 
                                    name="servie_to_clint" requir>
                                        <option></option>
                                        <option value="American Schools Represented">American Schools Represented</option>
                                        <option value="Canadian Schools Represented">Canadian Schools Represented</option>
                                        <option value="Represents Other Countries">Represents Other Countries</option>
                                        <option value=" United Kindom's Intitution represents"> United Kindom's Intitution represents</option>
                                       
                                    </select>
                                    @error('servie_to_clint')
                                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                           </div>
                        </div>
                    </div>       
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group" >
                                <label class="labeltext">
                                What institutions are you representing?  </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <input id="inistitutions_representing" type="text" 
                                placeholder="Inistitute Name"
                                    class="form-control @error('inistitutions_representing')
                                    is-invalid @enderror" name="inistitutions_representing"
                                    value="{{ old('inistitutions_representing') }}" requir autocomplete=" whatsapp_id">

                                @error('inistitutions_representing')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div> 

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group" >
                                <label class="labeltext">
                                What educational associations or groups
                                 does your organization belong to?  </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <input id="educational_associations" type="text" 
                                placeholder="Inistitute Name"
                                    class="form-control @error('educational_associations')
                                    is-invalid @enderror" name="educational_associations"
                                    value="{{ old('educational_associations') }}" requir autocomplete=" whatsapp_id">

                                @error('educational_associations')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div> 

                     <div class="row">
                        <div class="col-md-6">
                            <div class="input-group" >
                                <label class="labeltext">
                                Where do you recruit from?  </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                        <div class="input-group">
                                 <select class="role form-control @error('recruit_from') is-invalid @enderror" 
                                    name="recruit_from" requir>
                                        <option value="0">America</option>
                                        <option value="1">Canada</option>
                                        <option value="2">Bangladesh</option>
                                    </select>
                                    @error('recruit_from')
                                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                        </div>
                    </div>    

                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group" >
                                <label class="labeltext">
                                Approximately how many students do 
                                you send abroad per year? </label>
                            </div>
                        </div>
                        <div class="col-md-2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="student_count_per_year" value="0">
                                        0-5
                                    </label>
                                </div>
                        </div>
                        <div class="col-md-2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="student_count_per_year" value="1">
                                        6-20
                                    </label>
                                </div>
                        </div>
                        <div class="col-md-2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="student_count_per_year" value="2">
                                        21-50
                                    </label>
                                </div>
                        </div>
                        <div class="col-md-2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="student_count_per_year" value="3">
                                        51-250
                                    </label>
                                </div>
                        </div>
                        <div class="col-md-2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="student_count_per_year" value="4">
                                        250-400
                                    </label>
                                </div>
                        </div>
                        <div class="col-md-2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="student_count_per_year" value="5">
                                        400+
                                    </label>
                                </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group" >
                                <label class="labeltext">
                                Where do you recruit from?  </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                        <div class="input-group">
                                 <select class="role form-control 
                                 @error('marketing_method') is-invalid @enderror" 
                                    name="marketing_method" requir>
                                        <option value="0">Online Ads</option>
                                        <option value="1">Education Fair</option>
                                        <option value="2">Workshops</option>
                                        <option value="3">Sub-Agent Networks</option>
                                        <option value="4">NewsPaper and Magazines</option>
                                        <option value="5">Others</option>
                                    </select>
                                    @error('marketing_method')
                                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                        </div>
                    </div>  

                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group" >
                                <label class="labeltext">
                                Please provide an estimate of the number of 
                                students you will refer to Network Acadamy?. </label>
                            </div>
                        </div>
                        <div class="col-md-2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="estimated_number" value="0">
                                        1-5
                                    </label>
                                </div>
                        </div>
                        <div class="col-md-2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="estimated_number" value="1">
                                        6-20
                                    </label>
                                </div>
                        </div>
                        <div class="col-md-2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="estimated_number" value="2">
                                        21-50
                                    </label>
                                </div>
                        </div>
                        <div class="col-md-2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="estimated_number" value="3">
                                        51-250
                                    </label>
                                </div>
                        </div>
                        <div class="col-md-2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="estimated_number" value="4">
                                        250-400
                                    </label>
                                </div>
                        </div>
                        <div class="col-md-2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="estimated_number" value="5">
                                        400+
                                    </label>
                                </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group" >
                                <label class="labeltext">
                                Average Service Fee :  </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                        <div class="input-group">
                                 <select class="role form-control 
                                 @error('average_service_free') is-invalid @enderror" 
                                    name="average_service_free" requir>
                                        <option value="0">0-200</option>
                                        <option value="1">200-500</option>
                                        <option value="2">500-1000</option>
                                        <option value="3">1000-2500</option>
                                        <option value="4">2500+</option>
                                        
                                    </select>
                                    @error('average_service_free')
                                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="col-md-12">
                                        <div class="input-group" >
                                            <label class="labeltext" >
                                            Additional Comments : </label>
                                        </div>
                                </div>

                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="col-md-12">   
                                <textarea class="form-control" 
                                name="additional_comments" rows="2"></textarea>
                                </div> 
                            </div>
                        </div>

                    </div>   
        
    </section>

    <section style="background-color:white;">

        <h2>Reference Details</h2>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group" >
                                <label class="labeltext">
                                Reference Name :  </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <input id="reference_name" type="text" 
                                placeholder="Reference Name"
                                    class="form-control @error('reference_name')
                                    is-invalid @enderror" name="reference_name"
                                    value="{{ old('reference_name') }}" requir autocomplete=" whatsapp_id">

                                @error('reference_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div> 

                    <div class="row">
                        <div class="col-md-6">
                                <div class="input-group" >
                                    <label class="labeltext">
                                    Reference Inistitution Name :  </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                    <input id="reference_inistitution_name" type="text" 
                                    placeholder="Inistitution Name"
                                        class="form-control @error('reference_inistitution_name')
                                        is-invalid @enderror" name="reference_name"
                                        value="{{ old('reference_name') }}" requir autocomplete=" whatsapp_id">

                                    @error('reference_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                        </div> 
                    
                    <div class="row">
                    <div class="col-md-6">
                            <div class="input-group" >
                                <label class="labeltext">
                                Reference Business Email :  </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <input id="reference_business_email" type="email" 
                                placeholder="Reference Business Name"
                                    class="form-control @error('reference_name')
                                    is-invalid @enderror" name="reference_name"
                                    value="{{ old('reference_name') }}" requir autocomplete=" whatsapp_id">

                                @error('reference_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div> 
                    

                     <div class="row">
                    <div class="col-md-6">
                            <div class="input-group" >
                                <label class="labeltext">
                                Reference Phone Name :  </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <input id="reference_phone" type="text" 
                                placeholder="Reference phone Name"
                                    class="form-control @error('reference_phone')
                                    is-invalid @enderror" name="reference_phone"
                                    value="{{ old('reference_phone') }}" requir autocomplete=" whatsapp_id">

                                @error('reference_phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div> 
                    

                     <div class="row">
                    <div class="col-md-6">
                            <div class="input-group" >
                                <label class="labeltext">
                                Reference Website :  </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <input id="reference_website" type="text" 
                                placeholder="Reference Website Name"
                                    class="form-control @error('reference_website')
                                    is-invalid @enderror" name="reference_website"
                                    value="{{ old('reference_website') }}" requir autocomplete=" whatsapp_id">

                                @error('reference_website')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div> 
                   
                    <h4>Upload Files</h4>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group" >
                                <label class="labeltext">
                                Company Logo :  </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            
                            <input class="form-control 
                                @error('proof_of_address') is-invalid @enderror" 
                               
                                type="file" name="proof_of_address" requir>
                                 @error('proof_of_address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>
                    </div>     

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group" >
                                <label class="labeltext">
                                Business Certificate :  </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            
                            <input class="form-control 
                                @error('business_certificate') is-invalid @enderror" 
                               
                                type="file" name="business_certificate" requir>
                                 @error('business_certificatex')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>
                    </div>     
                    
    </section>

      <section style="background-color:white;">
        <p>General condtitions</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
            commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
            nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
            anim id est laborum.</p>
    </section>
   
    <button  class="button" id="prev" onClick="reply_click()">&larr; Previous</button>
    <button  class="button" id="next" onClick="reply_click()" >Next &rarr;</button>
    <button type="submit" class="button" id="submit" onClick="reply_click1()">Agree and Complete Registration</button>
    </form>

 </div>

    <script>
        function reply_click()
            {
                if($('#testing1')[0].checkValidity()){
                    event.preventDefault();
                }// returns true/false
                
            }
            function reply_click1()
            {
                if(!$('#testing1')[0].checkValidity()){
                    
                    toastr.error("Please Fill Up All The Field Properly")
                    event.preventDefault();
                }// returns true/false
                
            }


        $(document).ready(function() {
            var base_color = "rgb(230,230,230)";
            var active_color = "rgb(237, 40, 70)";



            var child = 1;
            var length = $("section").length - 1;
            $("#prev").addClass("disabled");
            $("#submit").addClass("disabled");

            $("section").not("section:nth-of-type(1)").hide();
            $("section").not("section:nth-of-type(1)").css('transform', 'translateX(100px)');

            var svgWidth = length * 200 + 24;
            $("#svg_wrap").html(
                '<svg version="1.1" id="svg_form_time" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 ' +
                svgWidth +
                ' 24" xml:space="preserve"></svg>'
            );

            function makeSVG(tag, attrs) {
                var el = document.createElementNS("http://www.w3.org/2000/svg", tag);
                for (var k in attrs) el.setAttribute(k, attrs[k]);
                return el;
            }

            for (i = 0; i < length; i++) {
                var positionX = 12 + i * 200;
                var rect = makeSVG("rect", {
                    x: positionX,
                    y: 9,
                    width: 200,
                    height: 6
                });
                document.getElementById("svg_form_time").appendChild(rect);
                // <g><rect x="12" y="9" width="200" height="6"></rect></g>'
                var circle = makeSVG("circle", {
                    cx: positionX,
                    cy: 12,
                    r: 12,
                    width: positionX,
                    height: 6
                });
                document.getElementById("svg_form_time").appendChild(circle);
            }

            var circle = makeSVG("circle", {
                cx: positionX + 200,
                cy: 12,
                r: 12,
                width: positionX,
                height: 6
            });
            document.getElementById("svg_form_time").appendChild(circle);

            $('#svg_form_time rect').css('fill', base_color);
            $('#svg_form_time circle').css('fill', base_color);
            $("circle:nth-of-type(1)").css("fill", active_color);


            $(".button").click(function() {
                $("#svg_form_time rect").css("fill", active_color);
                $("#svg_form_time circle").css("fill", active_color);
                var id = $(this).attr("id");
                if (id == "next") {
                    $("#prev").removeClass("disabled");
                    if (child >= length) {
                        $(this).addClass("disabled");
                        $('#submit').removeClass("disabled");
                    }
                    if (child <= length) {
                        child++;
                    }
                } else if (id == "prev") {
                    $("#next").removeClass("disabled");
                    $('#submit').addClass("disabled");
                    if (child <= 2) {
                        $(this).addClass("disabled");
                    }
                    if (child > 1) {
                        child--;
                    }
                }
                var circle_child = child + 1;
                $("#svg_form_time rect:nth-of-type(n + " + child + ")").css(
                    "fill",
                    base_color
                );
                $("#svg_form_time circle:nth-of-type(n + " + circle_child + ")").css(
                    "fill",
                    base_color
                );
                var currentSection = $("section:nth-of-type(" + child + ")");
                currentSection.fadeIn();
                currentSection.css('transform', 'translateX(0)');
                currentSection.prevAll('section').css('transform', 'translateX(-100px)');
                currentSection.nextAll('section').css('transform', 'translateX(100px)');
                $('section').not(currentSection).hide();

                
            });

        });

    </script>
    <script src="{{asset('backend/js/core/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/js/core/popper.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/js/plugins/moment.min.js')}}"></script>
    <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
    <script src="{{asset('backend/js/plugins/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{asset('backend/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/js/toastr.min.js')}}" type="text/javascript"></script>
    <!--  Preloader    -->
    <script src="{{asset('backend/js/resource_js_preloader.js')}}" type="text/javascript"></script>
    <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
    <script src="{{asset('backend/js/material-kit.js?v=2.0.7')}}" type="text/javascript"></script>
    <script src="{{asset('backend/js/select2.min.js')}}" type="text/javascript"></script>
    <script>
        $('.role').select2({
          placeholder: "Select an option",
          allowClear: true
        });
    </script>
    @if (Session::has('success'))
      <script>
          toastr.success("{!!Session::get('success')!!}")
      </script>
      @elseif (Session::has('error'))
      <script>
          toastr.error("{!!Session::get('error')!!}")
      </script>
      @endif
      </body>

      </html>