@extends('layouts.backend.layouts.app1')    

@section('content')
@include('layouts.frontend.forms.create1-modal')
<div class="card card-login" style="background-image: url({{asset('backend/img/iStock-52037437.jpg')}});
    background-size: cover; background-position: top center; ">
<section id="s1" style="background-color:white;">

<h2 style="color: black"> Institution Info  </h2>

<form action="{{route('school-form.update',1)}}" method="POST">
    @csrf
    {{ method_field('PUT') }}
<div class="continer">

        <div class="card-body table-responsive" >
            <button type="button" class="btn btn-info btn-sm pull-right" 
              data-toggle="modal" data-target="#exampleModalLongTitle" >
              Add Campus</button>
              <div class="clearfix"></div>
        </div>
        <div class="card">
            <div class="card-header card-header-info card-header-icon">
              
              <h4 class="card-title"><b>List of Campus</b></h4>
            </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="card-header-info">
                  <tr style="text-align: center" >
                    <th scope="col">SL</th>
                    <th scope="col" >Address</th>
                    <th scope="col" >Zip</th>
                    <th scope="col" >City</th>
                    <th scope="col" >State</th>
                    <th scope="col"  >Country</th>
                    <th scope="col"  >Action</th>
                  </tr>
                </thead>
                <tbody>
                    @php
                    $i = 1
                    @endphp
             @foreach ($campuses as $campus) 
                
                    <tr style="text-align: center" >
                        <td>{{$i++}}</td>
                        <td>{{$campus->address}}</td>
                        <td>{{$campus->zip}}</td>
                        <td>{{$campus->city}}</td>                        
                        <td>{{$campus->state}}</td>
                        <td>{{$campus->country}}</td>
                        <td class="td-actions">1
                            {{-- <a class="btn btn-primary btn-link" href="{{ route('admin-client.edit', $row->id) }}">
                                <i class="material-icons">edit</i>
                            </a>

                            <form class="form-delete" action="{{ route('admin-client.destroy', $row->id) }}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" title="Delete" class="delete btn btn-danger btn-link">
                                    <i class="material-icons">close</i>
                                  </button>
                            </form> --}}


                          </td>
                    </tr>
            @endforeach
                  </tr>
                </tbody>
              </table>
            </div>
        </div>


            

            <div class="card-body table-responsive" >
                <button type="button" class="btn btn-warning btn-sm pull-right" 
                  data-toggle="modal" data-target="#exampleModalLongTitle1" >
                  Add Program</button>
                  <div class="clearfix"></div>
            </div>  
            <h4><b>List of Courses</b></h4>
            {{-- <div class="table-responsive">
            <table class="table table-bordered table-hover" data-form="deleteForm">
                <thead class="card-header-primary" style="background-color: slateblue; color:white;">
                  <tr style="text-align: center">
                    <th scope="col">SL</th>
                    <th scope="col">Course Name</th>
                    <th scope="col">Programe Level</th>
                    <th scope="col">Intakes</th>
                    <th scope="col">Course Duration</th>
                    <th scope="col">Seasons</th>
                    <th scope="col">Tution Free</th>
                    <th scope="col">Admission Free</th>
                    <th scope="col">Language Profeciency(English)</th>
                    <th scope="col">Required English Exam Type</th>
                    <th scope="col">Visa Type Required </th>
                    <th scope="col">Required Grading Scheme</th>
                    <th scope="col">Required Grade Scale*(out of)</th>
                    <th scope="col">Required Grade Average</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($programs as $program) 
                    <tr style="text-align: center" >
                        <td>{{$i++}}</td>
                        <td>{{$program->course_name}}</td>
                        <td>{{$program->programe_level}}</td>
                        <td>{{$program->intakes}}</td>
                        <td>{{$program->course_duration}}</td>                       
                        <td>{{$program->seasons}}</td>
                        <td>{{$program->tution_free}}</td>
                        <td>{{$program->admission_free}}</td>
                        <td>{{$program->language_profeciency}}</td>
                        <td>{{$program->required_exam_type}}</td>
                        <td>{{$program->required_visa_type}}</td>
                        <td>{{$program->required_grading_scheme}}</td>
                        <td>{{$program->required_grade_scale}}</td>
                        <td>{{$program->required_grade_avg}}</td>
                      
                    </tr>
               @endforeach
                </tbody>
              </table>
            </div> --}}


            <div class="container-fluid content-row">
                <div class="row">
                    <div class="col-sm-12">
                        @foreach ($programs as $program) 
                        <div class="card card-nav-tabs">
                            <div class="card-header card-header-warning">
                                <span class="material-icons">
                                    school<h4>Course: {{$program->course_name}} </h4>
                                    </span>
                                    
                            </div>
                            <div class="card-body">
                              
                              <div class="table-responsive">
                                <table class="table" style="table-layout: fixed">
                                  
                                  <tbody>
                                    <tr>
                                        
                                      </tr>
                                    <tr>
                                      <th>Programe Level</th>
                                      <td>{{$program->programe_level}}</td>
                                    
                                        <th>Intakes</th>
                                        <td>{{$program->intakes}}</td>
                                      </tr>
                                      <tr>
                                        <th>Course Duration</th>
                                        <td>{{$program->course_duration}}</td>
                                     
                                        <th>Tution Free</th>
                                        <td>{{$program->tution_free}}</td>
                                      </tr>
                                      <tr>
                                        <th>Admission Free</th>
                                        <td>{{$program->admission_free}}</td>
                                        <th>Language Profeciency(English)</th>
                                        <td>{{$program->language_profeciency}}</td>
                                      </tr>
                                    <tr>

                                        <td colspan="4" class="td-actions">
                                            <button type="button" rel="tooltip" class="btn btn-danger" data-original-title="" title="">
                                              <i class="material-icons">close</i>
                                            </button>
                                        </td>
                                      </tr>
                                    
                                  </tbody>
                                </table>
                              </div>
                          
                            </div>
                          </div>
                          @endforeach
                    </div> 
                </div>
            </div>

            <div class="row">
       
        <div class="col-md-6">
            <div class="input-group" style="margin-top:15px;">
                <select class="role form-control @error('institution_type') is-invalid @enderror"
                name="institution_type" required>
                   <option>Select Institution</option>
                   {{-- <option value="4">Customer</option> --}}
                   <option value="1">University</option>
                   <option value="2">High School</option>
                   <option value="3">College</option>
                   <option value="4">English Istitute</option>
               </select>
               @error('institution_type')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
               @enderror
            </div>
        </div>

        <div class="col-md-6">
            <div class="input-group">
                <input id="no_of_campus" type="no_of_campus" placeholder="Number Of Campus"
                    class="form-control @error('no_of_campus') 
                    is-invalid @enderror" name="no_of_campus"
                    value="{{ old('no_of_campus') }}" requir autocomplete="no_of_campus">

                @error('no_of_campus')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>

    <div class="row" style="margin-top:15px;">

        <div class="col-md-6">
            <div class="input-group">
                <label class="from-control header 6" 
                    style="margin-top:15px; color:black;font-size:16px;font-wieght:18px">
                    Upload Logo of the Institution : </label>
            </div>
        </div>
        <div class="col-md-6">

            <input class="form-control 
                    @error('logo_name') is-invalid @enderror" type="file" name="logo_name"
                requir>
            @error('logo_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

        </div>

    </div>
    

    <div class="row">

        <div class="col-md-6">
            <div class="input-group">
                <label class="from-control header 6"
                        style="margin-top:15px; color:black;font-size:16px;font-wieght:18px">
                    Upload Business Certificate : </label>
            </div>
        </div>
        <div class="col-md-6">

            <input class="form-control 
                    @error('certificate') is-invalid @enderror" type="file" name="certificate"
                requir>
            @error('certificate')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

    </div>


            

      </div>

      <button type="submit" class="btn btn-info" > Completec Registration</button>
    </form>
</section>
</div>

@endsection