<form action="{{route('storeSchool')}}" method="POST">
    @csrf
    <div class="row">
        <div style="padding: 15px" class="col-md-12">
            <div class="form-group">
                <label class="required" for="country">Destination Country</label>
                {{ Form::select('country_code', countries() ? ['' => '-- Select --']+countries() : [], null, ['class' => 'form-control', 'id' => 'country']) }}
            </div>
        </div>
    </div>

    <div class="row">
        <div style="padding: 15px" class="col-md-12">
            <div class="form-group">
                <label class="required" for="school">School Name</label>
                <input id="school_name" type="text"
                class="form-control @error('school_name') is-invalid @enderror"
                name="school_name" value="{{ old('school_name') }}"
                required autocomplete="school_name" autofocus>

                            @error('school_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
            </div>
        </div>
    </div>

    <div class="row">
        <div style="padding: 15px" class="col-md-6">
            <div class="form-group">
                <label class="required" for="first_name">Contact First Name</label>
                <input type="text" class="form-control" id="first_name"
                        name=contact_first_name>
            </div>
        </div>

        <div style="padding: 15px" class="col-md-6">
            <div class="form-group">
                <label class="required" for="last_name">Contact Last Name</label>
                <input type="text" class="form-control" id="last_name"
                 name=contact_last_name>
            </div>
        </div>
    </div>

    <div class="row">
        <div style="padding: 15px" class="col-md-6">
            <div class="form-group">
                <label class="required" for="email">Contact Email</label>
                <input id="contact_email" type="text"
                class="form-control @error('contact_email') is-invalid @enderror"
                name="contact_email" value="{{ old('contact_email') }}"
                required autocomplete="contact_email" autofocus>

                            @error('contact_email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
            </div>
        </div>

        <div style="padding: 15px" class="col-md-6">
            <div class="form-group">
                <label for="number">Phone Number</label>
                <input id="contact_number" type="text"
                class="form-control @error('contact_number') is-invalid @enderror"
                name="contact_number" value="{{ old('contact_number') }}"
                required autocomplete="contact_number" autofocus>

                            @error('contact_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
            </div>
        </div>
    </div>

    <div class="row">
        <div style="padding: 15px" class="col-md-12">
            <div class="form-group">
                <label class="required" for="c_title">Contact Title</label>
                <input type="text" class="form-control" id="c_title" name="contact_title">
            </div>
        </div>
    </div>

    <div class="row">
        <div style="padding: 15px" class="col-md-12">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" onclick="myFunction()" >
                <label class="form-check-label" for="defaultCheck1">
                    Check if you have been referred by someone in Admission Network
                </label>
            </div>
        </div>
    </div>

    <div id="myDIV" style="display:none">
        <div class="row">
            <div style="padding: 15px" class="col-md-12">
                <div class="form-group">
                    <label class="required" for="r_c_title">Contact full name</label>
                    <input type="text" class="form-control" id="r_c_title" name="ref_name">
                </div>
            </div>
        </div>

        <div class="row">
            <div style="padding: 15px" class="col-md-12">
                <div class="form-group">
                    <label class="required" for="r_email">Contact Email</label>
                    <input type="email" class="form-control" id="ref_email">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div style="padding: 15px" class="col-md-6">
            <div class="form-group">

                <input id="password" type="password"
                placeholder="Enter Password"
                class="form-control @error('password') is-invalid @enderror"
                name="password" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div style="padding: 15px" class="col-md-6">
            <div class="form-group">
                <div class="input-group-prepend">

                <input id="password-confirm" type="password"
                placeholder="Confirm Password"  class="form-control @error('password-confirm') is-invalid @enderror"
                name="password-confirm" required autocomplete="new-password">

                @error('password-confirm')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <span id='message'></span>
            </div>
        </div>
        </div>

    <div class="row">
        <div style="padding: 15px" class="col-md-12">
            <div class="form-group">
                <label for="textarea">Any additional comments:</label>
                <textarea class="form-control" id="textarea" rows="3" name="comment"></textarea>
              </div>
        </div>
    </div>

    <p>
        Admission Network is committed to protecting and respecting your privacy, and we’ll only use your personal information to administer your account and to provide the products and services you requested from us. From time to time, we would like to contact you about our products and services, as well as other content that may be of interest to you. If you consent to us contacting you for this purpose, please tick below to say how you would like us to contact you:
    </p>
    <div class="row">
        <div style="padding: 15px" class="col-md-12">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="agree" name="agree_communication">
                <label class="form-check-label" for="agree">
                    I agree to receive other communications from Admission Network.
                </label>
            </div>
        </div>
    </div>


    <p>
        You can unsubscribe from these communications at any time. For more information on how to unsubscribe, our privacy practices, and how we are committed to protecting and respecting your privacy, please review our Privacy Policy.
    </p>
    <p>
        By clicking submit below, you consent to allow Admission Network to store and process the personal information submitted above to provide you the content requested.
    </p>

    @if(auth()->user()== null)
        <button style="color: #808080; background-color: #fff; border-color: #fff" type="submit" class="btn btn-success btn-lg" id="testing1" >Submit</button>
    @else
        <button style="color: #808080; background-color: #fff; border-color: #fff" type="submit" class="btn btn-success btn-lg" id="testing1" disabled>Can not register institute while loged in</button>
@endif
</form>


