<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    {{--
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/mg/apple-icon.png') }}">
    --}}
    {{--
    <link rel="icon" type="image/png" href="{{ asset('/img/favicon.png') }}"> --}}
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        Admission Network
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link href="{{ asset('/backend/css/toastr.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('/backend/css/select2.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('/backend/css/resource_css_preloader.css') }}" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="{{ asset('/backend/css/material-kit.css?v=2.0.7') }}" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{ asset('/backend/css/custom.css') }}" rel="stylesheet" />


</head>

<body>

    <div class="card card-login" style="background-image: url({{ asset('backend/img/iStock-52037437.jpg') }});
    background-size: cover; background-position: top center;">
        <form id="testing1" action="{{ route('students-form.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div style="margin-left: 0px; margin-right: 0px" class="card-header card-header-info text-center">
                <h3 class="card-title">Student Register</h3>
            </div>


            <div id="svg_wrap"></div>
           
            <section id="s1" style="background-color:white;">

                <h2> Basic Information </h2>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group" style="margin-top:40px">
                            <select class="title form-control
                     @error('title') is-invalid @enderror" name="title" required>
                                <option></option>
                                <option value="Miss.">Miss.</option>
                                <option value="Mrs">Mrs.</option>
                                <option value="Mr">Mr.</option>
                            </select>
                            @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="input-group">
                            <input id="first_name" type="text" placeholder="Enter First Name"
                                class="form-control @error('first_name') is-invalid @enderror" name="first_name"
                                value="{{ old('first_name') }}" requir autocomplete="first_name">

                            @error('first_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-6">
                        <div class="input-group">
                            <input id="middle_name" type="text" placeholder="Enter Middle Name"
                                class="form-control @error('middle_name') is-invalid @enderror" name="middle_name"
                                value="{{ old('middle_name') }}" requir autocomplete="middle_name">

                            @error('middle_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="input-group">
                            <input id="last_name" type="text" placeholder="Enter Last Name"
                                class="form-control @error('last_name') is-invalid @enderror" name="last_name"
                                value="{{ old('last_name') }}" requir autocomplete="last_name">

                            @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">




                    <div class="col-md-6">
                        <div class="input-group">
                            <input id="date_of_birth" type="date" placeholder="Enter Date Of Birth"
                                class="form-control @error('date_of_birth') is-invalid @enderror" name="date_of_birth"
                                value="{{ old('date_of_birth') }}" requir autocomplete="date_of_birth">

                            @error('date_of_birth')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>





                    <div class="col-md-6">
                        <div class="input-group">
                            <input id="mobile" type="number" placeholder="Enter Mobile Number"
                                class="form-control @error('mobile') is-invalid @enderror" name="mobile"
                                value="{{ old('mobile') }}" requir autocomplete="mobile">

                            @error('mobile')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-md-6">
                        <div class="input-group">
                            <input id="emergency_contact" type="number" placeholder="Enter Emergency Contact Number"
                                class="form-control @error('emergency_contact') is-invalid @enderror"
                                name="emergency_contact" value="{{ old('emergency_contact') }}" requir
                                autocomplete="emergency_contact">

                            @error('emergency_contact')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="input-group">
                            <input id="address" type="text" placeholder="Enter Current Address"
                                class="form-control @error('address') is-invalid @enderror" name="address"
                                value="{{ old('address') }}" requir autocomplete="address">

                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-6">
                        <div class="input-group">
                            <input id="house_flat_no" type="text" placeholder="Enter House/Flat Number"
                                class="form-control @error('house_flat_no') is-invalid @enderror" name="house_flat_no"
                                value="{{ old('house_flat_no') }}" requir autocomplete="house_flat_no">

                            @error('house_flat_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="input-group">
                            <input id="street" type="text" placeholder="Enter Street Name"
                                class="form-control @error('street') is-invalid @enderror" name="street"
                                value="{{ old('street') }}" requir autocomplete="street">

                            @error('street')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-md-6">
                        <div class="input-group">
                            <input id="postal_code" type="text" placeholder="Enter Postal Code"
                                class="form-control @error('house_flat_no') is-invalid @enderror" name="house_flat_no"
                                value="{{ old('postal_code') }}" requir autocomplete="postal_code">

                            @error('postal_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="input-group">
                            <input id="city" type="text" placeholder="Enter City Name"
                                class="form-control @error('street') is-invalid @enderror" name="city"
                                value="{{ old('city') }}" requir autocomplete="street">

                            @error('street')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group" style="margin-top:40px">

                            <select class="residence form-control @error('country_of_residence') is-invalid @enderror"
                                name="country_of_residence" requir>
                                <option></option>
                                <option value="1">America</option>
                                <option value="2">USA</option>
                            </select>
                            @error('country_of_residence')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="input-group" style="margin-top:40px">

                            <select class="birth form-control @error('country_of_birth') is-invalid @enderror"
                                name="country_of_birth" requir>
                                <option></option>
                                <option value="1">America</option>
                                <option value="2">USA</option>
                            </select>
                            @error('country_of_residence')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group" style="margin-top:40px">

                            <select class="nationality form-control @error('nationality') is-invalid @enderror"
                                name="nationality" requir>
                                <option></option>
                                <option value="1">America</option>
                                <option value="2">USA</option>
                            </select>
                            @error('nationality')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="input-group" style="margin-top:40px">

                            <select class="ethnicity form-control @error('ethnicity') is-invalid @enderror" name="ethnicity"
                                requir>
                                <option></option>
                                <option value="1">America</option>
                                <option value="2">USA</option>
                            </select>
                            @error('ethnicity')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>



               

                {{-- <div class="btn-block" style="align-content: center;display: flex;">
                    <button type="submit" class="btn btn-primary btn-sm">Sign in</button>
                </div> --}}

            </section>

            <section style="background-color:white;">

                <h2>Additional Information</h2>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6"
                                style="color:black;font-size:16px;font-wieght:18px">When would you like to start study?
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <input id="start_study" type="date" placeholder="Select Start Date"
                            class="form-control @error('start_study') is-invalid @enderror" name="start_study"
                            value="{{ old('start_study') }}" requir autocomplete="start_study">

                        @error('start_study')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Which course are you applying for? </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">

                            <select class="role form-control @error('applying_course') is-invalid @enderror" name="applying_course"
                                requir>
                                <option></option>
                                @foreach ($courses as $course)
                                <option value="{{$course->course_name}}">{{$course->course_name}}</option>
                                @endforeach
                            </select>
                            @error('applying_course')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Which country do you prefer? </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">

                            <select class="role form-control @error('preferred_country') is-invalid @enderror"
                                name="preferred_country" requir>
                                <option value="0">Select Nationality</option>
                                <option value="1">America</option>
                                <option value="2">USA</option>
                            </select>
                            @error('preferred_country')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control" style="color:black;font-size:16px;font-wieght:18px">
                                Which Campus location do you prefer? </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">

                            <select class="role form-control @error('preferred_campus_location') is-invalid @enderror"
                                name="preferred_campus_location" requir>
                                <option></option>
                                @foreach ($campuses as $campus)
                                    <option value="{{$campus->state}}">{{$campus->state}}</option>
                                @endforeach
                                
                                
                            </select>
                            @error('preferred_campus_location')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Have you ever applied for student VISA? </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">

                            <select class="role form-control @error('applied_student_visa_befor') is-invalid @enderror"
                                name="applied_student_visa_befor" requir>
                                <option></option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                            @error('applied_student_visa_befor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                If yes, please specify when and for what course? </label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <textarea class="form-control" name="student_visa_details" rows="3"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" style="text-decoration: underline;">
                        <b>
                            <h4>Declaration of Criminal Record</h4>
                        </b>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Change it Do you have any previous criminal records : </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">

                            <select class="role form-control @error('declaration_of_criminal_record') is-invalid @enderror"
                                name="declaration_of_criminal_record" requir>
                                <option></option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                            @error('declaration_of_criminal_record')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                If yes, Please give details.</label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <textarea class="form-control" name="criminal_record_details" rows="3"></textarea>
                    </div>
                </div>


            </section>

            <section style="background-color:white;">
                <h2>File Uplaods</h2>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Upload Profile Picture : </label>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <input class="form-control 
                                @error('prof_pic_upload') is-invalid @enderror" type="file" name="prof_pic_upload"
                            requir>
                        @error('prof_pic_upload')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Upload Passport: </label>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <input class="form-control 
                                @error('passport_nid_upload') is-invalid @enderror" type="file"
                            name="passport_nid_upload" requir>
                        @error('passport_nid_upload')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Upload CV : </label>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <input class="form-control 
                                @error('cv_upload') is-invalid @enderror" type="file" name="cv_upload" requir>
                        @error('cv_upload')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Upload Certificats of your highest Educations  : </label>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <input class="form-control 
                                @error('work_experience_upload') is-invalid @enderror" name="work_experience_upload"
                            type="file" name="highest_education_certificates_upload" requir>
                        @error('highest_education_certificates_upload')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Upload Certificats of your Bachelor Degrees: </label>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <input class="form-control 
                                @error('bachelor_degree_certificates_upload') is-invalid @enderror" type="file"
                            name="bachelor_degree_certificates_upload" requir>
                        @error('bachelor_degree_certificates_upload')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Upload Certificats of your HSC: </label>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <input class="form-control 
                                @error('hsc_certificates_upload') is-invalid @enderror" name="hsc_certificates_upload"
                            type="file" name="hsc_certificates_upload" requir>
                        @error('hsc_certificates_upload')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Upload Certificats of your SSC: </label>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <input class="form-control 
                                @error('ssc_certificates_upload') is-invalid @enderror" type="file"
                            name="ssc_certificates_upload" requir>
                        @error('ssc_certificates_upload')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Uplaod Purpose of Study Statement: </label>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <input class="form-control 
                                @error('purpose_of_study_statement') is-invalid @enderror" type="file"
                            name="purpose_of_study_statement" requir>
                        @error('purpose_of_study_statement')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>
                


                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Uplaod Proof Of English Langauge: </label>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <input class="form-control 
                                @error('proof_of_english_language') is-invalid @enderror" type="file"
                            name="proof_of_english_language" requir>
                        @error('proof_of_english_language')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Upload Proof Of Address : </label>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <input class="form-control 
                                @error('proof_of_address') is-invalid @enderror" type="file" name="proof_of_address"
                            requir>
                        @error('proof_of_address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Other Documents : </label>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <input class="form-control 
                                @error('others_document') is-invalid @enderror" type="file" name="others_document"
                            requir>
                        @error('others_document')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </section>

            <section style="background-color:white;">

                <h2>Declaration</h2>
                <div class="row">

                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Accuracy of information on this form : </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">

                            <select class="role form-control @error('accuracy_of_information') is-invalid @enderror"
                                name="accuracy_of_information" requir>
                                <option value="0">None</option>
                                <option value="1">I Confirm</option>
                                <option value="2">I Do Not Confirm</option>
                            </select>
                            @error('accuracy_of_information')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

              


                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Comments : </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <textarea class="form-control" name="criminal_record_details" rows="3"></textarea>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="from-control header 6" style="color:black;font-size:16px;font-wieght:18px">
                                Additional Information : </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <textarea class="form-control" name="criminal_record_details" rows="3"></textarea>
                </div>
            </section>

            <section style="background-color:white;">
                <p>General condtitions</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore
                    magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea
                    commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat
                    nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                    mollit
                    anim id est laborum.</p>
            </section>

            <button class="button" id="prev" onClick="reply_click()">&larr; Previous</button>
            <button class="button" id="next" onClick="reply_click()">Next &rarr;</button>
            <button type="submit" class="button" id="submit" onClick="reply_click1()">Agree and Complete
                Registration</button>
        </form>

    </div>

    <script>
        function reply_click() {
            if ($('#testing1')[0].checkValidity()) {
                event.preventDefault();
            } // returns true/false

        }

        function reply_click1() {
            if (!$('#testing1')[0].checkValidity()) {

                toastr.error("Please Fill Up All The Field Properly")
                event.preventDefault();
            } // returns true/false

        }


        $(document).ready(function() {
            var base_color = "rgb(230,230,230)";
            var active_color = "rgb(237, 40, 70)";



            var child = 1;
            var length = $("section").length - 1;
            $("#prev").addClass("disabled");
            $("#submit").addClass("disabled");

            $("section").not("section:nth-of-type(1)").hide();
            $("section").not("section:nth-of-type(1)").css('transform', 'translateX(100px)');

            var svgWidth = length * 200 + 24;
            $("#svg_wrap").html(
                '<svg version="1.1" id="svg_form_time" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 ' +
                svgWidth +
                ' 24" xml:space="preserve"></svg>'
            );

            function makeSVG(tag, attrs) {
                var el = document.createElementNS("http://www.w3.org/2000/svg", tag);
                for (var k in attrs) el.setAttribute(k, attrs[k]);
                return el;
            }

            for (i = 0; i < length; i++) {
                var positionX = 12 + i * 200;
                var rect = makeSVG("rect", {
                    x: positionX,
                    y: 9,
                    width: 200,
                    height: 6
                });
                document.getElementById("svg_form_time").appendChild(rect);
                // <g><rect x="12" y="9" width="200" height="6"></rect></g>'
                var circle = makeSVG("circle", {
                    cx: positionX,
                    cy: 12,
                    r: 12,
                    width: positionX,
                    height: 6
                });
                document.getElementById("svg_form_time").appendChild(circle);
            }

            var circle = makeSVG("circle", {
                cx: positionX + 200,
                cy: 12,
                r: 12,
                width: positionX,
                height: 6
            });
            document.getElementById("svg_form_time").appendChild(circle);

            $('#svg_form_time rect').css('fill', base_color);
            $('#svg_form_time circle').css('fill', base_color);
            $("circle:nth-of-type(1)").css("fill", active_color);


            $(".button").click(function() {
                $("#svg_form_time rect").css("fill", active_color);
                $("#svg_form_time circle").css("fill", active_color);
                var id = $(this).attr("id");
                if (id == "next") {
                    $("#prev").removeClass("disabled");
                    if (child >= length) {
                        $(this).addClass("disabled");
                        $('#submit').removeClass("disabled");
                    }
                    if (child <= length) {
                        child++;
                    }
                } else if (id == "prev") {
                    $("#next").removeClass("disabled");
                    $('#submit').addClass("disabled");
                    if (child <= 2) {
                        $(this).addClass("disabled");
                    }
                    if (child > 1) {
                        child--;
                    }
                }
                var circle_child = child + 1;
                $("#svg_form_time rect:nth-of-type(n + " + child + ")").css(
                    "fill",
                    base_color
                );
                $("#svg_form_time circle:nth-of-type(n + " + circle_child + ")").css(
                    "fill",
                    base_color
                );
                var currentSection = $("section:nth-of-type(" + child + ")");
                currentSection.fadeIn();
                currentSection.css('transform', 'translateX(0)');
                currentSection.prevAll('section').css('transform', 'translateX(-100px)');
                currentSection.nextAll('section').css('transform', 'translateX(100px)');
                $('section').not(currentSection).hide();


            });

        });

    </script>
    <script src="{{ asset('backend/js/core/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/js/core/popper.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/js/plugins/moment.min.js') }}"></script>
    <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
    <script src="{{ asset('backend/js/plugins/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{ asset('backend/js/plugins/nouislider.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/js/toastr.min.js') }}" type="text/javascript"></script>
    <!--  Preloader    -->
    <script src="{{ asset('backend/js/resource_js_preloader.js') }}" type="text/javascript"></script>
    <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('backend/js/material-kit.js?v=2.0.7') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/js/select2.min.js') }}" type="text/javascript"></script>
    <script>
        $('.role').select2({
            placeholder: "Select an option",
            allowClear: true
        });

        $('.title').select2({
            placeholder: "Select Your Title",
            allowClear: true
        });

        $('.residence').select2({
            placeholder: "Select Country of Residence",
            allowClear: true
        });

        $('.birth').select2({
            placeholder: "Select Country of Birth",
            allowClear: true
        });

        $('.nationality').select2({
            placeholder: "Select Nationality",
            allowClear: true
        });

        $('.ethnicity').select2({
            placeholder: "Select Ethnicity",
            allowClear: true
        });

    </script>
    @if (Session::has('success'))
        <script>
            toastr.success("{!!  Session::get('success') !!}")

        </script>
    @elseif (Session::has('error'))
        <script>
            toastr.error("{!!  Session::get('error') !!}")

        </script>
    @endif
</body>

</html>
