@extends('layouts.frontend.layouts.app')
@section('content')
    <section style="padding: 5em 0" class="container">
        <div style="text-align: center">
            <h2>
                Admission Network connects international students, educational institutes and recruitment partners to provide educational opportunities around the world.
            </h2>
            <br>
            <a class="btn btn-primary btn-lg btn-r" href="{{route('about')}}">
                HOW WE WORK <i class='bx bx-right-arrow-circle'></i>
            </a>
        </div>

        <div style="padding-top: 3em;"  class="row">
            <div class="col-md-4">
                <div class="card shadow">
                    <div class="card-body">
                      <h5 class="card-title">STUDENTS</h5>
                      <p class="card-text">
                        Choose and apply to a Higher education course from a range of Universities and Colleges that matches your educational background, experience & interests.
                      </p>
                    <a href="{{url('/students')}}" class="card-link">GET STARTED <i class='bx bxs-chevrons-right'></i></a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card shadow">
                    <div class="card-body">
                      <h5 style="text-transform: uppercase" class="card-title">PARTNER Schools, Colleges and Universities</h5>
                      <p class="card-text">
                        Diversify your campus by bringing in quality students from around the world.
                      </p>
                    <a href="{{url('/schools')}}" class="card-link">LEARN HOW <i class='bx bxs-chevrons-right'></i></a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card shadow">
                    <div class="card-body">
                      <h5 class="card-title">RECRUITMENT PARTNERS</h5>
                      <p class="card-text">
                        Give your students a chance to successfully access and peruse their education in a range of prestigious educational institutions around the world.
                      </p>
                    <a href="{{url('/recruiters')}}" class="card-link">WORK WITH US <i class='bx bxs-chevrons-right'></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section style="padding: 5em 0; background-color: #DDF5FF">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{asset('frontend/img/new/STUDENTS.jpg')}}" class="img-fluid" alt="">
                </div>
                <div class="col-md-6">
                    <div class="">
                        <h1>
                            Easy Student Application Process
                        </h1>
                        <h2 style="color:#6F6F6F; font-weight:300">
                            Using the Admission Network Platform, will allow our students and recruitment partners to have an easy access to apply to the educational institutions around the world and most renown programmes.
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section style="padding: 5em 0; background-color: #ffffff">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="">
                        <h2 style="color: #B3B3B3">
                            STUDENTS
                        </h2>
                        <h1>
                            Pursue Your Dream of International education abroad
                        </h1>
                        <h4 style="color:#6F6F6F; font-weight:300">
                            Access to opportunities of Higher education around the world to apply to programs at Universities and Colleges that meet your future goals and needs. A great pathway to start your study abroad journey!
                        </h4>
                    </div>
                    <br>
                    <a class="btn btn-primary btn-lg btn-r" href="{{url('/students')}}">
                        GET STARTED <i class='bx bx-right-arrow-circle'></i>
                    </a>
                </div>
                <div class="col-md-6">
                    <img src="{{asset('frontend/img/new/STUDENT.png')}}" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>


    <section style="padding: 5em 0; background-color: #DDF5FF">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{asset('frontend/img/new/Partner.jpg')}}" class="img-fluid" alt="">
                </div>
                <div class="col-md-6">
                    <div class="">
                        <h2 style="color: #B3B3B3;text-transform: uppercase">
                            PARTNER Schools, Colleges and Universities
                        </h2>
                        <h1>
                            Bring in Diverse Quality Students
                        </h1>
                        <h4 style="color:#6F6F6F; font-weight:300">
                            The Admission Network Platform leads you to enhance the diversity of your campus by linking you with highly motivated quality students from around the world.
                        </h4>
                    </div>
                    <br>
                    <a class="btn btn-primary btn-lg btn-r" href="{{url('/schools')}}">
                        GET STARTED <i class='bx bx-right-arrow-circle'></i>
                    </a>
                </div>
            </div>
        </div>
    </section>


    <section style="padding: 5em 0; background-color: #ffffff">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="">
                        <h2 style="color: #B3B3B3">
                            RECRUITMENT PARTNERS
                        </h2>
                        <h1>
                            Further Your Student's Educational Success
                        </h1>
                        <h4 style="color:#6F6F6F; font-weight:300">
                            Deliver our students an exceptional application experience at Universities and Colleges by providing them quality service with improved efficiency.
                        </h4>
                    </div>
                    <br>
                    <a class="btn btn-primary btn-lg btn-r" href="{{url('/recruiters')}}">
                        WORK WITH US <i class='bx bx-right-arrow-circle'></i>
                    </a>
                </div>
                <div class="col-md-6">
                    <img src="{{asset('frontend/img/new/RECRUITMENT.jpg')}}" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>


    <section style="padding: 5em 0; background-color: #DDF5FF">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{asset('frontend/img/new/Opportunities.jpg')}}" class="img-fluid" alt="">
                </div>
                <div class="col-md-6">
                    <div class="">
                        <h1>
                            Access to better Opportunities
                        </h1>
                        <h4 style="color:#6F6F6F; font-weight:300">
                            Admission Network has built partnerships with prestigious universities and colleges, to give our students the best around the world.
                        </h4>
                    </div>
                    <br>
                    <a class="btn btn-primary btn-lg btn-r" href="{{route('study-destination')}}">
                        FIND INSTITUTIONS <i class='bx bx-right-arrow-circle'></i>
                    </a>
                </div>
            </div>
        </div>
    </section>


    <section style="padding: 5em 0; background-color: #ffffff">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="">
                        <h1>
                            Scholarships
                        </h1>
                        <h4 style="color:#6F6F6F; font-weight:300">
                            Admission Network guides our students around the world with their applications on global education. Admission Network also provide qualified students with scholarships facilities
                        </h4>
                    </div>
                    <br>
                    <a class="btn btn-primary btn-lg btn-r" href="{{route('study-destination')}}">
                        WORK WITH US <i class='bx bx-right-arrow-circle'></i>
                    </a>
                </div>
                <div class="col-md-6">
                    <img src="{{asset('frontend/img/new/SCHOLARSHIP.jpg')}}" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>

@endsection

@push('styles')
    <style>

    </style>
@endpush
