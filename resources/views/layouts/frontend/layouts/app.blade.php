<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>Admission Network - Trusted place to connect you with best Institutions</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{asset('frontend/img/LOGO-FINAL.png')}}" rel="icon">
  {{-- <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon"> --}}

  <!-- Google Fonts -->
  {{-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet"> --}}
  <link href="{{asset('/backend/css/select2.min.css')}}" rel="stylesheet" />
  <link href="{{asset('frontend/css/google-fonts.css')}}" rel="stylesheet">

  @include('layouts.frontend.partials.global-styles')
  @stack('styles')

  <!-- =======================================================
  * Template Name: BizLand - v1.1.1
  * Template URL: https://bootstrapmade.com/bizland-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  @include('layouts.frontend.partials.top-bar')
  <!-- ======= Header ======= -->
  @include('layouts.frontend.partials.nav')

  @if(request()->is('/'))
  <!-- ======= Slider Section ======= -->
    @include('layouts.frontend.partials.slider')
  @endif

  <main id="main">

    @yield('content')

  </main><!-- End #main -->
  @include('layouts.frontend.partials.footer')
  <!-- ======= Footer ======= -->


  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  @include('layouts.frontend.partials.global-scripts')
  @stack('scripts')

</body>

</html>
