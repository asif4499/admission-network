<footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

            <div class="col-md-3">
                <img src="{{asset('frontend/img/LOGO-FINAL.png')}}" class="img-fluid" alt="">
            </div>

            <div class="col-md-3">
               <h4>
                ABOUT ADMISSION NETWORK
               </h4>
               <p>
                At Admission Network we are committed to providing quality student consultancy and recruitment to students and institutions and will accept nothing less than a first class consultancy services for all students and institutions using our services. We always ensure that students are equipped to cope with change to acquire the correct skills and knowledge so that they have continued learning and observation skills throughout their lives and develop positive attitudes leading to high standards of perfection and excellence.
               </p>
            </div>

            <div class="col-md-3">
                <h4>
                    Quick Menu
                </h4>
                    <a  style="text-align:both" href="{{url('/')}}">
                        HOME
                    </a>
                    <br>
                    <a  style="text-align:both" href="{{route('about')}}">
                        ABOUT US
                    </a>
                    <br>
                    <a  style="text-align:both" href="{{route('study-destination')}}">
                        Study Destination
                    </a>
                    <br>
                    <a  style="text-align:both" href="{{route('student')}}">
                        STUDENTS
                    </a>
                    <br>
                    <a  style="text-align:both" href="{{route('school')}}">
                        SCHOOLS
                    </a>
                    <br>
                    <a  style="text-align:both" href="#">
                        Events
                    </a>
                    <br>
                    <a  style="text-align:both" href="#">
                        Blogs & News
                    </a>
                    <br>
                    <a  style="text-align:both" href="{{route('contact')}}">
                        Find us
                    </a>
                    <br>
             </div>

             <div class="col-md-3">
                <h4 style="text-align: center;">
                    Global office enquiries
                </h4>
                <div class="card-body">
                    <address style="text-align: center">
                        For enquiries from Bangladesh: <a href="Bangladesh@admissionnetwork.net">Bangladesh@admissionnetwork.net</a><br>
                    </address>
                    <address style="text-align: center">
                        For enquiries from Nepal: <a href="Nepal@admissionnetwork.net">Nepal@admissionnetwork.net</a><br>
                    </address>
                    <address style="text-align: center">
                        For enquiries from India: <a href="India@admissionnetwork.net">India@admissionnetwork.net</a><br>
                    </address>
                    <address style="text-align: center">
                        For enquiries from Sri Lanka: <a href="Srilanka@admissionnetwork.net">Srilanka@admissionnetwork.net</a><br>
                    </address>
                  </div>
             </div>
        </div>
      </div>
    </div>

    <div class="container py-4">
      <div class="copyright">
        &copy; ALL RIGHTS RESERVER & COPYWRITE BY ADMISSION NETWORK
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/bizland-bootstrap-business-template/ -->
            {{-- <div class="social-links mt-1"> --}}
              {{-- <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a> --}}
            {{-- </div> --}}
      </div>
    </div>
  </footer><!-- End Footer -->
