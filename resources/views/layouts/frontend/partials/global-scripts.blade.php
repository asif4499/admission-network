<script src="{{asset('frontend/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('frontend/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('frontend/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
{{-- <script src="{{asset('frontend/vendor/php-email-form/validate.js')}}"></script>
<script src="{{asset('frontend/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('frontend/vendor/counterup/counterup.min.js')}}"></script>
<script src="{{asset('frontend/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('frontend/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('frontend/vendor/venobox/venobox.min.js')}}"></script>
<script src="{{asset('frontend/vendor/aos/aos.js')}}"></script> --}}
<script src="{{asset('backend/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/js/dataTables.responsive.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/ion.rangeSlider.min.js')}}" type="text/javascript"></script>

<!-- Template Main JS File -->
<script src="{{asset('frontend/js/main.js')}}"></script>
<script>
    $( document ).ready(function() {
        $('.select2').select2({
            placeholder: "Select an option",
            allowClear: true
        });
    });
</script>
