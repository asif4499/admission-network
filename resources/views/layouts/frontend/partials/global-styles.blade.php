  <!-- Vendor CSS Files -->
  <link href="{{asset('frontend/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('frontend/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
  <link href="{{asset('frontend/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  {{-- <link href="{{asset('frontend/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet"> --}}
  {{-- <link href="{{asset('frontend/vendor/venobox/venobox.css')}}" rel="stylesheet"> --}}
  {{-- <link href="{{asset('frontend/vendor/aos/aos.css')}}" rel="stylesheet"> --}}
  {{-- <link href="{{asset('frontend/vendor/font-awesome.min.css')}}" rel="stylesheet"> --}}
  <link href="{{asset('/backend/css/jquery.dataTables.min.css')}}" rel="stylesheet" />
  <link href="{{asset('/backend/css/responsive.dataTables.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/ion.rangeSlider.min.css')}}" rel="stylesheet">
  <!-- Template Main CSS File -->
  <link href="{{asset('frontend/css/style.css')}}" rel="stylesheet">
