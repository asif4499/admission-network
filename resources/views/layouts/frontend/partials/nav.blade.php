  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div style="max-width:95% !important" class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="{{url('/')}}">
          <img src="{{asset('frontend/img/LOGO-FINAL.png')}}" alt="">
      </a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt=""></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="text-uppercase {{ (request()->is('/')) ? 'active-nav' : '' }}"><a href="{{url('/')}}">HOME</a></li>
          <li class="text-uppercase {{ (request()->is('about')) ? 'active-nav' : '' }}"><a href="{{route('about')}}">ABOUT US</a></li>
          <li class="text-uppercase {{ (request()->is('study-destination')) ? 'active-nav' : '' }}"><a href="{{route('study-destination')}}">Study Destination</a></li>
          <li class="text-uppercase {{ (request()->is('students')) ? 'active-nav' : '' }}"><a href="{{route('student')}}">STUDENTS</a></li>
          <li class="text-uppercase {{ (request()->is('schools')) ? 'active-nav' : '' }}"><a href="{{route('school')}}">SCHOOLS</a></li>
          <li class="text-uppercase {{ (request()->is('recruiters')) ? 'active-nav' : '' }}"><a href="{{route('recruiter')}}">RECRUITERS</a></li>
          <li class="text-uppercase"><a href="#">Events</a></li>
          <li class="text-uppercase"><a href="#">Blogs & News </a></li>
          {{-- <li class=" {{ (request()->is('students')) ? 'active-nav' : '' }}"><a href="#">FIND SCHOOLS & UNIVERSITIES</a></li> --}}
          <li class="text-uppercase {{ (request()->is('contact')) ? 'active-nav' : '' }}"><a href="{{route('contact')}}">Find us</a></li>
          <!-- <li class="drop-down"><a href="">Drop Down</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="drop-down"><a href="#">Deep Drop Down</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 2</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
            </ul>
          </li> -->

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
