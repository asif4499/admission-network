<div  style="padding-top: 10em;" id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img style="min-height: 80%" class="d-block w-100" src="{{asset('frontend/img/new/SLIDE-1.jpg')}}" alt="First slide">
      </div>
      <div class="carousel-item">
        <img style="min-height: 80%" class="d-block w-100" src="{{asset('frontend/img/new/SLIDE-2.jpg')}}" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img style="min-height: 80%"  class="d-block w-100" src="{{asset('frontend/img/new/SLIDE-3.jpg')}}" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
