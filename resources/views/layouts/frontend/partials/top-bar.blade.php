<div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        {{-- <b>CONTACT FOR QUERIES : </b> --}}
        <!-- <i class="icofont-envelope"></i> <a href="mailto:contact@example.com">contact@example.com</a> -->
        {{-- <i class="icofont-phone"></i> <a href="tel:+44(0)203 0023 555">+44(0)203 0023 555</a>  ||  INFO@ADMISSIONNETWORK.NET --}}
      </div>
      <div class="social-links">
        {{-- <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <a href="#" class="skype"><i class="icofont-skype"></i></a>
        <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a> --}}
        @guest
        <a href="{{asset('login')}}" class="hover-green">LOGIN / REGISTER</a>
        @else
        <a href="{{asset('login')}}" class="hover-green">
            {{auth()->user()->name}}
        </a>
        @endguest
      </div>
    </div>
</div>
