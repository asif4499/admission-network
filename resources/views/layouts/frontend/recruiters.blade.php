@extends('layouts.frontend.layouts.app')
@section('content')
    <section style="padding-top: 10em;">
        <div style="padding: 5em 0;height: 90vh;background-color: #0064e1; background-image: url('frontend/img/Recruiter_Hero_Banner_Accessible.png');background-repeat:no-repeat;background-size:cover">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="">
                            <h1 style="color: #fff">
                                Help Your Students Study Abroad
                            </h1>
                            <h2 style="color:#fff; font-weight:300">
                                Give your students more options globally with access to WORLD WIDE, Colleges and Universities. Simplify the application, acceptance, and arrival process through a single online platform.
                            </h2>
                        </div>
                        <br>
                        <a href="{{url('register')}}" style="background-color: #fff; color:#0064e1" class="btn btn-lg">Work With Us <i class='bx bx-right-arrow-circle'></i></a>
                    </div>
                    <div class="col-md-6">
                        {{-- <img src="{{asset('frontend/img/Students_Hero_Image_uirv.png')}}" class="img-fluid" alt=""> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section style="padding: 5em 0" class="container">
        <div style="text-align: center">
            <h1>
                Our Benefits
            </h1>
        </div>

        <div class="row">
            <div style="text-align: center" class="col-md-4">
                <div style="height: 300px" class="card shadow">
                    <div class="card-body">
                      <img class="img-fluid" src="{{asset('frontend/img/Benefits_Access.png')}}" alt="">
                      <br>
                      <br>
                      <h5 class="card-title">Access world wide
                        SCHOOLS, COLLEGES and UNIVERSITIES</h5>
                      <p class="card-text">
                        Featuring over 30,000 programs across all areas of study.
                      </p>
                    </div>
                </div>
            </div>

            <div style="text-align: center" class="col-md-4">
                <div style="height: 300px" class="card shadow">
                    <div class="card-body">
                    <img class="img-fluid" src="{{asset('frontend/img/Benefits_Programs.png')}}" alt="">
                    <br>
                    <br>
                      <h5 class="card-title">Find Programs
                        Faster</h5>
                      <p class="card-text">
                        Discover and apply your students to their programs of choice.
                      </p>
                    </div>
                </div>
            </div>

            <div style="text-align: center" class="col-md-4">
                <div style="height: 300px" class="card shadow">
                    <div class="card-body">
                        <img class="img-fluid" src="{{asset('frontend/img/Benefits_Application.png')}}" alt="">
                        <br>
                        <br>
                      <h5 class="card-title">One Easy
                        Application</h5>
                      <p class="card-text">
                        Create one profile per student and apply to as many Schools, Colleges and Universities, Colleges and Universities as they like.
                      </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div style="text-align: center" class="col-md-4">
                <div style="height: 300px" class="card shadow">
                    <div class="card-body">
                      <img class="img-fluid" src="{{asset('frontend/img/Benefits_Customer_Experience.png')}}" alt="">
                      <br>
                      <br>
                      <h5 class="card-title">A+ Customer
                        Experience</h5>
                      <p class="card-text">
                        A dedicated team that supports you and your students’ journey from application to arrival.
                      </p>
                    </div>
                </div>
            </div>

            <div style="text-align: center" class="col-md-4">
                <div style="height: 300px" class="card shadow">
                    <div class="card-body">
                    <img class="img-fluid" src="{{asset('frontend/img/Benefits_Platform.png')}}" alt="">
                    <br>
                    <br>
                      <h5 class="card-title">A Central
                        Platform</h5>
                      <p class="card-text">
                        Manage your students, their applications, and communications in one place.
                      </p>
                    </div>
                </div>
            </div>

            <div style="text-align: center" class="col-md-4">
                <div style="height: 300px" class="card shadow">
                    <div class="card-body">
                        <img class="img-fluid" src="{{asset('frontend/img/Benefits_Perks.png')}}" alt="">
                        <br>
                        <br>
                      <h5 class="card-title">Perks And
                        Rewards</h5>
                      <p class="card-text">
                        Earn generous commissions, bonuses, and discounts to support your growth.
                      </p>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section style="padding: 5em 0; background-color: #f1f7ff">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{asset('frontend/img/How_ApplyBoard_Works_uirv.png')}}" class="img-fluid" alt="">
                </div>

                <div class="col-md-6">
                    <div class="">
                        <h1>
                            How Admission Network Works
                        </h1>
                        <h3 style="color:#6F6F6F; font-weight:300">
                            Our artificial intelligence (AI) powered platform helps you find Schools, Colleges and Universities that meet your students' interests, apply to multiple programs that match their qualifications, and earn competitive commissions.
                        </h3>
                    </div>
                    <br>
                    <a href="{{url('register')}}" class="btn btn-primary btn-lg btn-r">Learn How <i class='bx bx-right-arrow-circle'></i></a>
                </div>
            </div>
        </div>
    </section>

<br><br>
    <section style="padding: 5em 0; background-color: #08A2FF">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div>
                    <h1 style="color: #FFFFFF">
                        GET STARTED WITH ADMISSION NETWORK
                    </h1>
                </div>
                <br>
                <br>
                <br>
                <div>
                    <a style="text-transform: uppercase; margin: 1em 1em" href="{{url('/students')}}" class="btn btn-white btn-lg btn-r">STUDENTS <i class='bx bxs-chevrons-right'></i></a>
                    <a style="text-transform: uppercase; margin: 1em 1em" href="{{url('/schools')}}" class="btn btn-white btn-lg btn-r">PARTNER Schools, Colleges and Universities <i class='bx bxs-chevrons-right'></i></a>
                    <a style="text-transform: uppercase; margin: 1em 1em" href="{{url('/recruiters')}}" class="btn btn-white btn-lg btn-r">RECRUITMENT PARTNERS <i class='bx bxs-chevrons-right'></i></a>
                </div>
            </div>
        </div>
    </section>
@endsection
