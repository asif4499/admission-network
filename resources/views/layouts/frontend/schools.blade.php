@extends('layouts.frontend.layouts.app')
@section('content')
    <section style="padding-top: 10em;">
        <div style="padding: 5em 0;height: 90vh;background-color: #0064e1; background-image: url('frontend/img/Schools, Colleges and Universities, Colleges and Universities_Hero_Banner_uirv.png');background-repeat:no-repeat;background-size:cover">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="">
                            <h1 style="color: #fff">
                                Increase Your Global Presence and Diversify Your Campus
                            </h1>
                            <h2 style="color:#fff; font-weight:300">
                                Admission Network recruits qualified international students from most of the countries of the world.
                            </h2>
                        </div>
                        <br>
                        <a style="background-color: #fff; color:#0064e1" class="btn btn-lg" href="{{route('register')}}">
                            Partner With Us <i class='bx bx-right-arrow-circle'></i>
                        </a>
                    </div>
                    <div class="col-md-6">
                        {{-- <img src="{{asset('frontend/img/Students_Hero_Image_uirv.png')}}" class="img-fluid" alt=""> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section style="padding: 5em 0" class="container">
        <div style="text-align: center">
            <h1>
                Expand Your International Reach
            </h1>
            <h5 style="color:#6F6F6F; font-weight:300">
                Attract the best students from around the world with Admission Network.
            </h5>
        </div>

        <div class="row">
            <div style="text-align: center;" class="col-md-4">
                <div style="height: 320px" class="card shadow">
                    <div class="card-body">
                      <img class="img-fluid" src="{{asset('frontend/img/Student_Diversity_uirv.png')}}" alt="">
                      <br>
                      <br>
                      <h5 class="card-title">High School</h5>
                      <p class="card-text">
                        High school typically includes grade 9-12 and allows students to develop skills to pursue higher education.
                      </p>
                    </div>
                </div>
            </div>

            <div style="text-align: center" class="col-md-4">
                <div style="height: 320px" class="card shadow">
                    <div class="card-body">
                    <img class="img-fluid" src="{{asset('frontend/img/Quality_Applications_uirv.png')}}" alt="">
                    <br>
                    <br>
                      <h5 class="card-title">Undergraduate</h5>
                      <p class="card-text">
                        Undergraduate programs are taught at colleges and universities. Students receive a diploma, an advanced diploma, or a bachelor’s degree.
                      </p>
                    </div>
                </div>
            </div>

            <div style="text-align: center" class="col-md-4">
                <div style="height: 320px" class="card shadow">
                    <div class="card-body">
                        <img class="img-fluid" src="{{asset('frontend/img/Managed_Recruiter_Network_uirv.png')}}" alt="">
                        <br>
                        <br>
                      <h5 class="card-title">Post-Graduate</h5>
                      <p class="card-text">
                        A post-graduate program follows an undergraduate degree and takes 1-2 years. Students receive a graduate certificate, graduate diploma, or master’s degree.
                      </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div style="text-align: center" class="col-md-4">
                <div style="height: 320px" class="card shadow">
                    <div class="card-body">
                      <img class="img-fluid" src="{{asset('frontend/img/Document_Verification_uirv.png')}}" alt="">
                      <br>
                      <br>
                      <h5 class="card-title">Document
                        Verification</h5>
                      <p class="card-text">
                        In-house verification of key documents, including IELTS.
                      </p>
                    </div>
                </div>
            </div>

            <div style="text-align: center" class="col-md-4">
                <div style="height: 320px" class="card shadow">
                    <div class="card-body">
                    <img class="img-fluid" src="{{asset('frontend/img/Flexible_Platform_uirv.png')}}" alt="">
                    <br>
                    <br>
                      <h5 class="card-title">Applicant
                        Matching</h5>
                      <p class="card-text">
                        Our platform ensures only eligible students apply to your programs.
                      </p>
                    </div>
                </div>
            </div>

            <div style="text-align: center" class="col-md-4">
                <div style="height: 320px" class="card shadow">
                    <div class="card-body">
                        <img class="img-fluid" src="{{asset('frontend/img/Schools, Colleges and Universities, Colleges and Universities_New_Promotional_Channels_uirv.png')}}" alt="">
                        <br>
                        <br>
                      <h5 class="card-title">Promotional
                        Channels</h5>
                      <p class="card-text">
                        Showcase your school on our platform, at events, and across social media.
                      </p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section style="padding: 5em 0; background-color: #f1f7ff">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="center">
                        <h1 style="color: #0064e1">
                            Trusted By Leading Institutions
                        </h1>
                        <h4 style="color: black; font-weight:300">
                            Join 1,200+ leading educational institutions who are already recruiting globally with Admission Network.
                        </h4>
                        <a href="{{route('study-destination')}}" style="background-color: #0064e1; color:#fff"  class="btn btn-lg">Explore Schools, Colleges and Universities <i class='bx bx-right-arrow-circle'></i></a>
                    </div>
                </div>

                <div class="col-md-6">
                    <img src="{{asset('frontend/img/placeholder.png')}}" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>

    <section style="padding: 5em 0; background-color: #ffffff">
        <div class="container">
            <h1 style="text-align: center">
                The Benefits of Partnering with Admission Network
            </h1>
            <h3 style="color:#6F6F6F; font-weight:300; text-align: center">
                Admission Network makes recruiting international students easier.
            </h3>
            <br>
            <br>
            <div class="row">
                <div class="col-md-6">
                    <div class="">
                        <h1>
                            Spend Less Time Vetting Applications
                        </h1>
                        <h3 style="color:#6F6F6F; font-weight:300">
                            The Admission Network Team reviews applications to ensure they are complete, and all required documents are included. This saves you time, while ensuring applications meet your admission requirements.
                        </h3>
                    </div>
                    <br>
                    <a href="{{url('/register')}}" class="card-link">WORK WITH US (The Benefits of Partnering with Admission Network) <i class='bx bxs-chevrons-right'></i></a>
                </div>
                <div class="col-md-6">
                    <img src="{{asset('frontend/img/Quality_Applications_Large_uirv.png')}}" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>

    <section style="padding: 5em 0; background-color: #f1f7ff">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{asset('frontend/img/Student_Diversity_Large_uirv.png')}}" class="img-fluid" alt="">
                </div>

                <div class="col-md-6">
                    <div class="">
                        <h1>
                            Increase Your
                            Campus Diversity
                        </h1>
                        <h3 style="color:#6F6F6F; font-weight:300">
                            Expand your international reach to students in 110+ countries, bringing new cultures and greater diversity to your campuses.
                        </h3>
                    </div>
                    <br>
                    {{-- <button type="button" class="btn btn-primary btn-lg btn-r">WORK WITH US <i class='bx bx-right-arrow-circle'></i></button> --}}
                </div>
            </div>
        </div>
    </section>

    <section style="padding: 5em 0; background-color: #0064e1; color:white">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{asset('frontend/img/Schools, Colleges and Universities, Colleges and Universities_Form_Light_uirv.png')}}" class="img-fluid" alt="">
                </div>

                <div class="col-md-6">
                    <h3 style="color: white">
                        WORK WITH ADMISSION NETWORK
                    </h3>
                    <h1 style="color: white">
                        Partnership Request
                    </h1>
                    <h5 style="color: white; font-weight:300">
                        Complete the form below and our Partner Relations Team will be in touch soon.
                    </h5>

                    @include('layouts.frontend.forms.school-form')

                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')

<script>
    $('#password, #password-confirm').on('keyup', function () {
      if ($('#password').val() == $('#password-confirm').val()) {
        $('#message').html('Password Matched').css('color', 'green');
      } else
        $('#message').html('Password Do Not Matching').css('color', 'red');
    });

    var password = document.getElementById("password")
      , confirm_password = document.getElementById("password-confirm");

    function validatePassword(){
      if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
      } else {
        confirm_password.setCustomValidity('');
      }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>

<script>
    function myFunction() {
      var x = document.getElementById("myDIV");
      if (x.style.display === "block") {
        x.style.display = "none";
      } else {
        x.style.display = "block";
      }
    }


    // $( document ).ready(function() {
    //     $('#country').select2({
    //         // placeholder: "FIND YOU FAVORITE COUNTRY",
    //         allowClear: true
    //     });
    // });
</script>

@endpush
