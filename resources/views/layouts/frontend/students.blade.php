@extends('layouts.frontend.layouts.app')
@section('content')
    <section style="padding-top: 10em;">
        <div style="padding: 5em 0;height: 90vh;background-color: #0064e1; background-image: url('frontend/img/Students_Hero_Image_uirv.png');background-repeat:no-repeat;background-size:cover">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="">
                            <h1 style="color: #fff">
                                Apply to Study Abroad at Your Dream School
                            </h1>
                            <h2 style="color:#fff; font-weight:300">
                                Learn about programs and Schools, Colleges and Universities, get matched to the best options for you, easily submit your application, and get the support you need along the way!
                            </h2>
                        </div>
                        <br>
                        <a style="background-color: #fff; color:#0064e1" href="{{url('/register')}}" class="btn btn-lg">GET STARTED <i class='bx bx-right-arrow-circle'></i></a>
                    </div>
                    <div class="col-md-6">
                        {{-- <img src="{{asset('frontend/img/Students_Hero_Image_uirv.png')}}" class="img-fluid" alt=""> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section style="padding: 5em 0" class="container">
        <div style="text-align: center">
            <h1>
                Study with Admission Network
            </h1>
        </div>

        <div style=""  class="row">
            <div style="text-align: center" class="col-md-4">
                <div class="card shadow">
                    <div class="card-body">
                      <h5 class="card-title">High School</h5>
                      <p class="card-text">
                        High school typically includes grade 9-12 and allows students to develop skills to pursue higher education.
                      </p>
                      <a href="{{route('study-destination')}}" class="card-link">Explore High Schools, Colleges and Universities <i class='bx bxs-chevrons-right'></i></a>
                    </div>
                </div>
            </div>

            <div style="text-align: center" class="col-md-4">
                <div class="card shadow">
                    <div class="card-body">
                      <h5 class="card-title">Undergraduate</h5>
                      <p class="card-text">
                        Undergraduate programs are taught at colleges and universities. Students receive a diploma, an advanced diploma, or a bachelor’s degree.
                      </p>
                      <a href="{{route('study-destination')}}" class="card-link">Undergraduate Programs <i class='bx bxs-chevrons-right'></i></a>
                    </div>
                </div>
            </div>

            <div style="text-align: center" class="col-md-4">
                <div class="card shadow">
                    <div class="card-body">
                      <h5 class="card-title">Post-Graduate</h5>
                      <p class="card-text">
                        A post-graduate program follows an undergraduate degree and takes 1-2 years. Students receive a graduate certificate, graduate diploma, or master’s degree.
                      </p>
                      <a href="{{route('study-destination')}}" class="card-link">Post-Graduate Programs <i class='bx bxs-chevrons-right'></i></a>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section style="padding: 5em 0; background-color: #f1f7ff">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{asset('frontend/img/Student_Journey_uirv.png')}}" class="img-fluid" alt="">
                </div>
                <div class="col-md-6">
                    <h1 style="color: #0064e1">
                        How It Works
                    </h1>
                    <div style="padding: 2em 0">
                        <h3 style="color: black">
                            STEP 1
                        </h3>
                        <h3 style="color:#0064e1;">
                            Find the Right Program and School
                        </h3>
                        <h4 style="color: black; font-weight:300">
                            Complete a short survey and get matched to programs and Schools, Colleges and Universities that meet your unique background and interests.
                        </h4>
                    </div>

                    <div style="padding: 2em 0">
                        <h3 style="color: black">
                            STEP 2
                        </h3>
                        <h3 style="color:#0064e1;">
                            Submit Your Application
                        </h3>
                        <h4 style="color: black; font-weight:300">
                            Select your program and school, complete your online profile, pay the application fee, and submit the required documents. Admission Network will review your application and ensure you’re not missing any information.
                        </h4>
                    </div>

                    <div style="padding: 2em 0">
                        <h3 style="color: black">
                            STEP 3
                        </h3>
                        <h3 style="color:#0064e1;">
                            Get Your Letter of Acceptance
                        </h3>
                        <h4 style="color: black; font-weight:300">
                            Your application is submitted and reviewed by the school. If accepted, you will quickly receive your acceptance letter.
                        </h4>
                    </div>

                    <div style="padding: 2em 0">
                        <h3 style="color: black">
                            STEP 4
                        </h3>
                        <h3 style="color:#0064e1;">
                            Start the Visa Process
                        </h3>
                        <h4 style="color: black; font-weight:300">
                            Admission Network’s team of experts provide you with guidance and support during the visa application process and beyond, to give you the best chance of success.
                        </h4>
                    </div>

                    <div style="padding: 2em 0">
                        <h3 style="color: black">
                            STEP 5
                        </h3>
                        <h3 style="color:#0064e1;">
                            Book Your Plane Ticket and Go!
                        </h3>
                        <h4 style="color: black; font-weight:300">
                            Once you’ve got your acceptance and visa, book your plane ticket and begin your educational journey abroad! Don’t forget to arrange your accommodations.
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

<br><br>
    <section style="padding: 5em 0; background-color: #08A2FF">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div>
                    <h1 style="color: #FFFFFF">
                        GET STARTED WITH ADMISSION NETWORK
                    </h1>
                </div>
                <br>
                <br>
                <br>
                <div>
                    <a style="text-transform: uppercase; margin: 1em 1em" href="{{url('/students')}}" class="btn btn-white btn-lg btn-r">STUDENTS <i class='bx bxs-chevrons-right'></i></a>
                    <a style="text-transform: uppercase; margin: 1em 1em" href="{{url('/schools')}}" class="btn btn-white btn-lg btn-r">PARTNER Schools, Colleges and Universities <i class='bx bxs-chevrons-right'></i></a>
                    <a style="text-transform: uppercase; margin: 1em 1em" href="{{url('/recruiters')}}" class="btn btn-white btn-lg btn-r">RECRUITMENT PARTNERS <i class='bx bxs-chevrons-right'></i></a>
                </div>
            </div>
        </div>
    </section>
@endsection
