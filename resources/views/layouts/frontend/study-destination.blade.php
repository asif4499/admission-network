@extends('layouts.frontend.layouts.app')
@section('content')
    <section style="padding-top: 10em;">
        <div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                          <h5 style="text-align: center">Filter</h5>
                            <hr>
                            {{-- <form action="{{route('study-search-result')}}" method="post">
                                @csrf --}}
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">COUNTRY</label>
                                    {{ Form::select('country[]', countries() ? countries() : [], null, ['class' => 'select2 form-control', 'id' => 'country', 'multiple' => 'multiple']) }}
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">PROGRAME</label>
                                    {{ Form::select('programme[]', programmes() ? programmes() : [], null, ['class' => 'select2 form-control', 'id' => 'programme', 'multiple' => 'multiple']) }}
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">UNIVERSITY</label>
                                    {{ Form::select('university[]', universities() ? universities() : [], null, ['class' => 'select2 form-control', 'id' => 'university', 'multiple' => 'multiple']) }}
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">CITY</label>
                                    {{ Form::select('city[]', cities() ? cities() : [], null, ['class' => 'select2 form-control', 'id' => 'city', 'multiple' => 'multiple']) }}
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">FEE</label>
                                    <input type="text" class="js-range-slider" name="fee" id="fee" value=""
                                    data-type="double"
                                    data-min="{{minFees()}}"
                                    data-max="{{maxFees()}}"
                                    data-from="{{minFees()}}"
                                    data-to="{{maxFees()}}"
                                    data-grid="true"/>
                                </div>
                                <br>
                                <div class="d-flex justify-content-center">
                                    {{-- <button type="reset" class="btn btn-secondary" onclick="resetFilter()">Clear</button>
                                    &nbsp;&nbsp; --}}
                                    <button type="submit" class="btn btn-primary" onclick="filter()">Apply Filter</button>
                                    {{-- <button type="submit" class="btn btn-primary" id="btn_filter">Apply Filter</button> --}}
                                </div>
                            {{-- </form> --}}
                        </div>
                      </div>
                </div>

                <div class="col-md-8">
                    <form action="{{route('country-details')}}" method="GET">
                        @csrf
                        <div class="input-group mb-3">
                            {{ Form::select('country-id', countries() ? ['' => '']+countries() : [], null, ['class' => 'form-control', 'id' => 'country-details']) }}
                            <div class="input-group-append">
                                <button formtarget="_blank" class="btn btn-outline-primary btn-sm" type="submit">Search</button>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <div style="height:650px; overflow: auto;">
                        <table class="table " id="get-result">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@push('styles')
    <style>
        table.dataTable thead th, table.dataTable thead td{
            border-bottom: unset;
        }
        table.dataTable.no-footer{
            border-bottom: unset;
        }
        .table td, .table th{
            border-top: unset;
        }
        .table thead th{
            border-bottom: unset;
            display: none;
        }
        .select2-container--default .select2-selection--single{
            border-radius: unset;
        }
        .select2-container .select2-selection--single{
            /* height: 35px; */
        }
    </style>
@endpush

@push('scripts')
<script>
 $(".js-range-slider").ionRangeSlider({
    postfix: "K",
 });
</script>

<script>
    $( document ).ready(function() {
        $('#country-details').select2({
            placeholder: "FIND YOU FAVORITE COUNTRY",
            allowClear: true
        });
    });
</script>

<script>
    filter();
    function filter() {
        var fee = $('#fee').val();
        var country = $('#country').val();
        var programme = $('#programme').val();
        var university = $('#university').val();
        var city = $('#city').val();

        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#get-result').DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                bPaginate: false,
                bInfo: false,
                bFilter: false,
                // paging: false,
                // ajax: '{{ url('study-search-result') }}',
                ajax: {
                    url: '{{ url('study-search-result') }}',
                    data:{'fee':fee, 'country':country, 'programme':programme, 'university':university, 'city':city},
                    type: 'post'
                },
                columns: [
                    // { data: 'data', name: 'data'}
                    { data: 'data', name: 'data',
                    render:
                            function(value, type, row)
                            {
                                var output="";
                                output ='<div class="card">'+
                                    '<div class="row card-body">'+
                                        '<div class="col-md-3 align-self-center d-flex justify-content-center">'+
                                            '<img class="img-fluid" src="{{asset('frontend/img/LOGO-FINAL.png')}}" alt="">'+
                                        '</div>'+
                                        '<div class="col-md-6 align-self-center">'+
                                            '<h5 class="card-title">'+value[1]+'</h5>'+
                                            '<p class="card-text"><b>Address: </b>'+value[2]+'</p>'+
                                            '<p class="card-text"><b>Country: </b>'+value[3]+'</p>'+
                                            '<a href="all-program/'+value[0]+'">Click to see All Programme Offers</a>'+
                                        '</div>'+
                                        '<div class="col-md-3 align-self-center d-flex justify-content-center">'+
                                            '<button type="submit" class="btn btn-outline-primary">APPLY NOW</button>'+
                                        '</div>'+
                                    '</div>'+
                                    '</div>';

                                return output;
                             }},
                ],

            });
            $('#get-result').DataTable().destroy();
    };

    // $('#btn_filter').click(function () {
    //     var fee = $('#fee').val();
    //     var country = $('#country').val();
    //     var programme = $('#programme').val();
    //     var university = $('#university').val();
    //     var city = $('#city').val();
    //     $('#serviceprocesses-table').DataTable().destroy();
    //     filter_datatable(fee, country, programme, university, city);
    //     });

    // function filter() {
    //     var fee = $('#fee').val();
    //     var country = $('#country').val();
    //     var programme = $('#programme').val();
    //     var university = $('#university').val();
    //     var city = $('#city').val();
    //     $.ajax({
    //             headers: {
    //                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //             },
    //             url: '{{route("study-search-result")}}',
    //             type: 'get',
    //             data: {'fee':fee, 'country':country, 'programme':programme, 'university':university, 'city':city},
    //             success:function(response){
    //                 var dataItems = "";
    //                 $.each(data, function (index, itemData) {
    //                     dataItems += index + ": " + itemData + "\n";
    //                 });
    //                 $('.filter_data').html(response);
    //             }
    //     })
    // };

</script>
@endpush
