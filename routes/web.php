<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Auth::routes();

Route::get('/registerInstitute', 'UserController@registerInstitute')->name('registerInstitute');

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/students', 'HomeController@student')->name('student');
Route::get('/schools', 'HomeController@school')->name('school');
Route::get('/recruiters', 'HomeController@recruiter')->name('recruiter');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/study-destination', 'HomeController@studyDestination')->name('study-destination');
Route::post('/study-search-result', 'HomeController@studySearchResult')->name('study-search-result');
Route::get('/all-program/{id}', 'HomeController@program')->name('all-program');
Route::get('/country-details', 'HomeController@countryDetails')->name('country-details');


Route::resource('school-form', 'SchoolInfoController');

Route::post('user/storeSchool', 'UserController@storeSchool')->name('storeSchool');
Route::resource('user', 'UserController');

Route::middleware('auth')->group(function () {
    Route::group([ 'middleware' => 'student'], function() {
    });
        Route::get('student/show-profile/{id}', 'StudentController@showProfile')->name('students-form.showProfile');
        Route::put('program/apply/{id}/{student_id}', 'ProgramController@programApply')->name('program.apply');
        Route::resource('program', 'ProgramController');
        Route::get('/dashboard/school-index', 'AllusertableController@schoolIndex')->name('school.index');
        Route::get('get-school-list', 'AllusertableController@schoolList')->name('get-school-list');

        Route::resource('students-form', 'StudentController');

    Route::group([ 'middleware' => 'admin'], function() {


    });

    Route::get('get-user-list', 'AllusertableController@userList')->name('get-user-list');
    Route::get('/dashboard/allusrIndex/{role_id}', 'AllusertableController@index')->name('alluser.index');
    Route::get('get-student-list', 'AllusertableController@studentList')->name('get-student-list');
    Route::get('/dashboard/student-index', 'AllusertableController@studentIndex')->name('student.index');
    Route::get('get-recruiter-list', 'AllusertableController@recruiterList')->name('get-recruiter-list');
    Route::get('/dashboard/recruiter-index', 'AllusertableController@recruiterIndex')->name('recruiter.index');
    Route::get('get-supervisor-list', 'AllusertableController@supervisorList')->name('get-supervisor-list');
    Route::get('/dashboard/supervisor-index', 'AllusertableController@supervisorIndex')->name('supervisor1.index');

    Route::resource('supervisor', 'SupervisorController');

    Route::resource('country', 'CountryController');
    Route::get('get-country-list', 'CountryController@getAllCountry')->name('get-country-list');
    Route::post('get-country-info', 'CountryController@getCountryInfo')->name('get-country-info');

    Route::put('dashboard/allusrIndex/user-approval/{id}', 'StudentController@userApproval')->name('user.approval');
    Route::put('student-form/assign-supervisor/{id}', 'StudentController@assignSupervisor')->name('student-form.assignSupervisor');
    Route::resource('recruiters-form', 'RecruiterController');
    Route::get('program-recruiter-index/{id}', 'AllusertableController@programRecruiter')->name('programRecruiter.index');

    Route::get('get-studentRecruiter-list', 'RecruiterController@studentListForRecruiter')->name('get-studentRecruiter-list');
    Route::get('get-studentSupervisor-list', 'SupervisorController@studentListForSupervisor')->name('get-studentSupervisor-list');


    Route::get('campus-add', 'CampusController@campusAdd')->name('addcampus.index');
    Route::get('program-add', 'ProgramController@programAdd')->name('addprogram.index');
    Route::get('program/forall/{id}', 'ProgramController@showforall')->name('program.showforall');

    Route::resource('campus', 'CampusController');
    Route::resource('program', 'ProgramController');
    Route::resource('comment', 'CommentController');

    Route::put('program/apply/{id}/{student_id}', 'ProgramController@programApply')->name('program.apply');
    Route::get('/dashboard/allprogram-index/{id}', 'AllusertableController@programIndex')->name('allprogram.index');

    Route::put('program/assign-status/{id}/{student_id}', 'ProgramController@assignStatus')->name('program.assign-status');





    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');




    Route::get('manage/{id}', 'DashboardController@userManage')->name('user.manage');

    Route::get('/home-components', 'HomeController@homeComponents')->name('home-components');

    Route::resource('role', 'RoleController');

    Route::resource('merchants', 'MerchantController');
    Route::get('merchant-requests', 'MerchantController@merchantRequest')->name('merchant-requests');
    Route::get('merchant-inactive', 'MerchantController@merchantInactive')->name('merchants.inactive');
    Route::get('get-merchants-admin', 'MerchantController@getIndexAdmin')->name('get-merchants-admin');
    Route::get('merchant-active/{id}', 'MerchantController@merchantActive')->name('merchant.active');

    Route::resource('special-merchants', 'SpecialMerchantController');

    Route::resource('customers', 'CustomerController');
    Route::get('customers-requests', 'CustomerController@merchantRequest')->name('customers-requests');
    Route::get('customers-active/{id}', 'CustomerController@merchantActive')->name('customers.active');

    Route::resource('admin-services', 'ServiceController');

    Route::resource('admin-client', 'ClientController');
    Route::resource('authority-team', 'AuthorityTeamController');

    Route::resource('admin-inter-city-rates', 'InterCityRateController');
    Route::resource('admin-rate-info', 'AddRateInfoController');

    Route::resource('admin-about', 'AboutUsController');
    Route::resource('admin-about-card', 'AboutUsCardController');

    Route::resource('districts', 'DistrictController');

    Route::resource('areas', 'AreaController');



    Route::resource('deliveries', 'DeliveryController');
    Route::get('print/delivery-info/{id}', 'DeliveryController@printInfoDelivery')->name('deliveries.info.print');
    Route::get('print/pickup-info/{id}', 'DeliveryController@printInfoPickup')->name('deliveries.pickup.print');
    Route::get('deliveries-admin', 'DeliveryController@indexAdmin')->name('deliveries-admin');
    Route::get('all-deliveries-admin', 'DeliveryController@indexAll')->name('all.deliveries');
    Route::get('get-deliveries-admin', 'DeliveryController@getIndexAdmin')->name('get-deliveries-admin');
    Route::get('get-all-deliveries-admin', 'DeliveryController@getAll')->name('get-all-deliveries-admin');
    Route::post('manage-deliveries-admin', 'DeliveryController@manageDelivery')->name('manage-deliveries-admin');
    Route::post('get-delivery-info', 'DeliveryController@getDeliveryInfo')->name('get-delivery-info');
    Route::post('admin-delivered', 'DeliveryController@delivered')->name('admin.delivered');
    Route::post('admin-pickup', 'DeliveryController@pickup')->name('admin.pickup');
    Route::post('admin-cancel', 'DeliveryController@cancel')->name('admin.cancel');
    Route::post('admin-hold', 'DeliveryController@hold')->name('admin.hold');
    Route::post('admin-no-status', 'DeliveryController@noStatus')->name('admin.no.status');
    Route::post('admin-paid', 'DeliveryController@paid')->name('admin.paid');
    Route::post('admin-unpaid', 'DeliveryController@unPaid')->name('admin.unpaid');
    Route::post('deliveries-delete', 'DeliveryController@delete')->name('deliveries.delete');

    Route::get('admin-delivery-statuses', 'DeliveryController@statuses')->name('admin.statuses');

    Route::get('invoices', 'InvoiceController@index')->name('invoices.index');
    Route::get('get-invoices', 'InvoiceController@getInvoice')->name('get.invoices');
    Route::get('invoice/delete/{id}', 'InvoiceController@delete')->name('invoice.delete');
    Route::get('invoice/{id}', 'InvoiceController@show')->name('invoice.show');
    Route::post('delivery/invoice', 'InvoiceController@getInvoiceInfo')->name('delivery.invoice');
    Route::get('invoice/pdf-export/{id}', 'InvoiceController@export')->name('export.pdf');

    Route::resource('payment-requests', 'PaymentRequestsController');
    Route::get('get-payment-requests', 'PaymentRequestsController@getRequest');
    Route::post('admin.request.solve', 'PaymentRequestsController@solveRequest')->name('admin.request.solve');
    Route::post('admin.request.pending', 'PaymentRequestsController@pendingRequest')->name('admin.request.pending');

});
