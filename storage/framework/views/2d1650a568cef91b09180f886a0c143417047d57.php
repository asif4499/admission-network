<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Admission Network</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo e(asset('css/not-active/bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo e(asset('css/not-active/animate.css')); ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo e(asset('css/not-active/style.css')); ?>" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo e(asset('css/not-active/colors/default.css')); ?>" id="theme" rel="stylesheet">
      <link href="<?php echo e(asset('/backend/css/material-kit.css?v=2.0.7')); ?>" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- Preloader -->
    <?php if((auth()->user()->role_id==2)&&(auth()->user()->status==3)): ?>
    <div style="text-align:center; margin:200px auto; justify-content: center;align-items: center;">
        <a class="btn btn-primary btn-lg btn-round" href="<?php echo e(route('students-form.create')); ?>"><h3 class="title">Complete the Registration</h3></a>
        <br>
        <a style="background-color: #f44336" href="dashboard.html" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40" href="<?php echo e(route('logout')); ?>"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                 Back to home
             </a>

             <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                 <?php echo csrf_field(); ?>
             </form>
    </div>
   
    <?php endif; ?>
    
    <?php if((auth()->user()->role_id==3)&&(auth()->user()->status==3)): ?>
    <div style="text-align:center; margin:200px auto; justify-content: center;align-items: center;">
        <a class="btn btn-primary btn-lg btn-round" href="<?php echo e(route('recruiters-form.create')); ?>"><h3 class="title">Complete the Registration</h3></a>
        <br>
        <a style="background-color: #f44336" href="dashboard.html" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40" href="<?php echo e(route('logout')); ?>"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                 Back to home
             </a>

             <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                 <?php echo csrf_field(); ?>
             </form>
    </div>
           
    <?php endif; ?>

    <?php if((auth()->user()->role_id==5)&&(auth()->user()->status==3)): ?>
    <div style="text-align:center; margin:200px auto; justify-content: center;align-items: center;">
        <a class="btn btn-primary btn-lg btn-round" href="<?php echo e(route('school-form.create')); ?>"><h3 class="title">Complete the Registration</h3></a>
        <br>
        <a style="background-color: #f44336" href="dashboard.html" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40" href="<?php echo e(route('logout')); ?>"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                 Back to home
             </a>

             <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                 <?php echo csrf_field(); ?>
             </form>
    </div>
           
    <?php endif; ?>

    <?php if((auth()->user()->status==2)): ?>
    <div style=" text-align:center; ">
        <div class="error-box">
            <div class="error-body text-center">
                <h3 class="text-success"><b>Congratulations</b></h3>
                <h2 style="color: #9c27b0">Your are successfully registered to <span style="color: #f44336"><b>Admission Network</b></h2>
                <h3 style="color: #f44336"><b>Please wait for Admin's approval</b></h3>


                <a style="background-color: #f44336" href="dashboard.html" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40" href="<?php echo e(route('logout')); ?>"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                 Back to home
             </a>

             <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                 <?php echo csrf_field(); ?>
             </form>
        </div>
        </div>

    <?php endif; ?>

    <?php if((auth()->user()->status==4)): ?>
    <div style=" text-align:center; ">
        <div class="error-box">
            <div class="error-body text-center">
                <h3 class="text-success"><span style="color: #f44336"><b>Account Deactivated</b></span></h3>
                <h2 style="color: #9c27b0">Please comunicate with <span style="color: #f44336"><b>Admission Network</b></h2>
                <h3 style="color: #f44336"><b>mail address: admission-network@info.com</b></h3>


                <a style="background-color: #f44336" href="dashboard.html" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40" href="<?php echo e(route('logout')); ?>"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                 Back to home
             </a>

             <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                 <?php echo csrf_field(); ?>
             </form>
        </div>
        </div>

    <?php endif; ?>

    <!-- jQuery -->
    <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo e(asset('backend/js/material-kit.js?v=2.0.7')); ?>" type="text/javascript"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
</body>

</html>
<?php /**PATH C:\laragon\www\admission-network\resources\views/layouts/backend/dashboard/not-active.blade.php ENDPATH**/ ?>