<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
  <title>Admission Network - Trusted place to connect you with best Institutions</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo e(asset('frontend/img/LOGO-FINAL.png')); ?>" rel="icon">
  

  <!-- Google Fonts -->
  
  <link href="<?php echo e(asset('/backend/css/select2.min.css')); ?>" rel="stylesheet" />
  <link href="<?php echo e(asset('frontend/css/google-fonts.css')); ?>" rel="stylesheet">

  <?php echo $__env->make('layouts.frontend.partials.global-styles', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->yieldPushContent('styles'); ?>

  <!-- =======================================================
  * Template Name: BizLand - v1.1.1
  * Template URL: https://bootstrapmade.com/bizland-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <?php echo $__env->make('layouts.frontend.partials.top-bar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <!-- ======= Header ======= -->
  <?php echo $__env->make('layouts.frontend.partials.nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  <?php if(request()->is('/')): ?>
  <!-- ======= Slider Section ======= -->
    <?php echo $__env->make('layouts.frontend.partials.slider', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php endif; ?>

  <main id="main">

    <?php echo $__env->yieldContent('content'); ?>

  </main><!-- End #main -->
  <?php echo $__env->make('layouts.frontend.partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <!-- ======= Footer ======= -->


  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <?php echo $__env->make('layouts.frontend.partials.global-scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->yieldPushContent('scripts'); ?>

</body>

</html>
<?php /**PATH D:\laragon\www\admission\resources\views/layouts/frontend/layouts/app.blade.php ENDPATH**/ ?>